﻿using System;

namespace Algorithms
{
    /// <summary>
    /// https://github.com/trekhleb/javascript-algorithms/tree/master/src/algorithms/math/euclidean-algorithm
    /// In mathematics, the Euclidean algorithm, or Euclid's algorithm, is an efficient method for computing the 
    /// greatest common divisor (GCD) of two numbers, the largest number that divides both of them without leaving a remainder.
    /// </summary>
    public static class GreatestCommonDivisor
    {
        public static int Calculate(int inputA, int inputB)
        {
            var a = Math.Abs(inputA);
            var b = Math.Abs(inputB);

            return b == 0 ? a : Calculate(b, a % b);
        }

        public static long Calculate(long inputA, long inputB)
        {
            var a = Math.Abs(inputA);
            var b = Math.Abs(inputB);

            return b == 0 ? a : Calculate(b, a % b);
        }

        public static decimal Calculate(decimal inputA, decimal inputB)
        {
            var a = Math.Abs(inputA);
            var b = Math.Abs(inputB);

            return b == 0 ? a : Calculate(b, a % b);
        }

        public static float Calculate(float inputA, float inputB)
        {
            var a = Math.Abs(inputA);
            var b = Math.Abs(inputB);

            return b == 0 ? a : Calculate(b, a % b);
        }
    }
}
