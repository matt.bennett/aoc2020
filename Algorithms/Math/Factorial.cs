﻿namespace Algorithms
{
    /// <summary>
    /// https://github.com/trekhleb/javascript-algorithms/tree/master/src/algorithms/math/factorial
    /// In mathematics, the factorial of a non-negative integer n, denoted by n!, is the product of all positive integers less than or equal to n.
    /// </summary>
    public static class Factorial
    {
        public static int Calculate(int number)
        {
            return number > 1 ? number * Calculate(number - 1) : 1;
        }

        public static long Calculate(long number)
        {
            return number > 1 ? number * Calculate(number - 1) : 1;
        }

        public static decimal Calculate(decimal number)
        {
            return number > 1 ? number * Calculate(number - 1) : 1;
        }

        public static float Calculate(float number)
        {
            return number > 1 ? number * Calculate(number - 1) : 1;
        }
    }
}
