﻿using System;
using System.Linq;

namespace Algorithms
{
    /// <summary>
    /// https://github.com/trekhleb/javascript-algorithms/tree/master/src/algorithms/math/least-common-multiple
    /// In arithmetic and number theory, the least common multiple, lowest common multiple, or smallest common multiple of two integers a and b, 
    /// usually denoted by LCM(a, b), is the smallest positive integer that is divisible by both a and b. Since division of integers by zero is undefined, 
    /// this definition has meaning only if a and b are both different from zero. However, some authors define lcm(a,0) as 0 for all a, which is the result of 
    /// taking the lcm to be the least upper bound in the lattice of divisibility.
    /// </summary>
    public static class LeastCommonMultiple
    {
        public static int Calculate(int inputA, int inputB)
        {
            return inputA == 0 || inputB == 0 ? 0 : Math.Abs(inputA * inputB) / GreatestCommonDivisor.Calculate(inputA, inputB); 
        }

        public static long Calculate(long inputA, long inputB)
        {
            return inputA == 0 || inputB == 0 ? 0 : Math.Abs(inputA * inputB) / GreatestCommonDivisor.Calculate(inputA, inputB);
        }

        public static decimal Calculate(decimal inputA, decimal inputB)
        {
            return inputA == 0 || inputB == 0 ? 0 : Math.Abs(inputA * inputB) / GreatestCommonDivisor.Calculate(inputA, inputB);
        }

        public static float Calculate(float inputA, float inputB)
        {
            return inputA == 0 || inputB == 0 ? 0 : Math.Abs(inputA * inputB) / GreatestCommonDivisor.Calculate(inputA, inputB);
        }

        // The following calculate the LCM for an array of inputs, using Aggregate to process values.
        // For instance, an array [1,2,3] would aggregate to LCM(LCM(1,2),3)
        public static int Calculate(int[] inputs)
        {
            return inputs.Aggregate(Calculate);
        }

        public static long Calculate(long[] inputs)
        {
            return inputs.Aggregate(Calculate);
        }

        public static decimal Calculate(decimal[] inputs)
        {
            return inputs.Aggregate(Calculate);
        }

        public static float Calculate(float[] inputs)
        {
            return inputs.Aggregate(Calculate);
        }
    }
}
