﻿namespace AoC2020
{
    public static class Data
    {
        #region Day 1
        public static string Day1 = @"1531
1959
1344
1508
1275
1729
1904
1740
1977
1992
1821
1647
1404
1893
1576
1509
1995
1637
1816
1884
1608
1943
1825
1902
1227
1214
1675
1650
1752
1818
862
2006
227
1504
1724
1961
1758
1803
1991
1126
1909
1643
1980
1889
811
1699
1654
1734
1770
1754
1828
1811
1997
1767
1854
1653
1800
1762
1962
1797
877
1660
1895
1939
1679
1496
1606
1262
1727
2005
1796
1595
1846
1822
1974
1829
1347
1341
1875
1726
1963
1659
337
1826
1777
1523
1979
1805
1987
2009
1993
374
1546
1706
1748
1743
1725
281
1317
1839
1683
1794
1898
1824
1960
1292
1876
1760
1956
1701
1565
1680
1932
1632
1494
1630
1838
1863
1328
1490
1751
1707
1567
1917
1318
1861
519
1716
1891
1636
1684
1200
1933
1911
1809
1967
1731
1921
1827
1663
1720
1976
1236
1986
1942
1656
1733
1541
1640
1518
1897
1676
1307
1978
1998
1674
1817
1845
1658
1639
1842
1929
1972
2010
1951
858
1928
1562
1787
1916
1561
1694
1944
1922
1882
1691
589
1940
1624
1570
1557
1791
1492
1919
1615
1571
1657
1984
1521
1766
1790
1782
1874
1198
1764
1278
1688
1905
1786
1281";
        #endregion

        #region Day 2
        public static string Day2 = @"4-5 l: rllllj
4-10 s: ssskssphrlpscsxrfsr
14-18 p: ppppppppppppppppppp
1-6 z: zzlzvmqbzzclrz
4-5 j: jhjjhxhjkxj
7-8 s: tsszsssrmsss
12-13 m: mmmmsmmmmmmqm
16-17 k: kkkkkkkzkkkkrkkmfkk
6-7 x: jxxwxpx
3-7 w: wwwwwwwwwwwwwwwwwwww
4-15 h: gcxfgbpbghdtrkhn
3-4 g: tprznvggnfgpgtmzsrmr
16-17 c: cccccccccccccccwccc
6-7 s: ssssssmsss
3-4 l: flll
4-5 v: mvvvc
17-19 j: lwjsfvkjgjjmsjrsjjr
2-20 k: xhfhfzvghbkbngzbqcck
2-3 s: rssnsxslshrdtk
12-17 b: hbvctbbxbkbbjhbvbw
8-9 j: qjxxtnwjqpjj
14-15 b: bbbbbbbbbbbbbbbb
14-19 l: llllllllllllllllllml
6-7 f: krgfxsffffsffrbf
8-9 f: fffffffffff
16-17 c: cczccwccccccccccz
8-10 s: tlstjjwsms
8-12 f: fffffffftftxff
5-16 z: vxjlgzxckzqgwjdw
8-15 t: lltqlhtptgtsvmw
5-9 h: dhsnnlthw
7-9 d: hgbdzzppdscxt
5-8 g: ggdmfgzm
2-14 w: wwwwwwwwwwwwwwwwww
9-16 q: qqlqqqqkqjqqqqrq
1-5 p: lpdmptz
1-3 l: llql
10-16 x: xxxxxxxxnxxxxxxcrxx
6-8 h: lhhhhqhh
4-15 z: zzszzzzzzmtzzzzrz
4-5 c: zvcsz
7-8 f: zcpftffqfwfffcdf
11-15 s: lsssssssgssfssrh
18-19 p: pppwpppppppppppppppp
3-5 g: gzgggqg
4-6 n: nnfnktnnndqt
1-10 l: llllllllldfll
9-17 x: xxxxxxxxrxxxxxxxhx
13-16 c: cccccncccxccgwckcc
3-4 k: kkhn
4-14 m: mmmdmmmmmmmmmmmm
4-16 t: stwqdgdhlzlzlwqtjmj
1-6 x: xpbxxj
9-10 h: mhhhhhhhhh
1-3 q: qqqqdqqq
1-3 b: bbbb
6-8 g: gggggpgmv
10-13 f: ffxzhfldffvmc
5-9 w: xwwwnwwfw
5-7 d: gtdhdzp
2-14 k: kkkkkkkkkkkkkkkkk
6-13 r: hzqgnrdqqxdwcshjr
5-10 d: ddczhrqjqd
1-10 b: qngplzdvrbsb
3-5 d: xrndch
2-6 s: sssssss
1-6 s: gsssssnssff
4-5 p: pnpxpgppsp
3-7 j: jcjnbvz
8-14 j: mrvbmlsdplscxznj
1-6 b: zbbbbbbbb
4-5 b: bbbbb
6-8 b: bbwbbbbbbbn
11-20 m: dlmcggnmlwmghngcqpxm
8-10 n: nfnncmwnnn
5-8 c: ccccjccc
18-20 k: qdckkkkdvknkkvvblhkk
1-15 l: gclfbjlvsxfgllq
14-16 v: vvvvcvvvvfknvvvvmvvv
4-7 t: rtdtttt
5-7 h: shhhwvb
4-5 x: xxsnvx
11-12 v: vqgqvdvrjvvdvvvvvvl
5-6 l: llllvwll
3-4 l: llhln
6-7 w: wwwwwwb
3-6 j: jjjjjjjfjjjjj
1-7 q: qqqqdqqqqq
4-14 q: qcqjqqqqqqqqqfqqqk
2-5 z: pzzfz
1-2 f: ffsflqqrgzfnmbw
1-4 p: qwgpwlbx
3-5 s: mzpcwhfswsz
6-7 w: dsmwcnwfww
2-5 h: qpkwhsmftnlvcgpq
10-11 m: mmmmmdmmmlbfmmmxmm
2-5 g: vgvgg
4-6 l: lglvltlll
4-7 t: ttttttmtttt
5-8 w: wwkfwwvhrvwwpx
4-5 k: nszkkkdrk
1-4 x: hbdg
6-9 p: pjppfwpjpj
4-5 h: hhrsqh
11-13 x: cfxfxrxxxxxrh
12-15 n: ctjxnzsbjmnnnfnjm
9-13 n: nnnndnntndzlgnnnnnnn
3-4 h: hhhws
4-15 f: kqgfffkvqtksrlwglt
10-15 d: dddddddddgddddbd
3-5 g: ggxgfhggg
4-8 b: bpjbpnvnzwbvfts
5-11 c: wccccmvccccc
17-20 h: hhshhhhhghhhhdhhvhhh
10-12 m: gmmmmmmmpbsmpmmmmhr
1-8 z: zzzzpzzxbh
4-10 c: kcxlxtckgxvc
8-10 f: ffffffffffffff
6-12 h: hhhhhjhhhhhhhhh
6-8 s: wsvwvfssh
3-6 j: xjjzjjzlztjrjjjt
2-3 g: gfvg
11-12 v: vvvvvvvvvvvv
2-4 l: lllltlll
4-9 q: qvqmhqdkd
4-5 l: lllxlj
5-6 d: dddbdddt
3-8 g: gpggggxzdggzxgh
2-7 s: nsrkvkwzpmv
11-13 v: vvvvvpvvvvvvpvvvvpvv
3-5 c: qccvjcc
12-13 d: ddddhddddddfld
3-4 q: dqqqqqqqqqq
12-14 k: kkkkkkkkkkqnkp
2-4 v: bvwvz
6-8 d: mxhdddddckdbkgddtsdd
3-5 p: ttpdppp
9-10 p: pnppvpkphp
2-5 q: qqxczq
1-5 x: xbgtwk
6-7 f: dwzfpfr
12-13 b: bbbbbbbzbbbbb
8-10 x: xxxxxmvcnphxfx
15-16 t: tttttttthmttkttdg
1-3 l: rsmgkpjxnpjnlrmdslsw
2-4 x: xhxsjxx
11-14 w: hwwbwlwhwgxmwwwwwz
3-4 j: pjjdr
1-8 s: sssssssssss
9-13 q: jqxcngqqqrxqwp
3-10 v: vjxhfzbfvv
14-15 w: lmxjqmwtpwsvjjw
8-9 c: tcmccdczs
4-5 w: wwwtw
5-10 f: ffffmfhffkpfffffffff
7-9 l: llgblxlxlwv
6-14 w: wghjmwtbvsvzkfcwwj
6-9 p: mqpnmlpcdppfp
5-7 b: bbbbbbbb
6-7 w: xwwwxcw
11-13 p: ppppppwppfppzszppwpp
3-5 z: rzmzmzzbzzczz
1-8 h: bhbxrhthphhbljknfvhh
11-12 q: qqqqqqqqqqwjqhq
2-17 r: zrmshszlckcrxtmsrtg
4-5 s: svztv
8-11 z: zzzzzzzqzzczz
3-4 h: hnzpc
13-14 q: qqqqqqqqqqqqqhqq
2-5 p: rppgpd
12-15 p: ppppppppppppppl
1-14 v: vvzvvvvvvvjszv
7-9 h: hxhnhhlhzdh
2-3 r: rrrr
5-9 m: mmmmdzmmwmmmm
4-7 f: zpvlfff
4-15 j: gpjjjcjpjjjjjpj
4-7 p: pdkgjhp
6-16 w: wwwwwwwwwwwwwwwww
9-10 q: hsdfqqqrlm
4-16 q: qqqqqqqqqqqqqqqjq
4-7 s: sssdtsr
15-16 p: bqphxhczjpmpwpqpd
7-8 g: wbjggggc
11-16 x: xxxxxxxxxxxxxxxmxx
16-17 w: wwwzwwwwwwwvxvxww
2-7 t: ttttttgttttt
6-14 q: mmkqxqljbkmfpjvgf
16-19 j: jfjjjsjjjjjjjjfjqjj
6-7 r: rrrrrrs
7-9 d: ddgndhdcvdlnddddsdt
2-3 m: mgpms
6-10 g: lqdxdtjmglvgn
6-7 q: qqqqqbqb
3-5 w: wwjtwwwwwwww
3-4 g: ggthg
6-9 k: dbhgrkcjllqkgh
14-16 c: ccccccccccwcclqccccc
2-8 f: wfpvntbqcjf
6-10 r: njrlljrfkl
1-4 h: hhtjhhfh
5-6 z: zgxwzjqz
6-7 b: xbcbsbk
3-4 p: prpq
1-4 s: dhsmk
2-6 f: pffqffrfjff
5-6 h: hhwbdzhhg
5-6 q: qqjqqqqqvq
5-8 k: kkkkkkkq
4-5 k: kkkkzk
8-9 s: ssdvssbss
11-13 q: qqqpqqqqqqrqq
2-13 w: wvcwwwwwcwvmwmwwwww
9-12 r: rrrrrprrgrrhrr
10-13 m: mmmmmmmmwfmmmmm
9-14 t: vgsrtrxzltkvbtt
2-3 j: sjjj
4-5 v: mvvfm
4-5 h: hhhrb
7-10 j: jjjdjvfcjjm
4-13 l: lldllllwlpgmllkbsbd
12-20 r: rwrrrrrrrfdrrqnrrhrr
9-11 s: smssssssssgss
4-5 b: dfmsp
2-3 d: dlsnxd
10-19 m: mmmmmmmmmfmfmmmmmmm
1-5 n: nnnnhnnnn
4-7 d: dfsddkw
1-7 q: dqzzqxqpqqqsqnqq
2-3 p: twrp
14-15 g: lrsggrgvgjjcvggfgcc
9-20 m: rsbmmdmpmjjfhjdmfpgl
4-5 v: vvvmv
7-9 t: tttttthttt
3-4 r: rrhjr
5-8 m: mmmxmmqmmmcssnmm
10-11 g: gbgmggggwhnggsgvgf
6-7 m: mcmpkmm
2-3 b: bzbb
5-14 w: wwwwbwwwwwwwwdsw
4-17 x: wctqhkgxmnhwlsvpx
6-9 x: jdkxxdxhx
4-6 k: hpkskbkgjkkckh
8-10 h: hhhhhhhphphhhsh
10-11 p: ppppjppppzjppppp
12-15 f: ffmphffwxwjzfsfbs
2-4 n: nnrn
5-7 b: zbrbmbrbbbbbbbbbsbb
4-9 q: qhpbmrldqpvfk
3-5 b: bbbcr
2-13 t: ztkrtfqmcdcwsx
1-2 r: rxjrnrrrrtprr
2-5 h: rhbdcbp
1-2 z: zzzz
2-5 x: xkgxxxqtghn
2-7 z: hzrtbqzbfgqzd
5-6 j: jjzjmj
9-14 q: hqtgqqsqqqqqqhnqvqqq
3-4 x: txmxcx
3-8 x: hszvqxxxzvgpjrtxtqk
4-10 p: plpzpppppp
5-6 v: dvvsbv
16-18 z: zzzzzzzzzzzzzpxzzz
1-18 t: tjtttcststttttvlttt
7-13 t: tbdttctqtxdfrtncntcl
9-10 r: rrrqrrrrrrm
3-13 n: fqgmfmtnxhnzn
3-8 l: scjrllgwmkkfgbr
6-13 t: tgttttzsftrrtqnqt
5-11 r: rdrqrprfrbrr
9-15 k: kkkkkkkkkknkkkkkhk
11-12 b: bbbbbbbbbbbb
1-4 n: nnnsn
6-7 t: xnttbttsnt
6-11 z: rzxzzzkwlnjzzrsswz
1-2 h: hrmfh
2-3 x: mxxx
10-12 k: jrqdmnxlsbkwgg
16-17 j: jjjjjjjjjjjjjjjjw
10-11 k: kkkkkkkhkgwkk
6-7 n: nnnnnrbn
9-11 q: qqqqqqqqqqq
1-4 c: cccq
13-15 p: pppppcpppkppppp
2-4 b: gbbbjdqdbz
13-17 p: pppxpbpppppphppppk
3-5 p: pplppp
4-5 q: qqqkpq
10-12 h: hhhhhhhhhhhmhhhhhhh
17-18 r: rxrrrrrrrrrrrrrrcq
4-6 p: ppppppnpp
3-10 r: rrrrrrrrrrr
2-3 b: qwrv
1-3 c: vfcgc
3-4 s: ppsrq
2-4 s: wpsvjxs
2-9 d: rjjdddcddjdd
10-11 w: hwwwnwwwwwsnslwpwwb
1-5 k: nfvnks
13-14 w: wwwwwwwwwwwwww
4-6 s: sssssks
2-6 m: mmmmmmmmmm
6-12 h: hhhhhhhhhhwvh
3-8 v: nvkvvvvrvv
9-14 x: xxxxxxxxxxxxxxxx
4-9 r: rrrxrmrkrrrvrr
2-6 x: qrpxzx
7-17 n: snnnnnzmvnsspnqlnznm
4-7 q: qqqqqqvqqnqqgq
7-8 v: vsvjvsvvvfvzv
8-12 d: kwzmmqfztrnd
2-4 n: nnjnh
5-6 f: zfwvfffvrfc
12-14 b: bcbbbztsbbbfbbbbbb
15-16 m: wmmmmzxmmmmmmmmhm
11-13 m: mwmdmmcnmmfmnmwmmm
1-3 n: znggn
8-10 x: xxxxxxpxxs
7-8 k: qkpspjkk
5-6 b: wjsbbbbmbbbq
9-13 x: xxxjxnrxhxlvpx
1-3 p: hpvp
5-6 z: qpsfzc
5-11 t: pdbtttftrtt
7-9 b: bbbbbbzbzb
6-13 g: ggggggggggggdg
8-14 h: hhhhhjhhqhhnhr
17-19 x: xxxxxxxxxzxxxxxxxxxx
5-7 s: sssssskx
5-8 p: phnjswphclpbtcwrdvp
2-14 b: jpvmgklvqjbclb
3-5 s: bkqmsvlfrhbtssmfrzlq
6-8 v: vvvvvwvv
7-8 c: mmlhlccr
11-15 f: hvvfvbpgwxfpxvfnb
5-7 n: pntnnsx
10-15 n: mlmrbrnknsncjddm
1-7 x: xzqswmkktxbvbcm
1-3 n: lndnn
7-8 d: dddmkddd
2-6 q: qggqqqzqq
3-4 z: zzszzz
14-16 r: nrrgprrxhrlrjxlr
2-14 p: pppppppppppppvp
7-10 x: trwjbxxwjqfhspxr
6-7 q: mqgpjwq
5-6 f: fffzgb
7-8 q: qqqlqtdqq
5-7 s: zsfkdss
2-4 h: thjwvg
2-8 f: zmrdffff
2-6 t: btrqbs
11-12 m: mmmmmmmmmmcm
7-10 w: wwwnwwbwwwwwwzwsw
4-6 c: khszwwltfcndcbd
12-16 v: svnxvvvrvvvvvvvvvvvv
2-9 d: dgddwddrnddscdchdd
6-8 p: dpmdzptwfcvkzbznwf
9-10 d: dddddddddvd
16-17 s: cqtsgqsbkjrldhlcs
3-14 w: dvrblqkwnnwwbww
13-18 n: nthqnkfgnnnsncnnnb
10-12 j: zlcjgjcpzjbj
10-11 r: rrrgjrrrrrfr
4-8 z: vnmzsbzrvcznhjhtn
10-11 r: rrrrrrrrrhqr
4-5 n: nhdnzd
10-14 z: nzzzzzzzzzzkzzzz
7-9 n: nzzwnnkqnn
3-4 h: hhhx
1-8 c: ccgqgmkc
4-5 w: wwdwrww
2-9 p: ncppppppfppszpw
12-18 n: nnnnnnnnnnncnnnnnnn
4-6 f: fhflfnv
16-17 x: xxxtvxxtjxxxsxxxx
12-14 h: zhsxhfhhhhhhvshpnvp
13-15 v: mvvnjgvvvvqnbvvvg
3-6 j: jgzwjpcjj
1-3 k: qcktkhnk
13-17 f: ffmfffffffffxffft
3-5 s: ssssq
8-16 m: zmzbmmmxmmcvphqc
4-5 l: llrsl
3-12 m: qmpfmmmmmbdjmmlmtpv
1-4 c: ccspqcj
3-4 l: qblll
12-16 h: mphhhhhhhhhhvhhwhkh
4-6 v: vvvrvbvnvv
12-13 x: gkxwxzwljjqxxxgwjgxx
1-5 z: zzzzzzzzz
1-14 q: ltqqqqqqqqqqqpqqqqq
8-10 g: ggmggjvghg
12-20 v: rttnvvvqvmvlnhsvvqtv
13-17 v: vvvvvvvvvvvndvvvhwvv
13-14 w: wrwwvwcwmwwwxwwwlwww
3-13 l: lltlllllllllp
7-14 d: bhcspvkdwdsmkdwnq
1-5 k: kkpkx
1-5 k: stkkk
1-10 n: ntbptczncn
5-8 m: rmffmrvcfzsqqmvvm
10-12 f: fffffffffsfff
3-4 g: kptg
4-9 f: fffffbsbf
1-2 v: dnvmd
7-16 f: ffffffdffffffhhffk
14-15 b: bzbbbbbpxbbblrp
11-15 c: chccccccccncccs
15-18 c: ccccccccccccccccctcc
17-18 v: vxvvvtcxvpvvpvmzxv
9-10 m: mmmmmmmmmmm
2-9 j: kjgfjfpkzwndlpk
3-5 r: jsqsr
8-9 v: dvvbvvvvp
8-9 r: rdrrrrrrrbjtmr
13-16 r: drddrrrmsrrdrjsr
2-5 f: fffffff
5-6 d: dddddjd
10-17 g: mgmhzjggdkxgskcsm
4-5 d: drddd
9-10 s: ssssssssst
3-5 m: nmmmgmmmmmmmmmm
2-3 b: vplpl
8-16 l: lllllblclllllllpl
12-13 w: twwwwwlwwwwfwwwwd
8-11 m: mmmmmmmsmmglm
10-16 j: jjbjwvjjjjrjdjfpj
12-13 n: nsnnnpnntncwnnn
2-4 f: vllf
1-9 f: pffffffffff
1-6 m: mmmmmmm
5-13 w: jxwwbwwrkwwwpwwn
10-12 q: qxtqqqqqqhgq
9-12 f: fffffvffmvffh
3-6 z: fjzjkz
2-8 k: pcnbqdtksnkm
8-13 l: lkxlllsnmlblclx
10-12 m: cwcnkcpdsnlm
7-9 g: gggnggghm
4-14 w: vxxwffpxrmpnwwqlr
5-7 m: wmfmmmm
7-10 m: mjmtmmjkmmmlg
9-16 l: cjzlllmllkfwlwcl
11-12 c: cccccccccccc
10-11 b: bmbfwmbbvxtvfb
7-8 v: xvvvdvvvv
5-6 l: llztlg
6-10 m: mmmmmzmmmmmmmm
3-5 b: tsbzvz
1-4 g: ggggg
5-9 b: bbbbtbbbbb
4-6 f: fffffvff
8-12 w: wwwwwwwnzwwpwwwwwww
2-3 m: mgmmmmmm
2-5 b: bbbbbbb
5-6 b: bbbbbg
3-5 f: fpfpl
6-7 b: bbbbrhbbbbfbl
16-18 x: xxxxxxxxxxxxxxxxxhxx
7-9 w: wprgxqlpw
5-9 k: qkxbkkkzp
2-7 b: bbbfbbkbl
18-19 l: lhlllllllllllllllll
4-6 z: zppzzs
3-11 j: nkcsvmhtklj
4-8 g: ggbrdwbf
10-14 s: qsssssssslsssssszss
3-4 z: zzzw
5-13 h: hhhhhthhhhhhr
11-12 m: pmmmmmmmcmmdmmgm
4-8 l: hclghxblsn
10-12 n: nnnnnnnnnhng
1-17 l: bxtlrzmsppwvvvwjp
5-12 d: xbsnsvbmdhddkwvdprw
18-19 c: lrphqjtncgftjfcjcctc
12-13 k: gqghmzhkhcskks
10-12 j: djjfvjjrjdjjjjp
3-6 c: cczcckcccccc
11-14 q: qrqzqqqqlqqqvt
16-18 k: kkkkkkkkkkkkkkkzkx
1-4 p: pptxfljp
2-7 p: pphdjqph
14-15 q: knfmcqptqwfnrqqqkm
2-3 k: zvlkn
1-6 s: ssqssxs
3-10 v: zbcbpfqrvsr
3-4 v: wcvlghhv
18-20 l: lllllllllllllllllbll
13-14 k: kkkkkkkkkkkktrk
4-11 c: lccmccccfccccjc
2-12 v: vbfdvsmtvkmvbvvvtvn
3-7 m: nmmcmmnm
13-18 k: qfqckkkkqfbtjkkkbp
8-9 k: klkkkkvkskspk
3-15 j: qnvxjqsgrpwjzkjzkp
1-4 s: qgss
16-18 w: wwwwwwwgwwwwwwwwwc
5-11 j: sjjjjjjjjnp
3-6 p: pppppmpppppppppppppp
5-12 r: rrmjskttjhmrs
4-5 f: znfkxthpffjj
1-19 z: zzzzzzzzzzzzzzzzzzbz
13-15 s: ssssssssssslpsss
4-11 c: whzhrcbhbccc
1-8 m: mmmmmmmfmmmmmmmmmmm
2-10 j: bjhbfqcjrjhwkk
15-20 k: bnqkkkmvqnfcklzdjgkk
7-9 r: hrdrrrrrprrrr
3-9 n: jrnbzzmnnxln
11-14 f: vfnpfrbzpjqffrjzf
5-6 f: ffsmpbl
2-7 z: sznjwfzs
7-8 t: ptttttttt
2-4 d: rsnd
3-4 r: cvbpvktkrr
2-3 j: jjjj
5-7 z: qnjwzxz
2-5 z: fcbmzbs
1-9 g: nhgggmggwggngf
7-10 l: xmjdbtmhrdjlltjcglxp
7-8 b: bbbbbbwjbbbbwb
2-4 h: mhfhnjl
5-14 k: hqkrkvkzkhrknzjphc
7-12 b: bbgbbbbnxtcgbbb
12-18 t: tmtxtkpmqtvmttwcfgn
2-16 b: lbbnbxfrbtjltvpnnbmn
1-5 c: gvcchczcckcccnc
10-15 d: kngtvjwdvdktvdq
8-13 g: ggggcggggggggnlggt
5-7 n: nnnnnnn
2-4 n: nnfnxl
4-5 h: fbmdghjth
14-18 q: qqqqqqqqqqqqqqqqqhqq
7-13 l: llgldxlxrkklbc
15-17 w: twwwxwqwwwwwwcfww
16-19 f: fffffmdrbfffwfffnnrf
2-14 m: lmhhfmsjrrtgrm
7-11 v: hrvtwbvbwqvb
4-7 r: rczrrrrr
12-13 h: hphhhmhhhhnqh
7-12 x: xrqdxpxhfqrtgxh
5-7 t: fdnrnktrgtkvckpxbth
3-4 v: kvvr
1-2 g: pggxggfwcwzqpdbr
9-14 t: ttttttttdtttttttt
3-4 v: rvvlzwxxmwvv
7-9 l: llllllflmllll
3-5 z: mzfrh
3-4 n: znmlnnwn
5-12 j: mjrhqkdtsjpmv
1-7 p: pppjntrrnzzdtv
3-4 k: prmx
3-5 c: ctccc
3-14 n: nnlxnnnsnnnnnxnnnnnn
2-5 m: mtmmmmm
2-3 j: wjjj
9-13 r: nbtrwwkrrhqpvqmgjrxf
2-13 r: lmfwxcxrcvxxrhd
11-15 h: hhhhhhhhhhhhhhvh
1-6 l: hplllslzlfl
2-13 v: xvvvctvxwcvmmwm
3-10 s: sxpssssssmkss
1-3 t: cltdrntxw
1-2 b: sndbw
1-5 r: prxrrrr
4-7 f: gffffgjf
8-10 k: nkkcrkkkkk
2-4 s: sssbml
6-9 f: pfffzfffwwfpfffk
5-7 l: mxlllvl
10-13 d: dddddddddcddkdddd
1-8 q: qqnqqqqqqqqq
2-6 v: nvzvfv
13-15 r: rrnrrrvrrncrrrhr
2-5 s: jhsss
10-15 v: vvvvvvvvvmvvvvvv
2-4 n: cnpvsknzhqwpc
2-8 z: ctxqmpzd
8-9 x: txbtbrzxxxvx
3-12 j: jjjjjjjjjjjjj
5-6 v: vqdsvv
4-5 c: ccclc
6-8 m: dptrqlwmfcbmskjw
4-14 s: sxssscssstsssssssbss
3-6 q: xhqwqfpprqm
14-15 f: ffffmfffffffffkbffff
3-12 f: msjmmfrgrlzvtss
4-15 c: mwwcfkcchhcpkhg
2-3 v: hvpnbcvxgqn
16-17 d: cddssdpzdjdfkddht
12-13 v: vnvvvvsvvvvvr
4-5 r: rrrrv
7-9 g: zgzvgfgtg
3-4 w: zwwh
16-17 h: gvhgvhhhhcgkkdqhhhp
10-11 b: bbbbbbbbrzzbbbb
13-14 v: vvvvvvvvvvvvbv
10-17 k: dhkczrpkckdbjthkkb
6-7 q: qqqqqdkqqqq
10-14 b: prnmgmmtjvjvfbnbbvg
8-13 w: wxwswrqbwmmwwgw
14-15 f: ffffffffffcffzff
3-8 l: llxltlll
3-18 b: brbpdcgjcmwqlvkncn
2-3 m: xpmmm
6-7 r: rqmcgkktsrrkrflgftg
3-15 s: wpcztpfsfpskfbsczm
1-4 c: cccccc
11-15 c: gcckqcgcccccncccccc
1-3 c: cscjpjfcdbpfpbrvv
18-19 x: xxzxmxjwxxxjhxxcdhf
6-8 j: jjspjsppjzqhvrgdvdmj
4-5 f: swffn
11-12 m: dmfmgmwmmhkw
15-18 m: mmmmmmmmmmmmmmmmmmm
7-8 l: llvlllllll
3-4 j: jwgflnp
1-7 v: dvvvvvvvvv
5-7 l: lwmfxbvx
3-10 f: flnqlvbfgff
2-12 s: stssbsssbssskstsfs
10-15 g: rjnsggtjgpqxshbg
7-8 j: jjjjjjjj
1-3 b: nbbbb
5-13 b: bbbbbbbbbbbbbbbb
14-17 h: hxhhhnhkhghhhhhhhhjz
9-10 b: wbbbbbbbvg
1-7 d: ddddwddddddddd
5-9 b: jgdkhbdbphbs
10-15 w: mwgqmhjzwwnvfwwwm
7-8 h: thhhhhhhhhhrfcphh
3-7 z: khczzxz
4-6 l: lllhlh
2-5 m: psmmm
4-6 x: lrchzhqxxrrxvr
13-16 q: qqqhjqqcqqqlkbqq
3-9 f: ffffpffffff
5-6 k: kkkkgkkk
3-4 v: vvll
5-9 x: cpbvxwxfhpxrxcxxnq
8-9 p: pxppvnlpp
1-2 l: llxl
13-14 h: vhhhmhhhhhvsmhhh
12-14 b: bbbbbbrbbbngbbbkbbb
10-11 r: rxmdszrtrrj
4-5 n: nnnnnn
2-4 x: xsnx
11-14 z: wqzpjzzzzpzzlczzsj
1-8 d: lddddddd
4-6 v: cvvkfvv
6-17 f: ffgxddfflfgfffxbf
1-7 x: bzvxhxr
4-16 w: wwwjwwwwwwwwwwwhww
15-17 l: lllllllllllllllll
1-19 v: vnbzvvjvfdvvvvvvvmt
2-5 t: tpttzs
12-16 f: ffffflfffffsffftfff
5-10 g: hbggtxxgvkfg
7-12 s: jssssgtjfcrsqsx
1-4 p: pppg
4-5 w: wrwbw
7-9 r: rxrqnxqrrtrfrrr
8-9 b: bbbbbbbbbbbbbbkb
5-7 d: ddddddddd
1-4 x: xxbfxxxjx
9-14 w: svwwnzjtqczwww
15-16 f: ffffffffffffffbff
8-9 h: hhhhhhhhh
11-12 g: ggggggggggtmg
15-16 g: gggggggggggggpxgggg
9-12 d: lctfgcddhddbfsxrpl
8-9 w: fszlzwzwwwdwzqf
10-16 v: vlvvvflvvmdvkvvvvvs
9-11 x: xxxbxxxxdkx
9-10 n: nnnnnnnnnt
10-15 b: zbwzkjvqbtbnbkbb
13-18 w: wwwgwwwwwwwwswwwwww
3-4 n: ndgg
14-17 k: nkmkdssxzkkkfkkkqflk
2-13 z: prmtcfxgldwcpr
10-15 f: xfffffzfwpffffhpffg
6-12 t: tttttttttttz
1-3 b: bbbbbb
2-4 q: qqqhqqq
4-8 w: lqzqwwzwpcwwwlr
15-16 f: fffgrffffffffvfc
12-13 p: pjmpppppppppv
6-14 h: zhgxlgshnfdhrl
17-19 d: qgwtdjcvzdpsphppdmds
15-16 f: xfffffffffffcrfj
5-6 x: xzxgxxxn
4-7 f: fljffdfbrxklbf
8-9 t: btnwxkrtt
9-10 g: bggsggggfgpgg
2-4 p: ggpsbgdx
2-6 c: cxscckkc
3-11 q: qplqztgmdjqbqvqg
5-9 g: ggggtgzggglgdg
8-16 c: ccccccccccccccccccc
1-5 d: dhdddddd
3-5 c: qjcdz
3-6 b: bwbbbbbhbbzbbbb
1-4 f: qfffff
9-11 l: tllxbrfzrzl
3-4 v: vvvv
3-7 z: dzzmpbvvw
11-16 r: rrrzrrrvrrzwrrrr
4-5 z: kzqvzzz
3-9 n: rtnnpmnnsnncnqdnnn
5-7 t: htmghwktpdttt
5-6 s: sssssss
1-4 r: crpwcrrrrr
4-9 t: tttjttttttt
2-3 t: mrgt
13-17 x: xxxxxxxxxrxxmxxgv
2-11 m: mmmmmmvmmqmmcljbkt
1-3 l: lwlg
5-7 z: zzzzzzkz
3-6 f: wdfwlk
4-6 v: hjfvcv
1-3 h: hhdhchjvh
3-12 m: hlkbjrmjbkps
5-9 t: ttthqdchttvjk
9-11 p: wpzjpbpppnpqwpk
4-5 v: lvvkwv
12-15 k: kzkkkkkkkkkbkkkmkkk
4-8 h: hnhfhwhslhhhhdhg
1-7 d: dddddddd
18-19 g: gggggggggggkggggggx
4-10 v: vvvbvvvvvnvvjv
5-9 v: lvtqgmvmvvlv
2-3 d: pdkrdzdlvdn
1-12 s: sssssssssschs
7-9 c: ccmkscwccwql
3-5 s: sksdr
6-7 p: pppptjgppp
13-14 m: mmmmmmmmmmmmmwm
3-5 j: jjdjfj
2-5 f: dbxfrv
1-11 w: wwzdqntxzwt
5-9 b: bbjwffgmbmxb
12-13 b: bnbfblrbznqbg
1-4 s: vlbsdkslvk
1-12 k: kkkkkkkkkskkkkkkks
3-4 f: ffcg
4-19 w: zlfvsqxthvvtmwrmrwj
6-13 r: bsnqgqrsrrjjnthdr
1-5 t: bctbqbtft
4-7 v: vvvvvvqvvv
2-5 n: jnqnn
2-4 r: rrrr
11-13 w: fbwmwjgvjlnpwvwmqf
12-13 v: vlvskvkjrlsdv
17-18 s: sssssssssssssssshsss
7-18 q: qtqhbqxkcqqxnqwlqdqq
5-9 q: jfqrdgtqpqs
7-13 j: jjjjjjmjjjjhgjjjjjjj
1-3 z: gzzzl
9-10 g: gbcgggnggg
1-2 d: ljdddddbdddddd
2-4 t: ntwf
8-13 p: ppphpppcpppppppppp
7-11 b: bbkfbbbbbbdbfb
5-7 z: dtgtzzzkxdp
2-3 w: vwtbc
11-12 s: bssvsssssssnsssbs
8-12 w: wqwpwcxwgpww
8-9 p: mqpcvlspwx
2-6 w: wwskwkwww
6-17 w: gbwwnwrdvnnpfxhgl
12-13 h: hhhhhhhhhhhht
14-15 b: bkbbbbxfrzbbtbbbb
1-11 j: jvjjjjjjjjgfj
1-11 k: tkkkkkkkkkpk
12-16 q: frqhkwxwjbqqqqqq
7-10 f: fjkvvhffrhfgfrfffl
14-15 n: kfnnnvsvnhqncqm
4-5 v: vvvhx
3-10 r: qwrscrfwgtntntzsvnhg
2-11 r: wfdmkrcbgzbrzs
4-12 p: smwpnnjvrpvzmpcfprw
5-11 k: kkkklkkfkckw
2-4 g: gbgq
4-6 d: pdvsvkddd
1-5 h: hxhhthhhbbhlshhwh
10-13 n: knnnnnnnnnnnnnnpnnnx
3-15 m: mmmmmmmsmmmmmmnmmm
3-6 d: bddvdd
11-12 p: pqppnmlpppvp
1-3 w: swwvdwqwwwc
11-15 d: dcxdddddwtldddddrpnd
5-10 m: mmmmxmmmmxmm
4-6 g: qgggtgggg
8-12 h: zpvzbbhsstnv
5-6 x: nxxxvxxx
2-8 h: hhpqhhhhzh
14-17 v: gzvvwvvvvvvvvnvmpv
2-14 h: hxhhhrhhhhhhhmhhhhh
11-13 g: sggggggggggvzvg
1-3 d: dtdd
1-11 g: gggggmgggggggggs
10-11 j: bbmwbjhgnjjjwp
5-12 g: grtgxgnpsgwvnkd
6-8 p: lpvspswp
6-9 q: gfqfhrqccljrr
12-13 g: lsscgbnmxpggzj
6-9 x: kjvxpzxfdxmwcx
2-4 c: gcnr
1-5 z: zzzzz
7-19 c: lcjvqcmldcvwgnpjcsc
1-3 p: pppmpg
11-17 v: vvvvvvvvvvvvvvvvsvvv
4-5 l: llwwl
2-6 n: nnnhnn
3-5 c: cgxcp
2-5 p: plpptpphp
4-7 b: bbbbbbw
3-12 r: rcrfzgzwxcbrxfpd
16-20 h: hzdhhjphnmmtrftftzsh
6-7 g: gnqbvgjfxt
4-5 d: ddddf
4-9 w: wwwwwwgwnwwwwwwwlww
11-13 q: qqqqjqfcqrsqzqqq
4-5 l: nltls
7-10 n: nnwnnnnnnnjndf
3-4 s: xsnqs
13-14 c: ccccccckzccccscc
5-6 z: zzzzzczz
3-5 j: vjjkjcc
5-6 x: xnsxvxxl
13-15 l: llllllllllllllh
7-8 b: jmbgbvcrb
1-4 g: gbgggg
9-11 z: zqdlwbzjtsz
1-4 x: xxxwbxx
8-10 w: wwlwwwwwww
4-5 z: zchzztz
1-2 m: ckjdmmm
17-18 j: mhjzljjjjnzjjjjblpj
3-9 b: bswsbvdld
3-9 m: fsmmcfvlmbztcxkqs
16-17 c: ccccccccncccccchc
7-14 z: zzzzzzzzzzczzzzzd
4-16 z: zzdwzzzkzzzzzzztzzzz
11-18 p: ppppppppqppppppppl
2-3 f: zfrbzsh
2-9 w: tllwlmbkwq
1-7 h: hhhhhhhhhb
11-19 p: hmglpprgflrqmpxjbpw
10-13 p: rppppppppdppfpp
1-4 g: xgkgkp
4-6 k: zkkkshvkk
2-4 n: kclktpqlmmpwmlw
8-9 b: qbbbbvbtqbbj
3-4 w: wwwj
1-8 w: wqwwwjww
6-7 v: vvvvvkvv
4-6 f: nrjwffh
4-5 h: fhjzl
5-6 b: bbbbfbbbbbbb
4-7 v: vvnddvvwcxrvvfkvhv
4-12 t: sbktxtktxlvcpltntc
11-13 x: xvrtdxxxxtrxfxvq
3-4 l: llllll
7-8 h: hhhhhhhgh
1-10 z: zzzzzbzzzzzfzz
3-7 w: wwwwwwww
18-19 d: ddddddddddddvdddddx
5-6 l: pllkmsplcllllblll
7-12 g: ggggggjgggggggk
10-11 p: prppbcpppzpppp
4-5 v: kjgfvrt
3-8 k: kvkkkzkkrk
6-13 p: cchphppnshjpgvh
3-6 j: jfhjjlj
7-11 v: vgvbjwvvrgcvvvcv
5-12 b: bbbbdbbbbbbmbb
2-4 b: cxbb
8-13 h: hhhhhhhhhhhhhhx
6-13 w: wcdnrrhwzzwpr
1-4 x: hxgp
9-17 t: xttgrkxptthzljhwl
10-11 w: sqhzqtswsww
2-3 b: cbrbbpbb
1-6 m: mcwmmlxn
9-11 b: qbcjjjstpslqzwbkbp
12-14 n: nnnbxnnzhfvqrclnj
6-12 z: zzzzzvzzzzzfz
17-18 j: jjjjjjjjjjjjjjjjdjj
1-3 k: mkbk
4-6 j: znrjhcjjlj
6-8 b: hmsbbkcwfvbbnb
2-8 w: wwwwwwwwf
4-6 v: pvvqrp
8-10 h: mbhzhhhhhkhhhhhhh
5-6 l: lllllmll
1-11 p: nppppppppppp
11-13 b: bbbbbbbbxbxbb
4-6 l: llljll
1-4 x: xxxxx
2-6 f: fffffm
8-13 k: rnqxkkjkmkkqskkkk
3-4 w: dqdsbb
6-7 c: cccvctzcczmcccb
10-13 k: kkkkkkklmzgkk
9-12 d: ddddddddcddddddd
7-9 v: frvvvgvftksnvpvw
13-15 d: djdddddddddddddddd
6-8 t: tttttftrt
5-6 w: wlwwwrww
5-8 m: mmmpnpnpcmj
2-5 p: kpkppp
10-11 d: xtlgxpfjdczm
6-7 v: vvnvrvvvvb
8-10 g: ggggggglvgggg
8-15 g: kngggxggggggggg
1-11 n: ntngqrhnnrxjnnnnwxnn
2-5 k: kckkbpkvxhkz
1-4 j: jtjjvjjjj
15-16 x: xxxxxxxxxxxxxxxdx
7-8 r: rrrrrzxp
5-13 m: zzrqmklcmhdmrzfz
6-14 h: hhhhhphhhhhhhhh
4-6 c: brchqcqwcwcthccc
16-17 v: zmvqhnjbhvvrbrpvv
9-12 p: pppppqvpkppsnppp
1-4 d: mddnd
3-4 s: sssj
2-3 d: kmbsdpqkhj
11-15 f: ffffftxbjdfffcnfcf
8-15 t: tgptpthtttqctbt
1-3 l: wfklr
14-16 m: trxpshgmzpwmsmcmzgwk
5-10 m: jdhjmlqrqmmpfqsj
3-10 t: tctnlmvvtt
9-11 c: vxzmqpcxgnc
4-5 z: fzszzbzhsfxcmh
1-3 d: dddddd
3-9 j: mhjkjljjjz
14-17 n: nnnnnnnnnnnnnnnnh
4-5 l: hjspb
4-5 z: bmjvtmbq
4-5 v: vgvvvvv
3-8 x: mgzfmbdxxgm
5-6 t: tdxxqj
3-10 m: mmlbmbrlmmmmb
12-13 b: bbbbbbbbbbbztbbbbb
4-7 d: dtbdbzdsnkgbvsswdhww
7-8 c: cckccccg
1-6 r: jzqrrrdrtsrlrrvmr
4-5 x: xxxqh
3-6 v: vvvvvcv
6-8 x: xbxxxxbxfgxcnxgb
10-11 n: jnbnnnnnnbnwdnnn
5-8 h: hhhhvhbh
3-8 x: xfgksplxx
4-9 x: xxxxxcxxhxxx
4-7 l: lsplglldx
12-15 d: xxcqmmwdddzttkdldrml
7-8 w: wwhnlwfw
4-5 t: nvtdt
3-4 q: qhqr
3-7 j: jjjjjjjjhj
8-10 x: xlxxpxxsxxxxdx
3-8 k: kflkcvvthxkkkczlh
6-7 v: kfvlgvn
16-17 f: ffffffffffffffffrf
6-7 z: zzczzhfvnz
1-4 f: fsfs
6-7 b: pqbsmfsv
2-3 n: nlbn
4-5 b: bpbdbbbbnsbbbxb
4-12 n: csgndnqnsjjvxn
13-16 r: rrrrrrzrrrrrqrrrrrr
13-15 t: ttttttttttttttqtt
1-8 q: qqqtqtql
6-10 t: ttttttttzwtttttt
4-10 v: vvvsvvvvvqvvvv
3-5 w: wwwtl
3-8 z: qgkszmzkp
4-6 w: wwwvwgww
13-15 v: nvvvvvvvvdzvvvr
10-11 v: gvmwdpgpvvb
4-11 g: gggjgggvmgggg
1-6 z: fzzzzzzzzz
6-9 r: dmrrhxrrbrr
1-2 t: tmttv
5-13 d: jxvctbwmkpbqd
6-8 w: wbwlhwdw
17-18 x: qdzkpnhbdxcxsfsxkx
3-5 w: wwwww
2-3 n: xnjnl
2-9 f: ffffxffff
5-6 g: sgggjzg
7-10 h: zlvnhhrhlz
10-11 h: hhhhhhrhhhhhh
6-7 k: kpvtkkkk
9-17 b: vbbwhjntdzhbbhmbbq
6-8 h: hlhhhhrdjncphc";
        #endregion

        #region Day 3
        public static string Day3 = @".##.....#....#....#..#.#...#.##
.###........#.##....#......#..#
#..#..#.....#...#....#.#.......
.........#.................#...
..#.......#.#.......#.......#.#
.####........#.#..##.........#.
........#.........#.........#..
#..##...##....#.....##......#..
.........#..............#......
#.........#...##.........#.#...
..............#........##.....#
##....#...........#....#.#...#.
.....#..#.....#...#.#..........
#.......#...#..##........##..#.
.#........#.......#............
.......##.....#.#.#..#.#.......
..#......#..#....##......#.#...
.....##....#..#.....#..#.......
.............#.......#.#....#..
.................#.#......#....
.#..#....#..........#.....#.##.
#.#.#.#.....###.......#.....#..
#...#..........#..#............
...#...##.......#.##..#........
..#...#.#.##...##.........#.#.#
.....#...#.#....#.#.....#......
...#...#.#..#...#.....#........
...........###.#.......#.#.....
..#..#.#........#.#.......#.#.#
.#.......#...........#.........
.#..#...##....#.......###..##..
#....#.....#....##..#.........#
#..#.......#...#......#.#....#.
......##..#..#....#.#........##
#.....#.........#......#..##..#
.#..#.......#....#............#
....#..........#.#...##....#.##
..#...#.#...#.###.#..#......#..
#.#...#..............#.......#.
..##.......#......#....###.....
......#.......#.#.##.#..##.#...
.#......#......#.#....#..#.#..#
....#....#..#...#.....#.#..#...
.#.....#.#.#..#........#.#.###.
#..#..#.#......#..#..#....#.#..
..#.###....#....##...#.........
...........#..#...........#....
.................#..........#.#
.#.#....#..#........#..#.......
...........##..#...............
...#.##.........#.........#.#.#
........#..#....#.#............
...##...##..................#.#
...#..##..#...#......#.....#..#
.##.#..#..#......#......#.....#
....#.....#....#......##.#.....
.....#.##....#...#.............
......#...#.....##....#...#..##
...#............#.###....##....
............#.#.#...#.#........
#.....#..#.#..##...........#.##
.....#.#.#.#..##......##.#..#..
.#.##..#.........#......#.....#
.#.#.#.#.#..#..#........###....
......##..........#.#.....##..#
..#...#..#.....#.#.....#.......
............#....#.............
........#...#..#....#.#..###...
#........##....##..............
.........#.#.#..#..#...#.#.....
....#............#....####...#.
##.#.#......#.....#...#....###.
...#..#..#..#.......#..#.#.#..#
..#..#....#...#.##..#.........#
#..#.#....#....#...#..##..#.#..
...#....#.............#..#.#..#
..#......#.##...#..............
#....##.#.#...##......#.....##.
.#...##...#...####.....##......
...........#.###....#...#...#..
##..#..##..#..#.#.#..###.......
.#...##......#........##..#....
.#...#...#.....#............#..
.#.#.#...#.#..#.......#......#.
.................#..#.#......#.
#..#####......##.#....#...#....
........#......#.....##......#.
....#.#...#...#..#.......#####.
.....##......#...#.......#....#
.#....#...#..#...#.#...#..#...#
....##.........#.#...####.#....
...##..........#.#.......##....
.........#......#.....#....#...
#....##..#......#.....##....#..
...#.#.............#...#.....#.
...........#...#.#....#..#.#...
.......#.#.#.....#..#........#.
..##.....#..#.....##..#........
...#.#..........#...#....#.#...
..#....#......#...#.#...##..#.#
.#...#..#..#..#.......#........
.................#.#...........
...............#......##.....#.
..##.....###..#....#.........##
....#.#........#.####.#...#....
.....#.....##..####..##.......#
.....####.#...#......#.........
........#..#......#.....##.....
...###..#.#..###.......#.......
...#...##..#..#..#..#.##.......
..#......##..#.....##..##......
#.......#.#..#.................
#.......#...#..###....#.#.#.#..
#...#.#....##.##.#...........#.
.#.........#..###..#.........#.
#...#......#...#.#.........#...
.#.##..............#.#....#...#
........#.....#..#.#.#......#..
............####.#......#......
......#.#.#...#.#...#.#.....#..
....#....#...#.....#.......#.#.
..#....#....###..#....#.....##.
.................#.....#.#....#
.#.............#......#.##..#..
#.....##.......#..#.....#....#.
#.#......####...##.....#....##.
.....#.#....#..................
....#....#..#.#...........##...
...#.............##......#..#..
......#..........#...#...##.###
...##......##.....#......#....#
........#.#.#...#...#..##......
......................###....##
.#.....#..#..#.##.#.#.#....#.##
.#..............#.....#......#.
.#...#.##....#.....#.#.#..#..#.
##...##.......#.....#..###.....
...#..#.#....#........#........
....#..............##...#......
...........#..#.....##.#.#.#...
#.#.....##..##.#.#........#....
.........#....#.....#..##.#...#
...#...#..#..#.####...#.......#
.....##.....##.....#......#....
#.##...#....#............#..##.
.#.##..#...#....#.#......#.....
..###................#.........
.#..#..#................#......
....#..#........#..#....#......
.#..........###......#...###...
...........##...#.#..#.........
...#....#..........#.....#..##.
..#..#.............#......###..
#....#....##.....#....#.##.....
......#.......#..#..........##.
#..##.#...#.#.........#....#.#.
...#...#..........#...#..#....#
...###..#.#......#.##.#####...#
..#.....#.#..............#..##.
#..###......#.#..#........#....
.#.......#.......#.....#.##....
.#...##..#.......##.....#....##
..........#.#..#.....#.........
.......####...#...#.....##.....
......#.......#.......#..#.#...
...##....##.#.......#.##......#
.#...#............#......##....
#..#..#...#.#........#.........
.......#.......#.....##.#......
.#....##...#....#.........#...#
#.#....#.....##...........#..#.
.....#......#....#......#.#...#
.#............#...#.#....#....#
........##..#..##..##.##....#..
........................#.#....
#....#...#.....................
##.#.............#.....#...##.#
....##....###.......#..........
..#.#..#.#...####.....#.....#..
#.........#.......#......#..##.
.#.#.............#..#...#...#..
#..#....#....#..##.........#...
#.#.....#.##.#...#.##..#.#..##.
......#......#.###....#..###...
.##...#.......#.........#.#...#
..........#...#....#..#....#...
.....#...#.....#....##....#.#..
#....#...........#.#...#.......
.###..#........##..........#...
....###.##..#...#.#..##......##
.#...#...........#...........#.
#......#....#.##.........##..#.
.#.......#........#......#.#.#.
.......#..##.........#......#..
.#..#.....##....##....#.....#..
#.#.#.....#...#......#.........
..............#.#.........#.#..
....#...#.............#.#......
..##.#............#.#.##....#..
.....####..........#.#....##..#
......#.#.........#.......###..
#....##.#...#.#...........#...#
.....#...#......#....###...#..#
#....#..............#...#......
...#..###...#..........#....#..
#......#..#.#.#......#..#...#..
................##......#..#...
....#..#..#........##..#...#...
...##.......#.##.#.....##...#.#
.......#.##.#..#.....#...#.....
......#........#..#......##.##.
....................#.....#.#..
.##....#...#...##...#.........#
..#...#..#.##..#.#.#......#....
#....###.#..#..#...#..#...##...
#.......#.....#.#.......###.#.#
.#.##...##..#......#....#...#..
#.....#.......##..#....#.......
...###...#............#....#..#
.#....#.#...#..#..#.##.#.#.#...
#......#.#..#.#.#......#.......
..#..#....###.#........#..#.#..
.......#......##.........#.....
...#...###..#..#.##.#..##......
.#.......##.......#..#..#.###.#
.###.#..#.###...........#......
...#................#.#...##..#
....#.###....#.......#........#
.##...#...#..#.....#...#.......
.#...#..#...........#.#......##
...##..#.#.#..#.#.#.......#....
.#.#..#..#.#...........#.......
..#....#.#.#.#.#..............#
..##..............##....#.#..#.
..#....#...##.....###.....#.#.#
#....#......#..........##......
.##.#.#......#...##..###..#....
.#...........#.##.......##..##.
###.....##...#.##..#...........
...#.....#...........#..#.....#
#.........#....#.......#.......
.#.#...#.###....#..#...........
.....#.......#.....#.##.#.#.#..
..##.#.........##.........#..#.
.......#....#......#.........##
...##.....#..#.......#..#.#....
..#...###.......#..#....###....
.......#...###......#.#.....#.#
#....#...#.#....#.#..........#.
........#..#.....#.#.#.........
......##.......###.......#...#.
.........#..#..#.......#.......
#.......#...#.....#.#..#....#..
.##....#..###.............#....
#.#...#.......#.....##.#.#....#
....#....##.#........##........
...##...#.#.............#...##.
##....#.....#..#..#......#.....
#...#.#........##....##......##
..#...........#..#......###....
..##..#.....#......#....##.....
....###.#...#......##......#...
....#....###...........###.#..#
..#....#...#.##....#...#.......
....##...........#............#
..#.#......#......#.##.#...#..#
#.###.............#.#.##.#.....
#....##....#..#.#.#...........#
...#...................#.......
.#...#......#.......#.#....#..#
....#...#..#..#..#.#.....#....#
..#....#............#..###..##.
...##...#...........#..#..#.#..
..#..#..#.........#.........#.#
...#.#.....#.#..##.........#...
....#..........................
....#.....#.#...#.###.........#
....#.#.......#..#.#.#...#...#.
.....#...#..#.....##....#.#.#..
#....###......#..#..........#..
.#.....#......##.......#...#.##
...#..#.....#.#.....#.......##.
............#..#....#...#..#.#.
..........#.#..#..##...........
.......#.......#..##...##.....#
....#...##.#..#...#.#.......#..
....#.#........#...####...#....
#.#.............#.............#
.#.#......#....#..#..#.....##.#
#..#...........#........#.....#
#....#....#.#..#.#....#.#...##.
....##...##...#...#...........#
...#.#..#....#..#..#..#........
...#..##..#........#..........#
#......#.##..##.......#..#.....
..#...#......#...##.#..........
.###.#..#..#........####...#...
#..............#.#.#........#..
..##....#.......#....##...#..##
.##...#..#.#.....#..#.......##.
..#.........##.......#....#..#.
.#..#...#..##.#..#.....#.......
.#....#.........#..#...#...##..
..###..######..#.##.#....#.....
....#..#.....#.............#..#
...#....#.......#..#.#.......##
.....#......#.......#..##...#..
.##..#....##..##......#...#..#.
......#......#...#...###.......
....#.....#.###..##.....#.#.##.
.......#....#...#..#..#...#.#..
...####.#...#...#.#...##....#..
......#.#....#....#.#....##....
#..##...........####....##.#...
...#...##.#.......#.#..........
..#......#..#..#...#......#....
..###..#.....#..#.#.......#...#
#........#...##..#...#....#....
...#.#...#.....#........#...#..
...#....#.###...#..#...#..##.#.
.....#..#..#...#...#..#........
..#......##...............#.#.#
.#...###.#....##..........#.#..";
        #endregion

        #region Day 4
        public static string Day4 = @"byr:1985
eyr:2021 iyr:2011 hgt:175cm pid:163069444 hcl:#18171d

eyr:2023
hcl:#cfa07d ecl:blu hgt:169cm pid:494407412 byr:1936

ecl:zzz
eyr:2036 hgt:109 hcl:#623a2f iyr:1997 byr:2029
cid:169 pid:170290956

hcl:#18171d ecl:oth
pid:266824158 hgt:168cm byr:1992 eyr:2021

byr:1932 ecl:hzl pid:284313291 iyr:2017 hcl:#efcc98
eyr:2024 hgt:184cm

iyr:2017 pid:359621042
cid:239 eyr:2025 ecl:blu byr:1986 hgt:188cm

eyr:2027 hgt:185cm hcl:#373b34 pid:807766874 iyr:2015 byr:1955
ecl:hzl

iyr:2017 hcl:#7d3b0c hgt:174cm
byr:1942 eyr:2025 ecl:blu pid:424955675

eyr:2026 byr:1950 hcl:#ceb3a1
hgt:182cm
iyr:2016 pid:440353084 ecl:amb

hcl:a4c546
iyr:1932 pid:156cm eyr:2034 hgt:193 ecl:zzz byr:2025

hcl:#ceb3a1 eyr:2020 pid:348696077 hgt:163cm
ecl:hzl
byr:1921 iyr:2016

ecl:gmt eyr:2031 iyr:2018 byr:1971 hgt:152in pid:454492414
hcl:z

hcl:#341e13 byr:1921 iyr:2020
pid:072379782 eyr:2022 hgt:166cm cid:253 ecl:brn

ecl:blu hgt:75in byr:1954 eyr:2026 iyr:2012 hcl:#623a2f pid:328598886

byr:2004 eyr:2035 hcl:#7d3b0c pid:359128744 iyr:2020 hgt:65cm
ecl:#70f23f

eyr:1988
pid:171cm byr:2003
iyr:1984
cid:50
hcl:z hgt:66cm ecl:#7a4c6e

pid:9632440323 eyr:1964 hgt:63cm
ecl:#fab0c5 hcl:z iyr:1945 byr:1986

pid:936403762 ecl:#337357 byr:1997
cid:196 iyr:2020
eyr:2030 hgt:165cm
hcl:#7d3b0c

byr:1931 pid:488791624 hgt:169cm ecl:blu
eyr:2029 hcl:#fffffd iyr:2013

hcl:#733820 hgt:76in pid:517689823
eyr:2028 byr:1988
ecl:brn iyr:2016

eyr:2023 hcl:#fffffd hgt:190cm iyr:2015 ecl:brn pid:739536900 byr:1951

ecl:brn
byr:1986 cid:262 hcl:#efcc98 pid:880203213 hgt:185cm iyr:2018 eyr:2029

pid:181cm hgt:113 hcl:z ecl:#2c2d2c iyr:1961 byr:2021 eyr:2031

hcl:#ceb3a1 iyr:2020
byr:1977
hgt:192cm
pid:338237458 eyr:2030 ecl:amb

iyr:1953 byr:2025 hgt:66cm eyr:1932
pid:181cm
ecl:#6f0b15 hcl:f79cb7
cid:109

hcl:#6b5442 pid:164cm ecl:blu
hgt:176cm byr:2015
iyr:2010 eyr:2029

eyr:2035
pid:085002665 ecl:#f88074 iyr:2018 hcl:#602927
hgt:169cm

byr:1958
hcl:z
pid:0468194841 iyr:2016 eyr:2007
hgt:152cm
ecl:#1c7a89 cid:124

hcl:z pid:233430735 byr:2021 eyr:2026
iyr:1953 ecl:#64769d hgt:184

hgt:70cm pid:156397147
iyr:2014 ecl:#d6ada0
byr:2030
hcl:#cfa07d

ecl:amb
byr:1990
iyr:2017 hgt:164cm hcl:10f33a
cid:293 eyr:2020 pid:332276985

pid:163252726 eyr:2026
hgt:163cm
iyr:2011 hcl:#efcc98
ecl:hzl byr:1936

hgt:157cm iyr:2019 pid:078770050 hcl:#efcc98 byr:1967 eyr:2030
ecl:gry cid:190

hgt:184cm ecl:amb pid:851379559 hcl:#ceb3a1 byr:1946 eyr:2022
iyr:2017 cid:280

hgt:171cm byr:1942 pid:830156471 hcl:#cfa07d ecl:gry eyr:2032
iyr:2022

byr:2013 ecl:#67cbe8 eyr:2024
pid:242908367
hgt:76cm
iyr:2025
hcl:796bda

ecl:amb iyr:2019
byr:1945 eyr:2021 hcl:#602927 pid:550065206

hgt:72in ecl:brn byr:1956 pid:253685193 iyr:2017 eyr:2023
hcl:#6b5442

eyr:2032 iyr:2019
hgt:176cm
ecl:oth pid:800237895 hcl:#888785 byr:1979

eyr:2026 iyr:2020 cid:226 pid:882830512
hcl:#866857 byr:1929 ecl:amb
hgt:60in

hcl:#cfa07d ecl:oth
iyr:2015 pid:807837948 byr:1966 eyr:2030 hgt:191in

byr:1969 iyr:2012 eyr:2024
cid:244 ecl:hzl hcl:#18171d pid:344160556

eyr:2020 pid:718422803
hcl:#18171d
hgt:181cm
byr:1925 ecl:amb
iyr:2019

byr:1943 pid:740807220 hgt:72in ecl:amb
iyr:2013 eyr:2022
hcl:#cfa07d

hcl:#733820
byr:1986 iyr:2016 hgt:184cm cid:333
pid:768188726 ecl:oth eyr:2030

eyr:2022 byr:1996 hcl:#341e13 ecl:hzl iyr:2015 hgt:160cm
pid:516401532

hgt:182cm ecl:grn pid:336742028 iyr:2014 hcl:#34f021 byr:1967
eyr:2029

byr:2030
hgt:142 iyr:2029 eyr:2040 hcl:426fc5
cid:312
pid:169cm
ecl:#069ff7

hgt:169cm ecl:gry hcl:#6b5442 iyr:2012 byr:1949 pid:131835020 eyr:2022

hgt:70cm iyr:2012
eyr:2037
hcl:64fd76
cid:175 pid:4880649770 ecl:grn byr:2029

iyr:2013 hcl:#7d3b0c eyr:2024 hgt:190cm pid:659772377 cid:226 ecl:oth byr:1958

ecl:lzr hgt:163cm pid:013605217
byr:2000
eyr:2020
hcl:z iyr:2024

cid:131 pid:896076106
hcl:#c0946f byr:1930
hgt:162cm eyr:2023 ecl:oth iyr:2017

byr:1935 iyr:2012
pid:942509879
ecl:amb
hgt:185cm cid:152 eyr:2024 hcl:#866857

ecl:#e490a3 hcl:4813a2 hgt:176cm pid:778369210 iyr:2020
eyr:2035 byr:2020

byr:2006 ecl:amb pid:148409219
hgt:189cm
eyr:2021 hcl:z iyr:2028

hgt:188in hcl:#9ed525
iyr:2018 ecl:grn eyr:2021
pid:065515632 byr:2012

cid:109 hgt:167cm
pid:545112664 ecl:grn hcl:#a62fea eyr:2026
iyr:2012
byr:1921

pid:174997024
iyr:2012
eyr:2030
ecl:grn
hgt:150cm
byr:1997
hcl:#866857

pid:451921339
hgt:181cm
hcl:#888785 iyr:2017 eyr:2026 byr:1936
ecl:hzl

hgt:187in
hcl:#866857 ecl:grn pid:623919686 eyr:2028 iyr:2011
byr:2016

byr:2001
ecl:gry eyr:2023 pid:324948416
hcl:ef16f8 cid:139 hgt:184in iyr:2026

byr:1954 hcl:#341e13 eyr:2023 pid:129321944 iyr:2012
hgt:183cm
ecl:amb

hgt:164cm pid:596870080
ecl:hzl eyr:2021 iyr:2017 hcl:#a97842
byr:1951

iyr:2013 byr:1944 hcl:#cfa07d
hgt:168cm cid:72 pid:160531632
ecl:grn

iyr:2012 pid:900043442 hcl:#ceb3a1 cid:124 byr:1941
ecl:blu hgt:156cm
eyr:2025

eyr:2021 hgt:61in iyr:2020 ecl:grn byr:1933

byr:1971 cid:175
eyr:2028 hcl:#efcc98 iyr:2013 hgt:170cm
pid:225213589

pid:147112660 hcl:#ceb3a1 eyr:2029 hgt:159cm ecl:grn iyr:2014
byr:1967

iyr:2015 pid:502975636 hgt:71in byr:1994
hcl:#18171d ecl:amb eyr:2029

byr:1948 hcl:#b6652a hgt:171in pid:181cm iyr:2019 ecl:grt cid:87

pid:859849571 ecl:amb hcl:#6b5442
hgt:193cm byr:1980
iyr:2017
eyr:2020

cid:125 pid:508147848
hcl:06ea75 iyr:1997 byr:2010 ecl:#c707f7 eyr:1970 hgt:161

eyr:2020 cid:326 byr:1989 ecl:gry hgt:160cm hcl:#cc080c pid:319135853 iyr:2010

ecl:utc
pid:531595917 hgt:180cm byr:1987
eyr:2024 hcl:#cfa07d iyr:2025

ecl:gry byr:2007
eyr:2028
iyr:2025
pid:6072964414 hgt:59cm hcl:#888785

pid:791025828 ecl:hzl hgt:178cm
iyr:2017
hcl:#733820
byr:1960 eyr:2021 cid:66

byr:1991 iyr:1934
cid:304 hgt:183cm ecl:grn
pid:408294229
eyr:2027 hcl:#623a2f

ecl:blu hgt:181cm eyr:2024 iyr:2010
pid:633234602 hcl:#2ce009
byr:1985

hcl:#c0946f hgt:192cm
iyr:2012 pid:120684397 ecl:grn eyr:2027
byr:1974

eyr:2026
pid:068304960 hgt:190cm byr:1925 iyr:2020 ecl:oth

hcl:#733820
hgt:168cm cid:307 iyr:2014 byr:1981 ecl:hzl pid:898831724 eyr:2026

hgt:73cm
eyr:2038
byr:1980 ecl:gry iyr:2027 pid:678846912 hcl:z

hgt:150cm cid:261 eyr:2021
hcl:z pid:159cm iyr:2014 ecl:hzl
byr:1955

pid:#172650 ecl:gry eyr:2040 hcl:z iyr:2013 hgt:169cm byr:2008 cid:290

iyr:2017 byr:1998
hcl:#ceb3a1 pid:274178898 eyr:2027 ecl:brn
hgt:183cm

eyr:2024 cid:183 ecl:grn
byr:1946
hgt:63in hcl:#6b5442 iyr:2017

hgt:97 byr:1990
iyr:2019
ecl:grn
pid:587580330
hcl:#341e13 eyr:2022

ecl:oth
pid:441517075 hcl:#c0946f iyr:2015 hgt:188cm eyr:2024 byr:1920

hgt:191in pid:185cm iyr:1993
hcl:93033d
eyr:2034 ecl:dne

pid:591478424 ecl:grn hcl:#888785
byr:1929 eyr:2023 hgt:173cm iyr:2017

iyr:1954
hgt:63cm
hcl:bdf2e0 ecl:amb pid:#912f46

byr:1956 iyr:2012 hgt:73in pid:986643426
ecl:blu
cid:235 eyr:2025

hcl:#cfa07d
cid:320 byr:1930
hgt:172cm
ecl:oth eyr:2024 iyr:2019

byr:1935 hgt:182cm pid:22794407 hcl:1b96fb eyr:1961 iyr:1941 ecl:#5e80cd cid:70

iyr:2020 eyr:2021
ecl:amb
hgt:59in pid:594829025 hcl:#93092e
byr:1976

hcl:#a97842 eyr:2030
byr:1937 iyr:2018 cid:295 ecl:oth
hgt:166cm pid:282634012

hgt:171cm hcl:#623a2f byr:1956
pid:068178613 cid:214
iyr:2012 eyr:2026 ecl:brn

byr:1921
hgt:161cm hcl:#888785
ecl:brn pid:010348794
eyr:2023 iyr:2011

hcl:#a97842 iyr:2010
byr:1955 eyr:2024
pid:473791166
ecl:brn
hgt:175cm

eyr:2028 ecl:grn pid:186196675 byr:1945 hgt:155cm cid:349
iyr:2011 hcl:#6b5442

hgt:161cm eyr:2030 cid:221
pid:994494879 hcl:#733820 iyr:2012 ecl:blu
byr:1957

eyr:1993 iyr:2022 hcl:z byr:2020 pid:013428192 hgt:62cm
ecl:dne

hgt:178cm eyr:2029 hcl:#733820 byr:1962 iyr:2017 ecl:blu pid:567713232

hcl:#fffffd
byr:1928 pid:390162554
eyr:2030 cid:79 hgt:150cm ecl:amb iyr:2019

eyr:2030 cid:320 hgt:171cm hcl:#888785 pid:540720799 ecl:amb iyr:2012 byr:1979

byr:1921
ecl:oth pid:204986110 eyr:2023 hgt:154cm iyr:2017 hcl:#341e13 cid:126

eyr:2020 cid:175 ecl:dne byr:1983 iyr:2016 hcl:#c0946f hgt:65cm

hgt:191cm
iyr:2010 cid:295 byr:1984 eyr:2025 hcl:#cfa07d pid:799775698
ecl:amb

iyr:2020 cid:278 hcl:#c0946f byr:1970 pid:773144393 eyr:2024 hgt:180cm

hgt:176cm
byr:1963
pid:252396293 iyr:2012 ecl:brn hcl:#ceb3a1
eyr:2030

pid:545130492
byr:2030 iyr:2020
hgt:190cm eyr:2034 ecl:blu hcl:#fffffd

hcl:#a97842 pid:032201787 hgt:190cm ecl:gry
eyr:2028 iyr:2012 byr:1994

hcl:#a97842 pid:064591809
ecl:hzl byr:1927 hgt:165cm
iyr:2011
eyr:2028

cid:77
byr:2005
hgt:125 iyr:1923 ecl:#605d73
eyr:2022 pid:90184674 hcl:z

cid:301 pid:106820988
iyr:2018
hcl:#cfa07d eyr:2029
byr:1993
hgt:193cm ecl:grn

hcl:#623a2f
cid:118
ecl:oth pid:75827285
hgt:189cm iyr:2010
eyr:2030 byr:1976

ecl:blu iyr:2023 eyr:1996
hgt:66cm cid:251 byr:1972 hcl:z
pid:557774244

byr:2002
hgt:169cm pid:629420566 eyr:2026 ecl:grn hcl:#341e13
cid:166 iyr:2019

iyr:2026 hcl:9b83a1 eyr:1979
ecl:dne hgt:111 pid:176cm

pid:#89718c byr:2026
hcl:2ca5c7 hgt:142 eyr:2040
ecl:lzr iyr:2029

ecl:grn
byr:2022 eyr:2020
pid:7024869 hgt:123 iyr:2019 hcl:z

hcl:#733820 hgt:155cm ecl:grn iyr:2020 byr:1955 eyr:2028
pid:217362007

hcl:#18171d ecl:gry
byr:1971 hgt:193cm
eyr:2020
pid:352009857 iyr:2013

byr:2018
hgt:175in ecl:xry iyr:2015
eyr:2036
cid:171 pid:6132398 hcl:#efcc98

pid:839955293
byr:1928 hcl:#fffffd ecl:hzl iyr:2011
hgt:162cm eyr:2023

hgt:175cm pid:482827478 eyr:2028
hcl:#6b5442 ecl:blu byr:1932 iyr:2010

iyr:2020 hcl:#866857
ecl:brn byr:1933 cid:269 pid:003931873 hgt:188cm
eyr:2022

byr:1981 hcl:#fffffd hgt:160cm cid:311 ecl:brn eyr:2025
pid:930857758 iyr:2014

hcl:#cfa07d hgt:73in
ecl:gry
pid:383281251
iyr:2013 byr:1934 eyr:2026

byr:1988 eyr:2026 pid:458002476
iyr:2017
hgt:175cm ecl:amb

eyr:1987
byr:2020 pid:299341304
hcl:#341e13 iyr:1935 cid:125
hgt:168cm
ecl:gry

iyr:2014 hcl:#b6652a pid:445799347
hgt:188cm byr:1960
eyr:2030 cid:290 ecl:amb

eyr:2023
hgt:75cm hcl:#733820 cid:195 byr:1933
ecl:amb pid:062770586 iyr:2019

hgt:168cm
eyr:2021
pid:725299968 ecl:grn byr:1932
iyr:2016 hcl:#888785

hgt:161cm hcl:#ceb3a1 byr:1962 eyr:2026 iyr:2013 ecl:amb pid:695426469 cid:227

ecl:dne hcl:#ceb3a1 iyr:2013 eyr:2022
pid:434786988 byr:1956 hgt:183cm

pid:697500517
byr:1968 hgt:169cm hcl:#fffffd ecl:grn cid:143
iyr:2010
eyr:2027

byr:2029 ecl:amb hgt:175in iyr:2015 hcl:#ceb3a1
pid:39839448
eyr:2021 cid:105

pid:0985607981 ecl:hzl iyr:2012
eyr:2021 byr:2024 hcl:5cad22
hgt:190cm

hcl:#b6652a hgt:178cm cid:222 byr:1992 ecl:grn
iyr:2011 pid:419544742

iyr:2019 byr:1960 ecl:hzl eyr:2021 hgt:184cm cid:66 hcl:#866857 pid:412920622

eyr:2025 hcl:#888785 iyr:2018 byr:1956 pid:698098389 ecl:grn hgt:173cm

ecl:blu byr:1935
pid:354892542 hgt:161cm
iyr:2018
eyr:2021 hcl:#b6652a

ecl:oth cid:287 iyr:2028 byr:1953 eyr:2027 hcl:#7d3b0c hgt:151cm
pid:211411839

iyr:2018 byr:1934 hcl:#a97842
pid:859748861
ecl:oth hgt:175cm eyr:2025

byr:1930 iyr:2018 eyr:2022
hgt:175cm
hcl:#292092
ecl:brn pid:987163365

hgt:167in hcl:#888785 eyr:2040 pid:4646402867 byr:2013 iyr:1941 ecl:#389aec

ecl:hzl hcl:#602927
hgt:168cm eyr:2026
cid:235 iyr:2016
byr:1942

iyr:1975 pid:11337832 ecl:#a25273 hgt:151 byr:2017

eyr:1979
hgt:71cm
byr:2003 hcl:7e7da7 pid:151cm ecl:#a8afb3 iyr:1937

eyr:2021 hgt:74in hcl:#cfa07d iyr:2014 byr:1932
pid:641867677 ecl:grn

ecl:gry hgt:185cm pid:556229206 iyr:2013
byr:1984
hcl:#fffffd eyr:2028

eyr:2020 byr:1989
ecl:grn pid:618876158 hcl:z
hgt:176cm iyr:2025

eyr:2025 byr:2001 hcl:#cdb7f9
pid:377402126 ecl:hzl hgt:184cm iyr:2019

byr:1939 hgt:180cm eyr:2029 ecl:oth hcl:#733820 iyr:2016
pid:733456875

pid:883743276
hcl:#7d3b0c eyr:2022 ecl:blu
byr:1928 hgt:150cm cid:150 iyr:2013

hgt:60cm ecl:#43f03d eyr:1994 byr:1975
iyr:1980 pid:169cm

hgt:104 byr:2029 eyr:2040 hcl:64a9b2
pid:83898860
iyr:1990
ecl:#938bbe

pid:284399238 ecl:gry hcl:#888785 iyr:2019 hgt:168cm byr:1944
eyr:2022

hcl:#733820 pid:486515752 ecl:grn hgt:188in byr:1941 iyr:2017 eyr:2005

iyr:2010
byr:1978 hgt:160cm eyr:2003
ecl:oth
hcl:#efcc98 pid:584668011

byr:1944 ecl:gry pid:962700562 iyr:2011 hcl:#866857 eyr:2022
hgt:191cm

hcl:z pid:758583213 iyr:1941 ecl:gry eyr:2007
hgt:67 byr:2022
cid:215

byr:1988
ecl:#ae2a9b hcl:#fe9d14 iyr:2012
pid:411550516 hgt:169cm eyr:2038

pid:400034647 byr:1927 hgt:165cm
iyr:2017 ecl:brn eyr:2024 cid:144 hcl:#341e13

hcl:#733820 hgt:153cm eyr:2027
byr:1935 pid:217121064 cid:120 iyr:2012 ecl:grn

hgt:168cm hcl:#866857 iyr:2012 pid:1527348755
byr:1946 eyr:2028 cid:184 ecl:amb

hcl:#a97842
byr:1967
hgt:152cm eyr:2030
ecl:blu
pid:929661915 iyr:2018

pid:671485026
hgt:188cm byr:1974 iyr:2015 ecl:grn cid:268 eyr:2021 hcl:#c0946f

pid:789877199 iyr:2011 cid:219 eyr:2029
ecl:oth byr:1991
hcl:#866857 hgt:154cm

cid:137 pid:059579902
eyr:2020 byr:1952
hcl:#18171d iyr:2020
hgt:172cm ecl:oth

pid:182cm iyr:1997 byr:2012
eyr:2034
hgt:161in ecl:#528abf hcl:b7d2fe

hgt:192cm ecl:oth iyr:2017 pid:264538307 byr:1994 cid:285
hcl:#18171d eyr:2030

hcl:#efcc98
pid:38036608
eyr:2010
iyr:2026
byr:2027
cid:239 ecl:zzz hgt:74

iyr:2012
eyr:2022 hgt:178cm
hcl:#888785
ecl:hzl
byr:1998 pid:000080585

pid:719620152 hcl:#b6652a cid:133
ecl:hzl
byr:1983 iyr:2012 hgt:175cm
eyr:2024

cid:155 eyr:1977 iyr:2019 ecl:#28de8b byr:1941 hcl:#602927 hgt:173cm pid:493773064

iyr:2010
pid:842124616 ecl:hzl eyr:2025 cid:146 hcl:#733820 hgt:166cm byr:1987

hcl:fd4dcf byr:2006 iyr:2011 pid:820797708 eyr:2020 hgt:189cm
ecl:gry

iyr:1971 pid:22107293 hcl:#5b3f01 cid:257
ecl:hzl
hgt:60cm eyr:2000 byr:1965

byr:1932 eyr:2028
hcl:#6b5442 ecl:amb pid:947149686
iyr:2015 hgt:187cm

hcl:#a97842
cid:260
hgt:167cm eyr:2027 byr:1973 ecl:oth pid:741678753 iyr:2016

pid:334234443 ecl:gry hcl:#18171d eyr:2020
iyr:2016 hgt:159cm byr:1926

hgt:118 eyr:1929 iyr:2013
pid:987139064
cid:196
hcl:#cfa07d ecl:#f72601 byr:1929

byr:1924
pid:623185744 iyr:2012 cid:341 hcl:#602927 hgt:192cm eyr:2022

iyr:2012 byr:1971 hgt:168cm cid:146 pid:673038025 hcl:#866857 eyr:2020 ecl:hzl

eyr:2023 iyr:2017
pid:205596613 cid:298 hcl:#341e13
hgt:169cm ecl:oth
byr:1996

ecl:blu pid:775831730
eyr:2029 iyr:1924 hgt:168cm hcl:z

byr:2023 hgt:181cm
pid:4365105095 iyr:2021
ecl:lzr eyr:2024 hcl:z

hgt:184cm byr:1987 pid:175cm ecl:#83a5fa eyr:2023

eyr:2021 pid:422371422 ecl:oth iyr:2015 hcl:#866857
byr:1963 hgt:174cm

pid:006970943
hcl:#2f22ef iyr:2020
ecl:gry
byr:1922
eyr:2024 hgt:163cm

cid:160 byr:2015
eyr:2038 hcl:z ecl:grt hgt:166 iyr:2026
pid:#14978f

hgt:178cm eyr:2021 iyr:2016 pid:471529794
hcl:#b6652a cid:192
ecl:grn byr:1970

iyr:2015 ecl:brn hcl:#602927 hgt:187cm
pid:729284172
eyr:2024 byr:1932

cid:153
ecl:dne eyr:2005
pid:178cm iyr:2028
byr:2029 hgt:160in hcl:482a92

byr:1995 iyr:2012 hcl:#866857 hgt:159cm
eyr:1950 ecl:gry pid:183cm

pid:875885919
hgt:159cm
iyr:2011
ecl:gry byr:1988 hcl:#341e13 eyr:2028

pid:2390267705 hcl:#7d3b0c byr:2009
eyr:2017 ecl:grn hgt:183cm iyr:2015

ecl:brn eyr:2029 hcl:#866857 iyr:2020 hgt:180cm byr:2001
pid:668021168

hcl:#c0946f
eyr:2024 ecl:amb pid:013487714 byr:1965 hgt:172cm cid:320 iyr:2020

eyr:2025 pid:115479767 hcl:#866857 ecl:oth
hgt:163cm iyr:2010 byr:1999

byr:1967 iyr:2011 cid:112 hcl:#733820
eyr:2040 ecl:grt
hgt:66 pid:804536366

hgt:163 pid:1764836278 eyr:2035
iyr:2021
hcl:z ecl:#f1bb27

hcl:#efcc98 hgt:176cm byr:1994 pid:590539278 ecl:grn iyr:2011 eyr:2021

iyr:2017 eyr:2024 hgt:167cm hcl:#b62e29 pid:495674801
byr:1970 ecl:brn

hgt:168cm pid:993244641
byr:1968
eyr:1926
hcl:#b6652a ecl:brn
iyr:2023

hgt:63in hcl:z pid:594070517
eyr:2021 ecl:oth
iyr:2017
byr:2000

eyr:2030 pid:272955042 cid:319 iyr:2011 ecl:amb byr:1999 hcl:#888785 hgt:158cm

eyr:2025
pid:814305816 byr:1945 ecl:brn hgt:162cm iyr:2018
hcl:#a97842
cid:229

byr:1996 eyr:2026 pid:582584802 hcl:#c0946f iyr:2020 ecl:grn
hgt:162cm

eyr:2027
hgt:155cm byr:1925
hcl:#888785
cid:182
iyr:2014 ecl:brn
pid:250884352

hgt:173cm cid:135
iyr:2017 pid:661330507 byr:1950 eyr:2020 ecl:gry hcl:#18171d

pid:208932950
eyr:2030 hgt:179cm
iyr:2013
ecl:oth
byr:1981
cid:58 hcl:#6b5442

hcl:#f183e7 iyr:2014
hgt:159cm pid:614579850 ecl:gry eyr:2029
cid:186 byr:1962

eyr:2027 hcl:#db3405 byr:1938 pid:194516631 cid:167 hgt:177cm ecl:oth

hgt:68in hcl:#733820 pid:228644594 eyr:2030 ecl:gry iyr:2010 cid:334 byr:1951

iyr:2017 hcl:#341e13
pid:#6a28c9 hgt:154cm ecl:gry
byr:1966 eyr:2023

pid:250155574 cid:84
hgt:157cm ecl:grn byr:1937 iyr:2017 eyr:2024 hcl:#b6652a

pid:831823039 eyr:2028 iyr:2015 ecl:gry
hgt:192cm cid:137 byr:1922
hcl:#6b5442

hgt:193cm byr:1941 eyr:2024 cid:56
hcl:#623a2f ecl:amb
pid:351293754 iyr:2016

byr:1947 iyr:2012 ecl:hzl hcl:#602927 eyr:2028 pid:252010138 hgt:152cm

hcl:#a97842 pid:801192586 ecl:hzl iyr:2018 hgt:193cm byr:1928 cid:323
eyr:2028

hgt:151cm
pid:756347561 ecl:hzl
eyr:2024 cid:161
iyr:2016 hcl:#623a2f
byr:2002

pid:648012871 iyr:2015 ecl:blu
eyr:2025 hcl:#623a2f byr:1973 hgt:177cm

byr:1999 hcl:#ceb3a1 cid:345 eyr:2025 ecl:#b29a96 pid:093304949
iyr:2017 hgt:93

hcl:#b6652a
iyr:2018 ecl:grn
byr:1951 pid:077278028 eyr:2024 hgt:62in

hgt:164cm pid:410770618 byr:1958
iyr:2019
eyr:2030
ecl:gry hcl:#fffffd cid:293

ecl:grt
eyr:2039
hcl:z pid:188cm byr:2022
iyr:2027 hgt:76cm

ecl:grn iyr:2012 hgt:150cm eyr:2024
byr:1926 pid:954310029 cid:64
hcl:#fffffd

ecl:oth eyr:2027 pid:091152959 hgt:180cm hcl:#ceb3a1 iyr:2015 cid:350
byr:1924

iyr:2017 hcl:#49a793 eyr:2021 cid:144 byr:1966
pid:717543257
hgt:161cm
ecl:hzl

eyr:2025 ecl:brn hgt:60in pid:391973520 byr:1928 cid:77
iyr:2012
hcl:#602927

iyr:2013 hgt:161cm pid:784483994 byr:1991
hcl:#cfa07d
eyr:2024 ecl:grn

ecl:hzl iyr:1967 byr:2009 cid:265 hgt:180in pid:168cm
eyr:1966

eyr:2024 iyr:2019 pid:534453983
byr:2028 ecl:oth hcl:#341e13 hgt:193cm

eyr:2029 iyr:2010 hcl:#623a2f ecl:gry hgt:152cm pid:572128647
byr:1996

iyr:2014 byr:1981 cid:176
ecl:grn hgt:183cm pid:974469723 eyr:2027

eyr:2029 pid:233353682 byr:1968
ecl:gry hgt:181cm iyr:2011
hcl:#efcc98

hgt:61 iyr:2005 cid:203 ecl:gmt pid:157cm hcl:z
byr:2013

iyr:2020
byr:1923 ecl:blu eyr:2026 pid:069770502 hgt:69cm
hcl:z

byr:1997 hgt:160cm
hcl:z iyr:2021 eyr:1920 pid:9374226872

ecl:hzl eyr:2024 pid:537492791 hgt:186cm byr:1952
hcl:#cfa07d
iyr:2020

hgt:73cm byr:1974
ecl:xry iyr:2016 cid:133
hcl:e741f5 pid:186cm

pid:161cm
byr:1950
eyr:2028 ecl:hzl hcl:#7d3b0c
iyr:2014 hgt:158cm

ecl:#2c491e
hcl:f8fe13 byr:2022
hgt:137 iyr:1948
eyr:2040 pid:#959a0f

byr:1923 hgt:70in
pid:904825661 hcl:#b6652a iyr:2010 eyr:2020
ecl:oth

iyr:2013
ecl:blu pid:858020233 byr:1950 hgt:61in

hcl:#18171d
iyr:2016
ecl:amb pid:613754206 byr:1975 hgt:164cm eyr:2025

byr:1938
iyr:2017 hcl:#623a2f cid:191 eyr:2027 hgt:174cm pid:287108745 ecl:amb

iyr:2025 hcl:#623a2f byr:2019 hgt:170cm
cid:233 pid:55323151 ecl:amb eyr:2037

ecl:amb
hgt:177cm hcl:#b6a3ce eyr:2025 byr:1967 pid:506927066
iyr:2018 cid:93

byr:1964 hgt:173cm eyr:2030 cid:106 pid:587635596 iyr:2012
hcl:#fb5993
ecl:hzl

ecl:lzr pid:190cm hcl:44746d eyr:1955 hgt:66cm iyr:1990 byr:2003

ecl:brn byr:1968 cid:216 hgt:181in hcl:#b6652a iyr:2016 eyr:2020 pid:0208311541

ecl:hzl hgt:181cm
eyr:1977 byr:2018 pid:527754216 hcl:#c0946f

ecl:grn hcl:#efcc98
byr:1935 eyr:2025 iyr:2018 hgt:65in pid:396444938 cid:293

hgt:64in ecl:oth
hcl:#18171d
pid:105602506 byr:1973
eyr:2022
iyr:2014

eyr:2039 hgt:64
ecl:#ab45a8 byr:2009
iyr:2025 pid:182cm hcl:d1614a cid:103";
        #endregion

        #region Day 5
        public static string Day5 = @"BFFFFFFRLL
FFBFBBBRLL
FBBFBFBRLR
BBFBBBBLLL
BBFBBBBLLR
BBBFBBBLLR
FFBBBFBRLR
BBFBFBFLRL
FBBFFBBLRL
BBBFBBFRRL
BBFBBFFRRL
BFBBFBFLRL
BBFFFBBRLL
FBFFFBBLLR
FBFBFBFLRR
FBBFBFFRLL
FFBBFFFRRR
BFBFBFBRLL
FBBFFBBRLL
BBBFFBBRLL
BFBFFFFLLR
FBBBBFBRRR
BBFBBFFLRR
BFFBFBFRRR
FBFBBBBRLR
BFBFBFFRLL
BFFFFFBRLL
FBBBFBFLLR
BFFBFBBRRR
BFFBBFBLLR
FBBBFFBLLL
BFFFBFBLRR
FFFFBFFLRR
BBFFFBFLLR
FBFBFFFLLR
FBFFFFFLLL
FFFFFFBRLL
BFFBFBBLRR
BFBFFBBLRR
BFFBFFBRLR
FFBFBBBLLR
FBFFFBFRRR
BFBFBFFRLR
BBBBFFBRRR
FBBFBBBLLL
FBBBBFBLRR
BFBBFFFRRL
FBBBBBBLRL
FBBFFFFLLL
FFBBBFFLLR
FBBBBBBRRR
BBBBFFBRLR
BBBFBFBLRL
FFBFFBFLLL
FBBBBBFLLL
FFBBBBBRLR
FBBFFFFLRL
BBFBBFBLLR
FFFBBFFRLR
FFFBBBFLLL
BFFFFFFLRL
BFFFBFFLRL
FBBFFFFRLR
FBFFFBBLLL
FFFFFBFRLR
BBBBFBFLLL
FBFBFFBLLR
FFBFBFBRLR
FBBBFBBLLR
FFBBBBFRLR
FBBBFBBRLL
FFFBFBFLLL
FFFFFBFRRL
FFFBFFBLRR
BFBFBBFRLL
BFBFFFBLLL
FFFBFBFRLL
FBFBBBFRLL
FFFBFBFLLR
BFBBFBFRRL
BBFFBFFRLR
FFFBBFFRLL
BBBBFFFLRL
BFFBFBBLLL
BBFFFFFLRL
BBFFBBBRLL
FFBFFFFLRR
FFBBFFFRLR
BBFFBBBRLR
FBFBFFBRRL
FFFFBBBLRL
BBFBFFFRRL
BFBBBFBRRL
FFBFBFBLRR
BBBFBFFRLR
BFBBBBBRRL
FBFFFFFLRR
BFFFFFBLRR
FFBBBFBLLL
FBBBFFFLRL
BBFFBBFRRL
FBFFFFBRRR
BFBFBBBRRL
BBFFBBBLRL
FBBFBFBRLL
FBFBBBFLLL
FFFFBBBLRR
FBBFBBFLLR
FBFFBBFLRL
FFBBFBFRRR
BFFBBBFLRL
FFFBBBFRRL
FFBBBFFLRL
BFBBFFBRLL
BFFBFFFLLL
FBFFFFBLRR
BFBFFBFLRL
FBBFFBFLLR
FBBFBBBRRL
BBFBFFFLLL
FBBBBFFLRR
FBBBFBBRRR
FFFBFFFLLR
BBFFBBFLRL
FFFFBFFRLL
BFBBBFFRRR
BFFFFBFRRL
BBFFBBBLLR
BFBBBBFRRR
BFBBBFFLLL
BBFBFFFLRL
BBBFFFBRRL
BBBBFFBRLL
FBFFFFFLRL
BFFFBBFLLR
FFFFBBFRRL
BBFFBFBLLR
BFFBBFFRRL
BBBFFBBLLL
BFFBFBFLLL
BFBFBFBRRL
BBFFFFBRRL
FBBBFBFRRR
FFBFBBFRLR
FFFBBBBLRR
FFBBFBBRRL
BBFBBFBRLL
FFBBBBBRLL
BFBBBBFLRR
FBFBBFBLLL
FBBFFFFRLL
BFFBFFFLRL
BBBFBBFLLL
BFBFBBBRRR
FBFBBFFLRL
FBBBFFFLLR
FBBBFFBLLR
BBFFBBFRLL
FBBFFBFLRL
BFFBBBFRRR
BFBBBBBRLL
FFBBBBFRLL
BBBFFFFLLL
BBFBFFBRRR
FBBBBFFLLL
BBBFFFFRRR
FBBFBFBLLL
BBBFBBFLRR
FFBFFBBRLL
FFBBFBFLLL
FBFFFFFRLL
FBFBFFBLRR
BBFFBBBRRL
BFFBBBFRRL
FFBBBFBRLL
FBBBBFBRRL
BBFBBBBRLR
BFFBFFBRRL
BBFBFBBRRL
FFFBFBFRLR
FFBBFBFRLR
FFFBBFBRLL
BFFFFBBRLL
FBFBBFBLLR
FBFFBBFLLL
BBFBFBBRLR
BBFBFBBLLL
FBBBFBBRRL
BFFFBBBRLR
BBFFBBBRRR
BFFBFFBLRL
BBFBFFBLLR
FFFFFFBRRL
BFBFBFFLLR
BBBBFFBLLL
FBBBFBFRRL
FBBFFBFLLL
FFBFBFFRLL
BFBBFFFLLL
FBFFFBBLRL
FFFBFFBLRL
BFFFBBBLRR
BBBFFBFRRL
FBBBFFFRLR
FFBBFFBRRR
FBBBBBBLLL
BFFBBFFLLR
FFFFFBBLLR
FBBFBFBRRR
BBBFBFFLLL
FFFFBBFRRR
FBFFFFBRLL
FFBBFFFLRR
FFFFFBBLRL
BBBBFFFLLL
FFFFBFBLLR
BBBBFFFRRR
FBBFBBFRRR
BFFBFFFRLR
FBFFBBFLRR
BFBBFBBRLR
BFFBBBFLLL
BFBFBBBLLL
BBFBBBFRRL
FBBFBBFRRL
FFFBFFFLRL
FBFFBFBLRR
FFBFFFFRLL
FFFFBBFLRR
BFBFFBFLRR
FBBFBBBRLR
BFBBBFFLRL
BBFBFBFRLR
FFFFFBBRRL
BBFFBFFLRR
FBBBFFBRLR
BBBFFBBRRL
FFFFFBBRLR
FFFBBFFRRL
FFBFFFFRLR
BFFBFBBLLR
BFBFBFFLLL
FFBFBBBLRL
BBFBBBBLRR
BFFFFFBRLR
BBFBFBBLLR
FBBBBFBRLL
BBFFBFBLRR
BBFBFBBRRR
BBBFBBBLLL
FBFBBBBRLL
BFFBFBBRLL
BFBBBBBRLR
BBFFFFBRLL
FBFBBBBRRR
FBBFFFBRRL
BFFBBFFLLL
BBFBBFBLRL
FBBFBBBLRR
FBBFBBFLLL
FFFFBBFLRL
BFFFBBBRLL
FFFBBBFLRL
BFFFBBFRLR
FFFFBBBLLR
FFFFBFFRLR
FFFFBBBRRR
BFBFBFBRRR
BBFFBBFRRR
FBBFBFFLLR
BFBBBFBRLL
BFBFFFFRLR
BFBFFFBLLR
BFBBBFBRRR
BFFBBBFRLL
FBFFBBFRLR
FBFBBFFLLL
FBFBFFFRLR
FFFBFBBLRR
FFBBBFFRRL
BBFBFFBLRL
FBBBBBFRLR
BBBFBFBLLR
FFFBBFFLRL
BFFBBFFLRR
BBFBFFFLRR
FFBFFFBRRR
BFFBBFBLRL
FFBBFFBRLL
BFBFFBFRLL
FFBFBFFRLR
FBFFFBFRLR
BFBBFBFRLL
BFFBFBFRRL
FFBBFFBRRL
FBFFFFBLLL
BFFFBBBLLL
FBBFBFFRRL
BFBFBBBRLL
FFFBFFFLLL
BFFFBFFRRL
FBBBBBFRLL
FFBFBBFRRR
FBFBFFFLLL
FFFBFBBRRR
BFBBFFFLRR
FFFFFBFLRR
FBFBFFBRLR
FBFFBBFLLR
BBBFFFBLLR
FBFFFBBRLL
BFFBFBBLRL
FFFFFFBRRR
FFBFBFBRLL
BBFBBFFLLR
BBBFFFFRLR
BFBFBFBLRL
FBFFBFBRLR
FBBFFFBRRR
BBFFFFFLRR
BFBFBFBLRR
BFBBBBBLLR
FFBFFBFRRR
FFFFBBBRLL
FBBBBBFLRR
FFBBFBFRLL
FBFBBFFLRR
FFBFBBBLLL
FBFFBFFRRL
BBBFFBBLRR
BFFFFFFRRL
FBFFBFFRLL
FBBBBFBRLR
FBBBFBFLRL
FBBBFFBLRL
FFBBFBBLLR
BBFFFBBRLR
BBBBFFBLLR
BFFFFBFLRR
BBFFBFFRLL
FFBFBFFRRR
BFFFFFFRLR
FFFFBFFLRL
BFBBFFFRLR
BBFFBFFLLL
FFFFFBFRRR
FFFBFBBLRL
BFFBBBBRRL
FBFBBFFLLR
FFFBBFFLLL
FFFFBFBLRL
BBFBFBBLRR
BBFFBFFLRL
BBBFFFBLRL
BFFBFBFRLL
FFFBFFFLRR
FBFFFBBRLR
BFFFBBFRRR
FBBBBBFLRL
BFFFBBBLLR
BFFFBBFLRL
BFFFFBFLLR
FBFFFBBLRR
FBBFFFBRLR
BBFFBFBLRL
BBFBFBFLLR
FBBBFBFRLR
FBFFFBFRRL
BFFFBFFLRR
FFFFBFBLRR
FBFFBFFLLL
FFBFFBBRLR
BBFBBBFLLL
FBFFBFBRRL
BFFBFFBRLL
BFBFFFFLRR
FFBFBBBRLR
BFFBFBBRRL
FBFBBBBLRR
BFFFFFBLLL
FBFFBFBRRR
FBFBBFFRLR
BFBFBBFLLL
BFBBBFFRRL
FFBBBFBLLR
FBBFFFBLRL
BFFFFFFLLR
FBFFFFBLRL
FFBBFFFLRL
FFBBBFBLRL
BFFFBFBLLR
BFFBBBFLRR
FBFFFBBRRR
FFBFFFBRLL
FBFBFBBLLR
BFBBBBFRLL
FBBBBFFRLL
BFBFFBFRRL
FBBBBFFRLR
FFBFFFFRRR
BFFFBBFLRR
BBFBFBFRLL
BFFBFFBRRR
FFFFFBFLLL
BFBFFFBRRL
BFFBBFBRRL
BFBBFFBRRL
FFFBBBBRLR
FFFBBBBLRL
BFBBBFFLLR
BFBFFFFRRR
BBBFBFFRRR
FFFBFFFRRR
FFFBFBBLLR
BBBFBFFRRL
BBFFBFFLLR
FFFBBFFLLR
FFFBFBBLLL
FBFBFBFLRL
FFFFFBBLLL
FBBBBBBRLR
FFBBFBFLRL
BFFFFBBRLR
BFFFBFBLLL
BBFBBBFRRR
FBBBFFBRRL
FFBFFBFLRL
FFBFFFFLRL
BBFFFFBLLL
BFBBFBBRLL
FFFFBFFRRR
BFFBFBBRLR
BFFBFBFLRR
BFFBBFBRLL
FBBBFBBLRR
FBFBFFFRLL
BFBFFFBRLL
FBBBBBFRRL
FBFBFBBRLL
FBBFFBBRRR
BFFFBBFRRL
BBFFFFFRRR
BFFBFBFLRL
BBBFFBBLRL
FFBFBBFLRR
BBFFFBFRLR
FBFBBBFRRR
BBFBBBBRLL
FBFFBFFRLR
BFBBBFFRLR
FBFFBBBLRR
BFBFFBFRRR
FFBFFBFRRL
FFBBBBBLLL
BBFBBBFRLL
BFBBFBBLLL
BFBBBFBLRL
FBBBFBFLLL
FBBFBFFLRL
FBFBFBFLLL
BBFFBBFRLR
FBFBBBFLRL
BBFBBFBLRR
FFFFBFFLLL
FFFBBBBRRL
BBBFBBBRLL
BBFFFFFLLR
FBBFFBBRLR
FBBFBBFRLR
BFFFBFBLRL
BFFFFBBRRR
FBFFFFFLLR
FBBFBBFLRL
BBFFFBFRRR
BBBFFBFLLR
BFFBBFBRLR
BBFBFFFRLL
FFFBBFBLRR
BBBFBFFLLR
FBFFBFBLRL
FBBFBFFLRR
FBBBFFFLRR
FBBFBBBRRR
BBFFFBFRLL
FFBFBBFLRL
FBBFFFBLLR
BFFFFBFRRR
BBFBBFFRLL
FFBFBBBLRR
BFBFFFBLRL
FBBFFBFRRL
FFFFBBBRRL
FFFBBFBRRR
FFFBBBFRLL
BBFFBBFLLR
FFBFBFFRRL
BFBFFFBRLR
FFBBBFFRLL
FFFFBBFLLL
BFFFFFBLLR
FBBFFFFRRR
FFFFFFBLRL
BBFBFFFLLR
BBBFBFFLRL
BBBFFFFRLL
BBFFFFBRLR
FBFBBFFRRL
FFFFFBFLLR
BFFFBFBRLR
BFFFFFFRRR
FBFBFBBRLR
BBBBFFFRLR
FBBFBFBLRL
BBFFBFFRRR
BFFBBBBRLR
BBBBFFBLRR
BFBBFBBRRL
BFFFFBBLLL
BFFFBFBRLL
FBBFFFFLLR
BFBBFFFRLL
BBFBBBFRLR
FBFBFBFRRR
BFBFBFFLRR
BBFBBBBRRL
BFFFBFFRLR
BBBFBFBLLL
FBFBFFFRRL
FBBBFFFRLL
FBFBBFBRRR
FBFFFFFRRR
FBBBBBBRRL
BBBBFFFLLR
BFFFBFFLLR
BFBBFBBLRL
BBBFFFBRLR
FFFBBFBLRL
BFBFBFFRRR
BBFFFBBLLR
FBFBBFFRLL
FFFBBBFRLR
BFFFFBFRLL
FFBBFBBRLL
FFFBFFBRRL
BBBBFFFRLL
FBFFBBBRLL
FFBFBFFLLL
BBBFBBFLRL
FBFBBFBLRL
FFBFBFFLRR
FFFBFFBLLR
FBBFBFFRRR
FFBFBFBLLR
FFFFFFBLLR
BBBFFFFLRR
BBFBBFFRLR
FFFFBBBLLL
FBBFFFBLRR
BFFFFBFLLL
BFBBBFBLRR
FBFFFBFLLR
FBFBBFBRRL
BFBFBBFRRR
FBFBFFFLRR
FFBFFBFLLR
FBFBFBFRRL
FFFBBBFLRR
FFFBBBBLLR
BBBFBBFRRR
BFFBBBBLRR
BBBFFBFLRR
FFBBBFFRRR
FBFFBFFRRR
BFFFFBBLRR
BFFBFFFRLL
BFFBFFBLRR
FBBBFBBRLR
BFBFFBBRLR
FBFFBBFRRR
FFBBBFFRLR
BBFFFFFRRL
BFFBBBBLRL
FBBFFFFLRR
FFBFBBFLLL
BFFFBBBRRR
BBFFFFBLRR
FFFFBFBLLL
FBFFBFFLRR
BFBFBFFLRL
BBFFFFFRLR
BBBFFBBRLR
BBFFBFBRRR
FBFFBBBLLR
BBFBFBFRRR
BFBBFBFRRR
BFFFBBBLRL
BFBBFBBLRR
BBBFBBBRRR
FFFBBFBRRL
FFBFFFBRRL
FFBFBFBLRL
BFFFFFBRRR
BFBFBBBLLR
BFFFBFFRRR
FBFBFFFLRL
BFBBBBBRRR
FFFBFBFLRL
FFFFBFFRRL
FBFBFBBLLL
FFFFBBBRLR
FBBFBBBLRL
BFBBFBBRRR
FFFBFBFLRR
BBBBFFBRRL
FFBBFBFLLR
FFFBFBBRLR
BBFFFBFLRL
FFBBBFBRRL
FFBFBBBRRR
BFBBBBFLLR
BBFBBBFLRL
BFBBFBBLLR
BBFFFBBLRR
FFBBFFBLRL
BBBFFFFRRL
BFFFBFFRLL
BBBBFFFLRR
FFBBBBFLRR
FBBFFBFLRR
BBBFFFFLLR
BFFBFFFRRL
FBFBFBBLRL
BFBFFBFLLR
BFFBFBFLLR
BFBFFFBRRR
FFBBFFFRRL
BBFBBFBLLL
FBBBFBFRLL
BBFBBBFLRR
BBBFFFBLRR
BBFFFFBLLR
FBBFBFFRLR
FBFBFFBLLL
BFFBBBFRLR
BFFFFBBRRL
BBBFFBFLLL
FFFBFBFRRR
FBFFBFFLRL
FFBBBBBLLR
BFFBBFBRRR
FBBBBFBLRL
BFBFBBBRLR
BBFBBFFLLL
BFFFFFBLRL
FBBFBFBRRL
FBBBFFFLLL
BFFFBFBRRR
FFBFBFFLRL
FFBFBFBRRL
FFBBBBBRRR
FFFFBFBRLR
BFBBFFBLLL
BFFBBBFLLR
FBBBFFBRLL
FBFFBBBLRL
BFFBBBBRRR
FFBFFBBRRL
BFBFFBBRLL
FFFFFBFLRL
FFBBBBFRRR
FFBFFFFLLR
BBBFBBBRRL
FFFFFBBRLL
FBFBBBBRRL
FBBBFBFLRR
FFFBBFFLRR
FBFFFBFLRL
FFBFFBFRLL
FFFBFFFRRL
FFFFBFBRLL
BBFBFBBRLL
FFBBBBFLLR
FBFFFBBRRL
BFBBBBBLLL
BFBFFBBLLL
FFFFBFBRRL
FFBBFFBLRR
BFBBFFFRRR
FFBFFBFRLR
BBFBBBBLRL
FBBFBBBLLR
BFFFBBFLLL
FFFFBFFLLR
BFFFFBFRLR
FBBFBFBLRR
BBBFFFFLRL
BBFFFBBLRL
BBFBFFBRLL
FBBBBFBLLR
FBFFBBFRLL
BBFFBFBLLL
FBBFFFBRLL
BBBFBFBRRL
FBFBBFBRLR
BFBBBBFLLL
FBFBFBFRLL
FBFBBBFLLR
BBFBFBFLLL
BBFFFBFLLL
BBBFFFBLLL
BFBBBFBLLL
FFBBBFFLLL
FFBFFFFRRL
FBBBBBFLLR
FBBFFBBLRR
BBFFFFFRLL
FFFFFBBLRR
BFBBFFBLRL
BFFFFFBRRL
BFBBFFFLLR
FFFBFBBRRL
FFBFFBBLRL
BFFFFBBLRL
FBFBBBBLRL
FBBBFFBRRR
FFBBFBBRLR
BFBBFFBLRR
FBBBFFBLRR
FBFBFFBRLL
FBFBFBFLLR
BBFFFBBRRL
BBBFBFBRLL
BBFBBFFRRR
BFBBFFFLRL
BFBFBFFRRL
BBFBBFFLRL
BBFBFBFLRR
BBFBFFFRRR
BBBFFBFRLR
BFFBBFFLRL
FBBBBBBRLL
BBFFFBBLLL
FFFBBBBLLL
BBFFFFBLRL
FFBBFFBRLR
BFBBBBBLRR
BBFBBFBRRR
FFFFFFBRLR
FFFFBBFRLL
FFFFBBFRLR
FBFBBFFRRR
FFFFBFBRRR
FBFBFBBRRR
FFFFFBBRRR
BFBBBBBLRL
BBBFBBFLLR
BFBFFBBRRL
BBBFFFBRLL
BBFBBBBRRR
FBFBBBFRRL
BFBBFBFLLL
FFFBBFBLLL
BBFFBFBRRL
FFBFFBBLLR
FFBFBBFLLR
BBBFBBFRLL
FBBFFFFRRL
FBBFFBFRRR
BFFBFFBLLL
FBFBFFBLRL
BBBFBBFRLR
FFBFBBBRRL
FFBBBBFLLL
FBFBBBFLRR
FFBBFBFLRR
FBFFBBFRRL
FFBBFFFLLR
FBFBFBBLRR
BBBFBBBRLR
BFFBFFFLLR
BFFFBFBRRL
FFBBBFBRRR
BFFBFFFRRR
BBBFBFFLRR
FFFFFFBLRR
BFBFBBFRRL
FBFBBFBRLL
FBBBBFFLLR
BFBBBBFLRL
FBBBBFBLLL
FFBBFFBLLR
BBBFFBBRRR
BFBFFFFRRL
FFBFFBBRRR
BFFBBFFRRR
FBFFBFFLLR
BBFFFFFLLL
BBBBFFBLRL
BFBBBBFRRL
FBBFBFFLLL
FFFFFBFRLL
FBBBBBFRRR
BBBFBFFRLL
BFBBBFFRLL
FFBBBFFLRR
BBFFFBFLRR
BBFFFBFRRL
BBFBFFBRLR
FFBFFFBLRR
FBFBFBBRRL
FFFBBFFRRR
FBBBBBBLLR
FBBFFBBLLL
FBBBBFFRRL
BFBFFFFLRL
BFBBBFBLLR
FFFBFFBRLL
FBFBBFBLRR
FFBBFBBLRR
BBBFBBBLRR
BBFBBBFLLR
BFBFBBBLRR
BFBFBBFLRL
FFFBBBBRRR
BFBFFBBRRR
FBBBBFFRRR
FFFBBFBRLR
FBFFFBFRLL
BFBFFBBLRL
FFBFBBFRRL
BFFFFFFLLL
BFBFFFFLLL
BBFFBFFRRL
BFFBBFFRLR
FFFBFFBLLL
BBFBFFBLLL
FBFFBBBRLR
FBBBBFFLRL
FBFFBBBRRR
BFFBBFBLLL
BFBFBFBLLR
FBFFFFBRLR
BBFFFBBRRR
BFBBFFBLLR
FFFBFBBRLL
BBFFFFBRRR
BFFBBFBLRR
FBFBFBFRLR
FBFFFBFLRR
FFBBBBBRRL
FBFFBBBRRL
BBBFFFBRRR
FBFFFFBLLR
BBFBFBFRRL
BBFBFFFRLR
BFFFFBFLRL
BBBBFFFRRL
FBBFBBFRLL
BFBFBFBLLL
FBBBBBBLRR
FFBBBBBLRR
BFFBBFFRLL
FFFFBBFLLR
FFBBFFBLLL
FFBFBBFRLL
BBBFBFBRRR
BFBBFBFRLR
FFBBFFFRLL
FBFFFBFLLL
FFBFFBBLRR
BFBFBBFLRR
FBBFFBFRLR
BBBFBFBLRR
BBFFBBBLLL
FFFBBBFRRR
BFFBBBBLLL
FFFBBFBLLR
BFBBBBFRLR
FBFFBFBRLL
FFBFFBFLRR
FFFBFFBRLR
BFBFFBFRLR
BFBBBFFLRR
BFBBFBFLLR
BBFBBFBRRL
FBFBFFBRRR
BFBBFFBRRR
BBFBFFBLRR
FBFFFFBRRL
FFFBBBBRLL
FFBBFFFLLL
BFBFBBFLLR
FBBBFBBLLL
BBFFBBFLRR
FBFFFFFRRL
BFBFFBBLLR
FBBFFBBLLR
BBBFBFBRLR
BFFFFFFLRR
BBFFBFBRLR
FBBFFBBRRL
BFFFFBBLLR
BBFBBFBRLR
FFBBBBFRRL
BFFBBBBLLR
FFBBFBBLRL
BFBFFFFRLL
BFFFBBFRLL
BFBFBBFRLR
BBFFBBFLLL
FFBBBFBLRR
FBFFBBBLLL
FFBFFFBLLL
FBBFFFBLLL
FFBBBBBLRL
FBBFFBFRLL
FFFBBBFLLR
BBBFBBBLRL
BFFBFFBLLR
BFBFBBBLRL
FFBFFFBRLR
BBBFFBFRRR
BFFBFBFRLR
FFBBBBFLRL
FBBFBBFLRR
FBBBFFFRRR
FBFBFFFRRR
FFFBFFFRLR
BFBBFBFLRR
FFBFFBBLLL
FFBFFFFLLL
FBFFBFBLLL
BFBFFFBLRR
FBFFFFFRLR
BBFBFBBLRL
FFBFBFBRRR
FFBFFFBLRL
FFBBFBBRRR
BBFFBFBRLL
FFFBFBFRRL
FFBFFFBLLR
FFFBFFFRLL
FFBFBFBLLL
FFFBFFBRRR
BBBFFBFRLL
FFBFBFFLLR
BBBFFBFLRL
FBFBBBBLLR
FBBFBFBLLR
FFBBFBBLLL
BFFBFFFLRR
BBFFBBBLRR
FBBFBBBRLL
FBFBBBFRLR
BFFFBBBRRL
BFBBFFBRLR
FBBBFFFRRL
BFBFFBFLLL
FBBBFBBLRL
FBFBBBBLLL
FBFFBFBLLR
FFBBFBFRRL
BFFBBBBRLL
BBBFFBBLLR
BBFBFFBRRL
FFFFFFBLLL
BFBBBFBRLR
BFFFBFFLLL";
        #endregion

        #region Day 6
        public static string Day6 = @"mz
mz
mzch

fzmwqgcjylr
goqewcrzfjm
caqgpmrwz

hupvfabkzntecq
diuvsxqtpbfjckmz

ovc
jcuvo
ocv

zpikjxaynebqltfg
xgcqajrb
xbhqujgamv
gbcrqdhjawx
qagmovxjb

vfxuaj
ryhsdotgzaekp

sva
lv
p
lv
ekuy

xyhnip
kxiyp
ixyp
txbisypfzr

xklhemvrcaudqs
ztbj
wobj
ngi

rldtoyewcbk
cjlrfbikmyw

rkvfule
asgwbtiypcx
f
mloj

z
v
z
z
z

crqksbxng
qsbcxzgkn

axefrumdwtzsvqbnjpih
svnphxqdzucrmfibtwe

tvpcsrgbfu
ptvbscufg
bfgpctsvu
jsfugcpvtbyeq

cmdkrgezqbup
kqumercgzd
qtzgkjcrmued

gf
f

pizyco
zuvdxqcornepw
zoyplc
oczyp
jtzcoiyp

lq
lq
lq
oql

bksxqpdnvoctywjmrlezif
nzobdvywjcmrsiqtlpkfex
ezfmkwlgnvqjsotibcxrpyd

zwyvtxondeblf
mxqjuolebcgifhwz
bstorxweyfldzv

naurgqhwsjie
hgeaqjrwnt
agrnqhwlej
eabqhgjrnw
qrjamegwhnp

p
ep
p

phrvq
prhlqv
pqrvh
vpqgrh

xnqofa
xnoafq
nafoxq
xnqofaj

vcuibfm
icdfuvm
ivmudcf

xgfcdyqhmzetbvp
qbwfmhgatzxysvncpldejr
vcythpqbeumfdgxzi

gkeanqohi
ofagqiekhn
neioghqak
vgeoqinakh

ehvjdabzoluniywmprqskfxt
biwvhzgaftysnrjqpkolduxe

mwdet
e
eg
nez
evjp

cnlyzw
qcfklwnm
gjunvbxeacohr
nictp
fcns

xjdkyvsg
djkpxuygs

zgyovlcmqkiejwbn
wecmknzvbglyji

jrfcu
mcnlifrwy
ufrxcq
uftcrja

ndryz
dnbyzpr
yzrnd
znryd

boigahmft
iubmwtvqx

boidcxahqvwzsuytnfmgrejlkp
nuzrsagxtwpjqdcyfekmoilhvb
henvortgaqpufsikxyjcwlbmdz
dyerfaojtnguxzbhvlsikwmcqp
cmtlqghxodpbyafrijenvskwzu

td
xitod
mtyd
dtq
dtmnq

rdvebxmjtgl
dejrbglmit

gqrnekjhlat
pxosynvzumbcdf

dxw
woqu
w
w

ucydqmngvbipoxskzwrjelta
ovanpticdzjrbgufmsweq

xpksqf
qwxsk
kwqsx
qstkxz
xkaqes

jhge
he

qkzibmslehp
gjxcyvnf
gudcrt

stnqlzvxujgmci
jizgxulnmvqecst
qtlzmnsgixvcuj
juzctmsgvixlnq
uixqstmzjvolncg

rcuytdziwp
aydzuitrcw
cwdtyrui
bikwfztrudcy
clusdiwrtmyj

vinw
icrfwq
ibw
wzib

yca
aoyc
zcy

lrtchbe
ikreqgyvbh
rdfxmzhsnbeo
erghpikbw

jbfzo
tnpsxoiu
lebdwco
lqeamjrow

bcrd
lkqofmujitzxn
gsea
c
gvp

evijgtpazbrosdyfu
rotbdpijsfzyugahev
yjsftipavduzbgero
ifpgtzduoyabqvrjes
pyovcbdrtmjugfsezai

xu
ux
ux
uxa
xu

whnumzsbdjxvfklgp
mkezxfdwolsvqtgjun
lfyngxsumvkdjzw
nvxwjszgdlkfmub
skmfngvwplxjduz

njrxskbodqmyleu
ynxutlswbekzjorcqdm
koemuynlsdbrqijx

rsmtfkwp
nayjbukgcdoxzp
vkqhprewi

divsuxwnjhayflozcrkgembq
kjgorbnwityelauxvqmzhcpdfs

hrwsbtejpczayulgoqfm
ugqtoswycrzhafpbmlje
ryusqmwlgcajethpzfbo

s
pihnt

iyfvsbhqdltxg
ifwecadbghoyztnj

bviwpexnymh
lfdzjurgas

neacpxhlgiwvrytof
fpoxarvklecitngy
ycprnualtxekgiof
flceyxozrptiagnd

vqthogbnkfapjymsxudz
sotbeqjdfhzvxuknp
qudxgptzbhoyfnvks
vkfsrtxupnwqizbhld

y
y
y
e
y

vot
vw
skdv
vld

wxfkqj
fjkxpwq
skwehfqjmx
fakpxqjw

krmyuhfbqjicdo
yrecdasbkqlpfuihtm

kmwjvoz
kzj
jkz

hu
hu
uh
uh

xdotkgseayqcjv
atgjiqceozdsknxy
rxyghacbqomjdts

lhijk
ilhkzj
jaulcpkdhrvoe
wlhfyjsk
hkjnl

hzwipsumcygkand
kaungswdyzcpmhi
wyhiaupdgkmsncz
gapiysmchnkzduw
nyhwdkcmpsigzua

m
mwu
m
m

xymekvhjfoqanwu
jyefxonhwumkavq
ekamynojhqwxfvu
fvdwhumjexqnoayk

uso
ous
uos
ouhsn

ba
ba
ab

z
z
z
z
zm

x
ex
xs
hxil
sx

l
l
lt
l
l

wgkiablvjhos
hoajlgwksivb
kjblqvoawhsgi
giavbolhskjw

gramcuzevh
ejsmobhzy
zemih

w
h

wl
ycl
lcy

jxtuecnwavyg
utqygrejwnlxc

bydxajenfpwtluchg
ljtaybdhfnewxuci
auyidcnktljhfwzebmx
hatuwfjxbylednc

abdws
pxwsdb
ybrhwotsd
dcxgsbw

mcnujadbiz
jdznmucaib
zncijdmau
aiundczsmj

aofecx
xcaqfo
ojcafr

edifkl
ehgjwkli
ksepqlm
wbkle

cq
uj
k
e

aiwcx
agcixw

axykbruimzfwsenhcglpjdqo
qflhrpmownujiagxebzcdky

meyxdtjrwusilkb
iotakdjqrmewxysu
rjtdsykiwexhum
eyujdmotaxswkri

girbwahyvqno
qgkovyinraxbhw
qyvraobihwng

pmxefc
fme
tfrwmeha

fz
zfw
zf
lzd

d
d
d
d

uehgdsrj
jbzergthdsq
lrxdghjem
aqetghdrj

mhpke
pkaem

ijdspfvu
mowdfsvuit
skeuxyivdfn
odvjgtsufi
icvsdbufg

qtpbs
cyrag
xm

gptbaxizvchwjnkmufr
uzvxrbpgcajkwtfim
fajzbvrkpxtmuwgci
mzxqbvuwjtrpgakcif
bjcgixftvakzrupwmy

xdbinla
iyb
mbyi
esubyi
zrib

bpindyumqzg
obkaswqyvnexiltdg

vclqgxkaowurtyhip
usbfdyomh
senjyhuo

jghuexsqtzyvilk
qephbvglkyisuzjtx
yliupkqeghztvsjx
hjgyxkletizqusv
rzvyhetxqkjmiuosgl

pwr
rw

mxdbjlt
xrwjp
cqjxg

xzejwkg
pwgcxke

n
n
n
n
n

mohyfds
hirldzty
ydgrhz

zjnwrmqvf
wotaimsy

fjb
fib

k
k
mj

gmqpucbtzheayxjnisvwlf
auwfxmsjvephlzqygnbcti
ilzbwaxfsqunjmehpycgtv
bjqsngepuaxzlwfmctyvhi

kjtrspzvdaxq
vmaxdzpjqrktfs
zptrsvakdhqjx
vsctahyrqkijxzpd
dvzsjtkapxrq

pgdrsqj
mdcqkgrspj
depsgajqr

enaub
bnuae
eunkba
qbuean

aolmtpfdrujqhbngxeyvzkswic
ikybegrjmvzxplqcdhutafswo
hxbcizkajomlsdwpfteyqgvru
jqzcuohfawlvriyemkgxsdptb

jzhoyflbvwgax
zkawjhuloyxvgb
ozexgablyjcwhv

tzqdyshga
dgmhzjq

kshuoxnlvecdjfqt
hksdeyncviuqlmxbtoj
pnrvdskltjuwexhq

gdfcriwh
cdigfrohw
cahzfiwxrdg
chgrfiwda

ebtjygzhoivq
yqvhtbiegoz
gyqahibovwtze
oyigebqhvtz

t
t
t
r

vgrhsw
hnvsxg

hwlnmuyeckqi
pxajovtbgz

pmiweusdaxtkv
ikaxyvwmseudt
dtiauvxemwks
kiwdevxsmuat

sokdrqtghpuanycexifmzb
ztdewbaigkonsqyvumpr
mngkrzouxbdapeqsftihy
odiaqkubgtfrnzympes

znspbjwtymoghcdfluerq
qdspobimcxvtzhewya

wvryt
vjwtyaor
trwvy
vwrty

bpnd
nasdfm

nplfiwzjxht
xjnziptwh
pwtjhnxiz

dozimxujkn
djumoknxz
odmjxzbknu
doxumjzkn

rbfgqwj
fahqbjw
fnybmwq
qbzwf
ilwfpcvutqb

qgkbjltchfyavedu
dmawbygrltq
atipzbsdxgyqlmo

ckye
jeyi
uemy
xveby
ywuei

eskrx
emjrx

wljvungdr
bnuwdsexkmap

xupgbd
sdgp
gpcd

eota
aeot
aeto

ysfc
fysc
tfscruy

jgkhymseqrdtnba
ymkqbtgshjnrdae
tyebmnrhqksagdj
sbhtjgqdkemnrya
hdsqabmygnrtejk

f
kf
f

ytjcedxaz
apzkbof
avpsxug
niahrmlw

rfudz
fhazyk
ibxtwvsglj
qd

c
c
d
hn

cskuido
uskco
scoku
koucs

tvb
ycdv

qhirvcsbd
rihejdpm

iybrma
yrmbcu
byrme

zed
zkxw

qzfnvplshubydexi
pnlvhdyeixzsb
zlnvxihypdebs
ydzvphscijlxbwagrone
xilvbdphszyekn

gvtrahxldoikzwq
qrzwhkvtoidlxga
zkxqigwvrhtaudlo
torivqgdhzknxlawp
ldxkrqywtizgohav

ubndcrqswvelxtzoayhj
hyrwatzedscqjxlvoub
zxrdhuoasqylevcjwbt
eqlhxworuvjzyctdabs
wtcuvhdelozqabrysxj

elcpsu
cpsule
cpsleu

zlh
hzl
hzl

dxiga
girfkm
jeigrf
ysnulphgzivq

psat
oa

ue
ue
ucpe

ogjh
xjothga

koftzwcqubmnlrhjipyseag
eoafnwvygpmqtjirblhczs
pmwsiqyhtjanogecbzlfr
ifgaznswcqjumyrlbeptoh
gnewjrhspaqtulmcoyikfzb

ix
i
i
iea
i

ulqaygxi
fdqeyauxg
zjuagxqpy

h
h
h
xh
ho

vxaw
wav
wvai

mxtkdpoeyi
mxetpyikdo
otxmedyipk

wdcnobehuztmp
ukqmabxsozjcpv
wpurectobdlfgmiz

bgdqnwvozepkhriacuxyl
nzuqydvhwplrockaegxbi
bagkhpzxlouyqcdewrinv

qhcltbxvwafsjyzdrm
qxstfdzejcylabhr

fmxdshjvzt
wlcqmjbgsyozten

iefmpxscygwlan
gmnabscpxeywvi
idamrwxgycslpen

oy
dhoy
yo
oy

xuwvolghnsiamrecbpf
yfnlkjpwtrosxcqbmadu

zevtqpufwohjsym
sujpcgxvfteyizmwkhq
luwdshftvempyjqbz

xwalcisvjbg
wmxgcshavjd
cxsjwgavn
wjxgavsc

ugbazkrqhewcsnlyv
kgbzywncqvelasr
vngwszacqbykrel
gsbwayqzrckenvl

odyfsmr
hmlazrkfqid
dmraf
dnfxm
ecgpbtvdwujmf

mgfdipvqwylt
yvmfgwpidlz

bqmlkj
ugvjpkf
yxnokt

uq
uq
qu
uq
qu

wi
iw

wzldovhb
mxwoabdljh
wolhbsd

wl
ly

izlwhjont
tilohzvnjw
xhwjpitoblz
zhjwilot

hywqgtx
qhkpmtyxgb

ofcbmgz
mgzoc

twrhoiq
xnwtvqpucoh
wohtqr

kyubjpqwmxlefvdh
wmipajfxdleyshukq
kmhulexyqfpjdnw
yefdjlwpmhxkqtu
mkhwyxldupfeqj

ofwrlh
jtxpb

gluokvay
sam
ba
cnaz

uxfmyszp
msuyfxz
zuxymfs

bkszijdoqnmyecutgwaf
beomaiwdjnfqcskzygu

jnvfxyis
isnyjxaflvb
dsfyncxivj
xzvfsinyj
jvfsxnyiu

aucnsglqtwydip
qwuiatngsdlpcy
pqgidcnsyltuaw
epioacwqylsmduntg
ypuwcilnasdtgq

luscjpnra
gzhfvwlnpkj
ldpnj

gdulsqjpoaybtciwnxfrmke
kimlncroyebfjqwgdxpastu
rkpmqycojftluednibgaxsw
qnmikcgjbftduewsyoparlx
qdotrglyakwsjneicbfmuxp

n
n
n
n
n

xmwdne
mnw
mniwg

dmikwtap
pdiamwkt

xwrtgqjol
xtloqrwjg
iwpxtgqljyozr

szatehfijudvnmcwgl
xiqpkol

bof
vsgk
b
l
f

qyjkltr
uzjk
jk

byhzmsgqkiw
gzksiqhxwymb
yghzswimqkb

xwdayibf
lobrevphkjsqtm

zbq
qa
qgjcnoki
qb
q

cy
cy
yr
yp
ysmjxl

mpgvawkxqilefjhzd
wcgrptlbiyqn
orgpsywqilu

tqnhwfzrval
sqjhazftln
ibdqltngpahfz

tkb
kbt
tb
itv

rastml
mac
oaqmih
cmoa
caomqh

euwfnjhqzk
zequbkfswhn
ufnhrxwekzq
hknwzueqxf

oa
oaz
ao
oa
ao

savhkgfjimzxetqbw
agbqjkfhevxtmzw

evxrktf
pueltvmodwcjixzfs
vfrbxyetn
vfrtaxe

msgunwydrpjabkle
lujndkpragesybwm
mweglaujkdprynsb
wmjdngaksplyeurb
gnujemlwpakrbysd

cr
dc
dc

blqpgi
lipqbz
zbqlpi
pqbilv

xydkpmghbc
pgykdmcxburl
zcykpgmxdub
dcvabygjxkmp

jprbeoa
frnb
zgcvsdwuqk

vtxoj
xvtpjql
divrft

gupfytlizkdbqex
eigxmyfbsctqunr

tuqs
sqtu
tuqs
uqts
squtv

byhigkoufalws
ak
jatke
akt
qakvm

ywdflkx
kylxwdf
dlkwxfy
xylwkdf
dkfxywl

f
l

aftsgyjkxvnzwue
wzkeljtasxuyrg
jtkswzyeaxugb
egkxjazyturws

itz
djt
ctv
gnthe
jt

yshu
sh
slmhgj

z
ji

obgi
sk
lks
dhjwanv
rm

xpiucywoa
uyopaxwncf
yvlgmouxwcjb
xuywfco

wlinyhgvomaft
fvgslaxtynwqio
lapiotndgyvwfm

qeckphv
udr

u
u

jctghsovrbxuldmeq
svrtubcqmpoxkedj

tdgkfpsncb
hgzlickap
wtgkfbqcp

znstglqikxh
lhbtxigmkaqcz
iqdopufzerxwgltkj

hlp
pvh

bdcmqvigkl
bmqghkvnladi
qigdvlcbkm

buphave
vbuhpe

ojupf
fupjo
opfju
joufp
pjfou

tdlgefz
zdgfelt
xdzgftl

m
a
a
e
m

aetxgnmbwlqorhcv
vdetolsmuwxgrznf

wjmgzhstnklvpqfre
zkjvmgihpasqtfr

uvzpwsegyldkt
vwuzngtlkyepd
lbvweapjfcqyxktzg
lmzuwepvghtyk

aqxirdlg
kv
f
jkm
kmf

lntwbeguivhjrczomdysk
lzshbojydwcitevuknm
tfosedjzwuhvinlcmkby
ywtsqhbidjuzcvklemon

cbarozvgwudfy
vradygocbfuw
gyurwbvdacof
vcagouydwrfb

uqtiably
yaqbvluit
brxqfymulati
tdiscyqjbalu
buqaliyth

xhi
ixh

qyiklhnzxm
mrxykqnh

snhiex
qjhkdwixvg

ruahnkqvztblspj
rnkhvatuqlszbpj
zurblstvqpjnhak
auvqlgtzsrpjnbkh
hjknpvolqbzrutias

urihbgctwdlfpkexzvy
ktecgwvfyuphdzlrxb
dfuktzavxylwbpogerch
yxtvjcdrpmubshfznewgkl
lzbdcgfyohvukrwpext

oainb
aqlfwdkj
rboa

dvhuwiytqfxgocs
lqiuhadstfgcv

xvwuibrygcldtao
cvidylwaxgshr
enarwdcymjpifvzlx
cawrqxygivhld

gebqtnvmsrcwiyadhx
ciodjyxbhetsngvwmraf

qujrfpiywcsxetovn
jhpriwqetfxucsyon
uqwrzpsmgiocnljefxbdy
kqrneposjfaiwxuyc

jpbilafygw
fbhcgjwlkpeyiau
yjiwalfpgrb
ylwabpgifj
jwpibylafg

wtszqgebajnh
zbwjthgeqasn
sqzbtegjnwha
sgjhtnzqbewa
entwgshqbjaz

rtjzh
jzhvr
rjhzt

inegmbt
mitgben
eimgtnb

rzc
jmx

mgjcabfol
hiavxjypk

hdewoxam

lfkjgtrdqeyv
oimtuah
xztb

hcr
qed

mhesdwpfgabjqut
eupahqbgdsmftjzw
bjeqagdtphwfsmu
oagpdekhwbtjfimrsu
ygcxfdhewbumstjap

osq
oq
oqs
ofqe

yv
yv
vy
yv

ejarwcshd
shecdajrw
cjshaweondr
rsdhajcew

yghdti
ihtdgy
ityhgd

cvbgprtdenxmf
uwoci
qacoh
ocjs
chkzq

gvj
ixopnqr
w
kw

zgjdhpumekvwi
saxcntfgjwzyqpikd

ypa
yp
mzdubpcq

tn
wosvaye

qlcperntgzsfwku
dqtcfnpswxlbu
twsnulfchqp
fntmwslpucrqg

kuapxrhzscefjgnyb
xrkqbyejnp

nzwdcjqgbkuyompih
qlbioakexrypd

xzpuvdgbksoehtya
ptsoyudzbkaxgevh
xasvzgtpoheybudk

cfsu
sucf

mgnyxeosiflbjarck
tilsoanckhxygeq
koaglncxyisweu

mdxw
mwx
mwxkr

xhbmivwocpnfj
gonpwvcbqjuh
kprwyvsnobazedchj

h
h
h
h

ylihseo
ielos

uceik
nmdu

ieoupwmsczxn
cwnfaixuphrmozs
izswomcpxun

szibkplxgjecfu
fueklisgcznpx

ozlrwfbyvpha
haobxsevztf
ftzbaesdovniqh

awlj
wlaj
whjgazc
wlfaij
wjab

bdxcvsi
bhsutypqzaeji
goxnlmskib
xsicdwb
isfxbr

fgyekxoc
fkoygxec
fexokcyg
xfaykeogc
kfexogcy

wfzxvbnuomqyprijakth
ymbqfakjlpoxuvrwnzth
tzxuyrnpmbhjoqfkvwa
xbkuwjanqvfrpmhztyo

rj
rj
jrnfo

ktn
njk
nk
kn
kn

expjkivrsbtay
hjripyesaxtb
aytexdibjlprsu

uaexqwnogypcrltfj
glsbvjofretayiunkxd
gtazeownjmuhxqlfry

ktbxcwjnqzgfo
acufingqsw

hk
kha
hk
kh

ckeiaqhvo
avqokcid

ydsjhnoef
hdjoesnyf
edjshytofn

djntazq
wryjkcglx

fm
mab
zhkog
a

in
i
i
i
ia

usx
oxsu
buxs

wuvrcathfyqjkbgoield
kryebltidhunwv
eyzbudwtvhiklrx

komt
okmt
xobvuhmrkfe
tokm

knwzvmxdhga
kdniavoyxgm

ujfriox
rhjsmfuo
dfobuejvzr
jogsurfyq
sjoufr

rkbwpliavcg
xhtvqgcpwy

cdbfvigluhnqmexjkow
clvbitqhdpkygoxefjr
kodfbvhclegqyizjx

hajewmgkrufxos
ujoyahirmgswefx
rwcmohgsfexuyqaj
fhadmlogejprsubzxw

kafstecyuloihbdn
tqwzspanoklj

xmefigduhoykazjwsl
kslweodzjihfaxymu
sfmkdyaxozwijeulh

saxvbgpytiewmzdko
wkpimyxzbgoeats
wgoxhtbzayeksijrcq
tzyogakbxuswei

igyrvzsbhjlfmckptnx
hpuinvzfklbctymjrsgx
fcbxsipgnryztvklhmj

auznpvehdqbkwgsy
skavghewdbnlzu

knlhpeiayrjqoxmvgz
qalkjxzopgvrnhymie
rkxmqpovhleyzjaign

wkcj
zuexp
j

isvcz
czivs

zcponrsbfwyd
bfwdrpzoycsn
wcpyoznbfsdr
bnrfpzcysdow
rpzycswbdfon

ovrxfapbi
irbfvxpao
xibrapofv

dksnutobj
olysudtvbjcn

mu
mae
me
sm
ybm

bkdpatzy
yzikp
lyzipkx
jxpzcyk

uv
upzv
uv
uv
iuvh

emjlvyhcfxbwg
spodukaqzlitrn

ksdr
wuvs

uphbk
ukhbp
phubk
uphkb
buahpk

cbshmdpuavwrtenlzyo
brlcwapsoevmyhtudzn

csqumglrtnk
msltkgncqhrv
slmdqtcgrnk
mnqgcrslkt

piokdlbgrxems
xpdmklisorwebg
xlrkbpomdesig
exglsbiropdmk

b
g

ghclsovdmtxq
hmglcoxvsdtq
thdoxsgyqcmvl
ogrhdsxvqcltm

ounsldtx
lunbowyt

vfrkutowbmgdc
oijnkpgbtud

lhujrbktn
dijr
dpzqjra
dfszrcj

rdbn
zkjcmatoub
frbh

sjviweohd
idso
sodi
gumlodisy

lhrdskgfeycbtpi
lcihtfkysderbgp
tdrpfihekylsbgc
gpsklchbdftyrei

xlajt
gnjtxq

tqzbcshjlvgpoyximdweuk
vyjibzqheuwpckgdxsomt
yknthubopzgmwqadexsjifvc
yvkxumjsbdzhqowipgtec
hisuqgkcjdxtzmevwbpyo

uketflynzcw
yuckntewfzl
qtcelwkynzxfu
wectmkzfylnu
kewzuynlftc

vswzacr
wvsazrc

e
ed
e

hebto
wtmsib

lwutqp
qltupw
htqpwu
pwlrtuq
tqwzpu

hdjvamqcegzbspiukrt
gjhcbtkspumrdvazi

jnbadfseltgwqicyp
fjaplrsgnibycqedw
nckyblgamoqpjhvdwefsi
upysigwjecnqaldbf
sbjlgfpyedanqcziw

eqfocbxlrn
kafbngoxrl

qvsw
nik
hrw

snufiz
uzjsfni
nsiuzf
fyiznous

ckpibgjwafuh
ghiljrcxakp
khjpgaweic

msvnuqxypgofjih
omqxvfygijpsuhn
ymquspjvonxigfh
oiqvnypuhjgfsxm

nkemyfxgpijluhqsdabvtrwc
ksfbumgetnavpijhqwxcyzldr

c
sc
hcd
sc

tuxfnesdclrvwhqjb
ocbiahyvljsxtpfezr

fbskmvrij
vifscrbmznk

myzb
umz
zmwr

uh
uh
uh
hu
hu

goajwbie
fdrx
xzp
nvftuq

iuavjemkrhgtpzs
zjmikaovtehrscu
izhvtnkbsqwfdjamue

xwtdegkbmzfpohs
tmxsdhpkebw
pmxkseihbtadr
txgcepnzsbdkmwh

zfahslmie
asnhezilfm

ykctbsjxuprlmnevzo
yvuropfkshndxze

lqavxpoykrctuhdbmesfjznwg
hmarbvonjpgzwcxdqtkf
dwohqktamjivgbpcrzfnx
ptfqwhgzkjmoxrcdnabv

zfm
fm
fm

ilcqbzxkmphwody
wzbqomvhlcxpyd
bwyoeczldqxvhmp

judhxf
xjduhf
juxdhf

q
u
qf

jcawmzxvihftkrebngo
frgtbhomlzynwexai

aih
ifwqduk
ejhi
ipz
in

xofynradpmsezlkt
sbkplaoydqmfnzcetr

crqaz
vrecpaqt
rcayq
qayhrnc

fjzuibwdqgk
ldsxctyevmafjnwquir

zxve
xvbrelq
mxpgujytowk
xrh
exnh

m
p
m
yz

bporefigvm
skdapgxintqh

mzrk
jov
s

seg
dgm
dgm

oermkyc
ecmkqyr

mftuja
xatug

lvp
ev

uos
xvstu

ievoqg
eozig

mcjvxrazogyqdutbnskfhliewp
muptksjqehgblvfwidocnrzxay

biqpca
qiacpb
axiqpcgb
aibcqp
cziqbap

extusijradokfy
leropqdtgbwu

dyrhapgfuzibswlxt
tysfcbldhjiugrpxaw

vxpwancrbigsezdljf
rbgaewzhpvflucnsjd
beswjpvgadtcmfzlrn

mqhdsrki
nwobaxgyf
ckeujlv
kmip
hurzmdkq

aj
n
n

qxolihwd
ndopqzxi
gqfryxcodtie
qjovsbximd
idxsoq

owpxfnubqckaygihvels
khuogazqblysetnxv

lw
wl
lw
lw
lw

nbmictlsfpe
lbpjtsen
snrepulbaozq
hpldsebkn
pswebvxnlh

kaxnq
cgzdqulw
qtev
kqxat

mktvdgqrjfpauwcx
xvqmwbatfrpcgukj
xqjhuimftgavwpkrc

suhrkjm
drhjoepn

muarvhpcj
budyqsxio
ukegjn
wuca

pfqy
pqf
qpf
fcpq

km
idfo
n
i
s

dzrel
rldez
dzlre
rzled

xab
zib
vfob
bepyc
zjbln

qhuk
rvuh

ijoanqflgevpkhbdcmtry
cngpmhtrubiwsyafelk
mnaygtrkwfilchepb
efxylarhmntpcbgik

kabselvfz
jzelxsbf
zldcgeuqwyib
blatez

mskcylp
skucmpyl
lynkcpsm
myascplkf
ymplkcs

genqxfmpbscruwavtz
qscjwoflrvtgmey

gqx
qgx
qxg
gxq
xqg

orpvatekfxn
kfyhvbxjatponr

dltjwieqhbngzfru
dnruzxtpeakjcg
dueojhtnzgr

xodnalcfue
alnfeco
leaconf
clbafone
ncafleibo

vlg
lvg
lgv
flgv

l
l
l
l
l

qt
qjdt
apsgtq
dtbqy
jqyt

vh
hex

zumjtpa
uzjsm
mjsaczhu
gmikjewuzf

ji
q
k

detaocgxyvrufmqblphj
gahmorvjytlbxfpquedc
jxlhgyfeovrmqscdatpbu

rnkvuqidtco
tsfkzvirndoqu

diafzejbwokm
wqizdyotjfuemav
szfcjwndaiemo
jfzdecamwoi
ewdafzmjion

zdmspogfkrenxycvhlwiqu
urfosnixgqhkwjcdevmyapzl
ekhgmclisqyptvrufxnzdow
vysbilkqumcprdenhwxfgoz
rskdplyvcoiqehmzxgwftnu

ukqch
qgkbuhc
tquchkd
ckquh

db
o
d
rv
k

piaofhxmwnvtcgu
emxuwgapitnfvoc
pjutmxnvaofcgi
aixqcbmpozngfyutv

p
p
p
pe
pm

ibwkp
jfirb
bzvij

iokrxmlg
hmtkogirlx
ilqkxmgor
xgihomtrlk
fovpremiubnkxgl

uegzim
mnrxdsweltb

nrtehv
ntrvhe
hnrtev
tehrnv

scfzhav
vhszce
zlshtacv
chzbvixs
shuwvrzc

cnuzjowplfbxv
lubcfxvzjnwp
junwicfbxvlzp
fbujwlvxncpz
jlbfrpvngucwzex

gjys
vjgys
amlzgditx

anseyxckmvhfizwjglotb
onwbvsxftmljgyeihkazc

gabe
mbg
ujdvwgb
bg
gmb

roygu
uyvgor
ruyog

i
y

iynwubv
uivynw
yniwvu

kwctelmjza
wktelmazj
jezakltwm
mklwjfeazt
kteazmljw

tjprfkmnuscqleb
etbskqmulncjpfr
qmtucbrsnjkpfle
uenbclkpsmjqtfr

bijsc
sicjb

tmjqbgxyeos
tgysmkbo
matbvwhgyso

x
x
njw
x

wqpulj
hmcobyzkendg
atlxspvifqr

fndkgycm
kydfmrc
corkfdmy
kdyfcm

qw
i
asji

mbhqsncogdirywzf
egdrcyhowmibvnszf
ygfmosnbihrwdzc
wimrnhsfzeycodgb

rlqy
rl
rlm

e
y
r

livzeg
kilregzpn
ligzqeu
lizge

x
x
t
x
x

dptigbzqfjs
fzsudijgtrbpn
fwgzpisbdjt

npthoj
dremkjnozxbuf

m
ym
m
mz
mdt

wtphexvsjmgy
ctzselmpjg
jbptdisemcgla
jzteslpcmg

svdlony
undlsyo
yqoinltbs
olrymdns

lqfnrvmsbhpwuj
uxa
aku
iuoe
uycd

ygjrpoanvtzlxbu
rlyuadobvpzgt
sylrzpbtoavgdu

dnycvol
ekcy
ychpjw

pycdmtaqorxg
yzalgerstudqpc
hiqvrpdkwtnjyacg
sqcagtpdyr
cgsdfaqrbtyp

hpljymuotgarsxw
ygljahmpwunt
kbtlfayejqpzhwuvm
mtahywpculnj

noqzyaifsmvrhxkju
jhouamryznxfkqvis
moahzirsqfjuyvknx

knxuvyizohfbp
ynpvbzahfkouisx
unzblxgykihpfvo
kfhxoubpznyvi

xlfmqhkjiuosrvda
omsxifhujdqlkavp
umfkiwxdlvqahoj
juhaimqvflxrdok

rw
j
amgfyn
wj
bz

yijwmopraq
ioqmawrylpj
qioayjpwmr

dyvfzhmetqwrga
zfetnamhcrjw
lwrzkansftehm
mafhtrzlew

jolep
jo
jao

gwuqrp
rgqpuw
gpqurw
quwrpgo

hrfol
nkrfaleo

htrnmvfpqbckduwloyez
khytlsgrdopzeucfxbqn
dizcpufnthkeyolrqjb

zpyjmxrfdtwuhsblav
vtdjpxsylwbramuf
urwsdbxamylfptjv
uaysdwjrvmptxlbf
vpmsudtajlrwfxyb

axtiwvmg
rniv
iv
quevi

pmoyzhcbijtqagskvnulfewd
tpneiqdhmgbkaojucyzwfsv
zutihfdvogwbjseayncqmkp

lsirvdth
jdypag

tjqef
bxiytelq
entmquof
dqtevr

dwmxfunpzc
pmxfzdcwu
dwpcfuzmx
dfcmwzxpu
wxzdmufcp

hmygqnbwisvakefc
cesgpbfkavmqrny
uaodqcjnsvgyemfkxb
yvchkebnagqmfs
fsqemgytklcbavn

asiuw
wiu
iuw

fbuwvehgm
fwvmgbcuhe
hzxmubewf

zwkfytmcaijbelr
xur
rhgu
hr
pru";
        #endregion

        #region Day 7
        public static string Day7 = @"striped tan bags contain 2 light silver bags, 1 drab black bag, 2 clear tan bags, 2 mirrored tan bags.
dark black bags contain 1 vibrant indigo bag, 5 muted gold bags, 4 bright tomato bags, 3 dull tan bags.
dim silver bags contain 1 vibrant black bag, 3 muted cyan bags, 4 plaid turquoise bags, 4 faded orange bags.
faded maroon bags contain 3 pale aqua bags.
dim crimson bags contain 1 faded orange bag, 2 plaid silver bags.
plaid indigo bags contain 1 dull purple bag, 2 plaid cyan bags, 4 mirrored gold bags, 3 striped magenta bags.
mirrored brown bags contain 4 posh bronze bags, 3 dark brown bags, 5 shiny violet bags.
faded gray bags contain 3 drab beige bags, 5 vibrant olive bags, 3 muted cyan bags.
bright salmon bags contain 2 striped white bags, 5 bright plum bags.
plaid brown bags contain 1 mirrored violet bag, 1 dark turquoise bag, 5 dark gold bags.
vibrant lime bags contain 3 posh yellow bags, 5 dim aqua bags, 1 plaid coral bag.
pale gold bags contain 5 vibrant cyan bags, 2 faded red bags, 2 mirrored purple bags.
dotted orange bags contain 5 faded fuchsia bags, 1 light indigo bag.
mirrored chartreuse bags contain 3 bright black bags.
drab blue bags contain 1 plaid turquoise bag, 3 dim red bags, 5 dim lime bags, 3 dark maroon bags.
dark violet bags contain 5 plaid lavender bags, 1 dark salmon bag, 1 muted yellow bag, 3 drab beige bags.
pale salmon bags contain 3 dark red bags, 4 clear violet bags, 4 dark blue bags, 2 pale olive bags.
wavy fuchsia bags contain 3 striped plum bags, 2 light violet bags, 3 dotted yellow bags.
shiny gray bags contain 4 dull salmon bags, 2 dotted brown bags, 3 bright tomato bags.
dark plum bags contain 3 pale red bags, 3 striped gold bags, 3 posh bronze bags, 5 drab tomato bags.
mirrored green bags contain 5 faded red bags, 2 posh fuchsia bags, 2 dim green bags, 2 posh green bags.
drab brown bags contain 4 light violet bags, 2 mirrored salmon bags.
shiny magenta bags contain 4 clear turquoise bags, 2 posh silver bags.
clear olive bags contain 5 muted gold bags, 1 bright orange bag, 2 clear turquoise bags.
dim blue bags contain 2 pale red bags.
drab indigo bags contain 4 clear fuchsia bags, 3 plaid orange bags.
dull tomato bags contain 2 wavy purple bags, 2 faded brown bags, 4 plaid indigo bags, 3 faded silver bags.
light salmon bags contain 4 clear crimson bags, 3 clear gold bags.
light green bags contain 1 dull olive bag, 2 pale gold bags.
muted coral bags contain 4 pale indigo bags, 1 dark lime bag.
plaid chartreuse bags contain 4 dark gray bags, 5 clear tomato bags, 3 dotted brown bags, 1 mirrored olive bag.
dull plum bags contain 1 wavy gray bag, 4 wavy indigo bags, 4 dark black bags.
plaid crimson bags contain 3 mirrored blue bags, 2 dull turquoise bags, 1 dotted bronze bag, 4 clear coral bags.
light tomato bags contain 3 mirrored chartreuse bags, 4 faded bronze bags, 1 bright tomato bag, 2 muted bronze bags.
muted fuchsia bags contain 4 plaid turquoise bags, 5 light cyan bags.
wavy salmon bags contain 3 dim red bags, 2 muted crimson bags, 1 dotted purple bag.
dim turquoise bags contain 5 dull salmon bags, 4 pale crimson bags.
striped red bags contain 2 clear turquoise bags, 5 muted orange bags, 4 mirrored coral bags, 1 dim aqua bag.
light purple bags contain 1 clear chartreuse bag, 3 dull cyan bags, 5 light fuchsia bags.
pale purple bags contain 3 plaid fuchsia bags, 2 posh cyan bags.
mirrored violet bags contain 1 dark silver bag, 2 faded silver bags, 4 posh gray bags, 2 vibrant crimson bags.
mirrored olive bags contain 1 posh fuchsia bag, 5 dim turquoise bags, 5 dark red bags, 5 shiny magenta bags.
shiny aqua bags contain 3 vibrant olive bags.
bright crimson bags contain 4 muted cyan bags, 4 dim red bags, 1 dotted chartreuse bag.
light gray bags contain 5 plaid coral bags.
faded chartreuse bags contain 4 dark coral bags, 1 muted beige bag, 4 vibrant purple bags.
clear gold bags contain 2 plaid coral bags, 5 light chartreuse bags, 2 posh olive bags, 2 mirrored bronze bags.
faded tomato bags contain 2 light silver bags, 4 striped yellow bags, 1 light indigo bag.
wavy crimson bags contain 4 dotted beige bags, 5 light violet bags.
plaid aqua bags contain 5 dark aqua bags, 3 vibrant gold bags, 5 bright orange bags.
drab yellow bags contain 1 muted bronze bag, 4 muted blue bags.
dim yellow bags contain 3 wavy olive bags, 5 bright blue bags, 3 faded orange bags.
dotted teal bags contain 1 posh tomato bag, 4 drab green bags, 3 light plum bags, 1 light green bag.
plaid gold bags contain 2 faded beige bags, 5 clear teal bags, 2 pale coral bags, 3 dull gold bags.
faded indigo bags contain 1 muted yellow bag, 5 faded brown bags.
dull coral bags contain 1 dotted tan bag, 5 dull olive bags, 4 shiny red bags.
wavy tomato bags contain 3 dull purple bags.
pale plum bags contain 2 mirrored fuchsia bags, 4 muted olive bags, 2 bright purple bags.
bright red bags contain 2 muted turquoise bags, 2 dark crimson bags, 5 dotted purple bags, 1 muted beige bag.
vibrant maroon bags contain 4 bright indigo bags, 3 mirrored turquoise bags, 1 dull plum bag, 1 dark yellow bag.
dull gray bags contain 1 dark chartreuse bag, 1 light orange bag.
plaid silver bags contain 2 pale gold bags, 5 dotted purple bags.
shiny violet bags contain 3 pale crimson bags.
shiny fuchsia bags contain 1 mirrored silver bag, 4 vibrant olive bags.
drab bronze bags contain 5 wavy beige bags, 5 dim chartreuse bags.
dotted crimson bags contain 1 wavy black bag, 2 muted teal bags, 1 plaid cyan bag, 5 bright orange bags.
dim beige bags contain 5 clear orange bags, 1 dim violet bag.
posh red bags contain 4 faded indigo bags, 4 dim coral bags, 1 striped beige bag, 3 dark lime bags.
dim teal bags contain 2 bright red bags, 5 dull salmon bags.
drab magenta bags contain 3 dark crimson bags, 1 muted gold bag, 3 posh silver bags, 2 dull aqua bags.
posh aqua bags contain 5 clear crimson bags, 5 dim turquoise bags, 1 clear coral bag.
mirrored tan bags contain 5 shiny olive bags, 1 light violet bag, 4 muted gold bags.
bright chartreuse bags contain 2 plaid brown bags.
drab purple bags contain 5 wavy tan bags, 5 vibrant fuchsia bags, 3 pale brown bags.
faded coral bags contain 5 light tan bags, 2 clear crimson bags, 1 dark gray bag.
posh indigo bags contain 3 plaid silver bags.
plaid green bags contain 5 vibrant teal bags, 4 mirrored gold bags, 2 striped silver bags.
clear tan bags contain 5 striped magenta bags, 4 faded chartreuse bags, 1 dotted purple bag.
posh white bags contain 3 bright black bags.
dotted maroon bags contain 4 clear teal bags, 1 wavy fuchsia bag, 1 posh turquoise bag.
dark magenta bags contain 1 vibrant violet bag.
shiny indigo bags contain 4 dull lime bags, 1 vibrant turquoise bag, 1 light gray bag.
light fuchsia bags contain 4 faded silver bags, 1 mirrored indigo bag.
light plum bags contain 5 clear crimson bags, 1 posh bronze bag.
dark purple bags contain 4 clear indigo bags, 4 bright tomato bags, 1 faded olive bag.
posh plum bags contain 4 mirrored black bags, 5 plaid silver bags.
wavy bronze bags contain 4 faded magenta bags, 4 dotted chartreuse bags, 3 striped beige bags, 2 striped crimson bags.
clear teal bags contain 2 shiny olive bags.
plaid coral bags contain 4 drab tomato bags, 3 striped gold bags, 1 dotted lime bag.
bright violet bags contain 3 muted olive bags, 5 plaid orange bags.
vibrant white bags contain 5 light brown bags, 2 pale olive bags, 5 vibrant violet bags.
shiny blue bags contain 1 vibrant coral bag.
pale yellow bags contain 2 dark tan bags.
wavy cyan bags contain 2 faded brown bags, 3 dotted tomato bags, 2 drab cyan bags.
dim aqua bags contain 4 dim fuchsia bags, 1 dark red bag.
mirrored indigo bags contain 1 dotted purple bag, 1 dim violet bag, 5 dark silver bags, 2 mirrored blue bags.
faded red bags contain 3 drab gray bags, 2 shiny magenta bags, 4 faded silver bags, 2 dull tan bags.
clear silver bags contain 5 shiny gray bags, 4 light violet bags.
striped yellow bags contain 4 plaid turquoise bags, 2 vibrant green bags, 4 posh bronze bags, 2 dark crimson bags.
light orange bags contain 4 bright blue bags.
drab fuchsia bags contain 3 light lime bags, 1 dull maroon bag.
vibrant indigo bags contain 5 faded silver bags, 2 striped white bags, 5 shiny magenta bags.
pale black bags contain 1 dull aqua bag, 1 striped gold bag.
clear lavender bags contain 4 posh bronze bags.
bright tan bags contain 5 posh bronze bags, 2 faded coral bags, 2 bright tomato bags, 3 posh fuchsia bags.
vibrant blue bags contain 2 drab magenta bags.
drab red bags contain 5 bright purple bags, 1 mirrored fuchsia bag, 1 plaid salmon bag.
faded lime bags contain 3 dark salmon bags, 2 drab gray bags, 1 vibrant cyan bag.
faded teal bags contain 1 faded bronze bag, 3 bright blue bags, 4 dull salmon bags, 4 dull white bags.
dark yellow bags contain 5 posh chartreuse bags.
dull white bags contain 3 light green bags, 3 drab gray bags, 2 mirrored tan bags, 2 bright black bags.
clear yellow bags contain 4 clear beige bags, 1 faded blue bag.
light coral bags contain 5 dotted orange bags, 2 vibrant plum bags, 2 wavy purple bags.
faded orange bags contain 1 light violet bag, 2 faded olive bags, 5 posh silver bags.
bright olive bags contain 1 clear purple bag.
shiny beige bags contain 5 pale silver bags, 3 drab orange bags.
dark aqua bags contain 3 drab gray bags, 5 shiny plum bags, 5 dull purple bags.
pale green bags contain 4 clear olive bags, 5 drab crimson bags, 2 dim silver bags, 1 striped fuchsia bag.
muted violet bags contain 4 faded plum bags, 3 bright beige bags.
shiny brown bags contain 4 dull tomato bags.
light cyan bags contain 4 shiny bronze bags, 5 faded beige bags, 2 shiny plum bags, 4 mirrored purple bags.
plaid yellow bags contain 1 vibrant bronze bag, 5 light black bags, 1 plaid coral bag, 4 drab tan bags.
light chartreuse bags contain 3 drab magenta bags, 5 mirrored coral bags.
shiny orange bags contain 1 drab gray bag, 4 bright tomato bags.
mirrored blue bags contain 1 dark coral bag, 3 vibrant purple bags, 1 mirrored turquoise bag.
clear green bags contain 4 wavy orange bags, 5 muted black bags, 5 striped gray bags.
bright silver bags contain 2 plaid aqua bags, 1 bright blue bag.
clear blue bags contain 5 dotted tan bags.
posh silver bags contain no other bags.
light lavender bags contain 5 dark gray bags, 2 clear tomato bags, 5 muted cyan bags, 4 pale cyan bags.
dull salmon bags contain 4 dark silver bags, 5 mirrored coral bags, 4 bright red bags.
clear purple bags contain 5 wavy yellow bags, 2 striped orange bags.
dark lavender bags contain 3 clear purple bags, 4 vibrant cyan bags.
dotted white bags contain 4 light fuchsia bags.
mirrored cyan bags contain 4 dotted plum bags, 2 posh salmon bags, 3 light orange bags, 3 muted teal bags.
shiny cyan bags contain 1 dotted blue bag, 4 striped crimson bags.
clear tomato bags contain 5 vibrant green bags, 1 drab lavender bag, 1 dim coral bag.
pale olive bags contain 1 faded red bag, 5 dim lime bags.
wavy red bags contain 4 faded orange bags, 1 dull white bag, 1 dotted chartreuse bag.
pale violet bags contain 4 muted turquoise bags, 5 vibrant yellow bags, 2 bright plum bags, 4 posh plum bags.
dull lavender bags contain 4 faded turquoise bags, 5 striped gray bags.
drab gray bags contain 5 faded silver bags, 1 plaid turquoise bag.
vibrant aqua bags contain 2 drab brown bags.
shiny tan bags contain 2 dark red bags.
clear turquoise bags contain no other bags.
pale teal bags contain 4 dotted brown bags, 1 dull lime bag, 5 dotted purple bags.
posh teal bags contain 4 vibrant purple bags.
drab orange bags contain 1 striped magenta bag.
plaid salmon bags contain 3 light chartreuse bags, 3 dull purple bags.
mirrored white bags contain 2 light beige bags.
mirrored beige bags contain 3 dim tan bags, 5 bright salmon bags, 5 pale tan bags.
mirrored maroon bags contain 1 dotted salmon bag, 1 vibrant yellow bag, 4 bright cyan bags, 1 clear blue bag.
dim lime bags contain 5 light violet bags, 4 drab gray bags.
pale bronze bags contain 5 dotted fuchsia bags, 1 dull tan bag, 2 faded fuchsia bags.
vibrant turquoise bags contain 1 dotted tan bag, 2 clear purple bags, 5 dotted chartreuse bags.
vibrant crimson bags contain 1 faded orange bag, 3 light chartreuse bags, 3 dotted purple bags.
dotted green bags contain 2 vibrant lime bags, 1 dotted bronze bag, 3 faded chartreuse bags.
mirrored gray bags contain 4 shiny maroon bags, 2 faded salmon bags.
muted purple bags contain 3 clear magenta bags, 1 light green bag, 3 faded orange bags.
wavy maroon bags contain 4 muted beige bags, 4 posh gray bags.
clear aqua bags contain 3 clear fuchsia bags.
mirrored red bags contain 3 muted indigo bags, 4 pale lime bags, 1 dark silver bag, 3 mirrored tomato bags.
vibrant olive bags contain 2 shiny gold bags, 5 mirrored tan bags, 2 pale olive bags.
dull purple bags contain 5 dotted purple bags, 5 dotted fuchsia bags, 4 dark salmon bags, 3 faded olive bags.
muted maroon bags contain 1 posh green bag, 5 faded bronze bags, 4 striped gold bags.
shiny chartreuse bags contain 3 bright crimson bags, 4 dim purple bags, 2 dark violet bags.
pale crimson bags contain 4 dark silver bags, 3 dark crimson bags.
vibrant fuchsia bags contain 3 shiny gold bags, 1 wavy bronze bag, 4 wavy purple bags, 5 dark gray bags.
pale white bags contain 4 clear purple bags, 5 muted turquoise bags, 4 clear lavender bags.
faded silver bags contain 1 striped white bag.
dim magenta bags contain 5 plaid salmon bags, 3 dark chartreuse bags.
dark beige bags contain 3 bright brown bags, 4 dull turquoise bags, 3 muted black bags.
wavy beige bags contain 5 faded olive bags.
dark turquoise bags contain 4 striped indigo bags, 5 pale blue bags.
pale tan bags contain 3 dim olive bags, 4 posh green bags.
muted beige bags contain 2 pale indigo bags, 1 bright black bag, 3 bright plum bags.
faded fuchsia bags contain 4 faded silver bags.
shiny olive bags contain no other bags.
dark lime bags contain 3 clear olive bags, 2 clear blue bags, 2 dull cyan bags.
dim tan bags contain 4 plaid chartreuse bags.
light bronze bags contain 5 bright aqua bags, 1 plaid magenta bag, 5 dim lime bags.
muted chartreuse bags contain 5 faded cyan bags, 4 posh fuchsia bags, 2 drab tomato bags.
bright blue bags contain 5 vibrant green bags, 5 dull white bags, 1 bright orange bag.
light beige bags contain 3 dim magenta bags, 5 plaid tomato bags, 5 wavy black bags.
dull maroon bags contain 1 dotted gold bag.
bright lavender bags contain 3 drab blue bags, 2 dotted yellow bags.
drab teal bags contain 5 shiny crimson bags.
faded tan bags contain 2 muted cyan bags.
wavy lime bags contain 4 mirrored silver bags, 3 dark purple bags.
dull turquoise bags contain 4 posh green bags, 4 shiny gold bags, 5 clear blue bags.
clear salmon bags contain 4 striped blue bags, 2 faded chartreuse bags, 1 muted tan bag, 4 dark coral bags.
wavy magenta bags contain 3 dim gold bags, 5 shiny white bags, 4 striped gold bags.
striped crimson bags contain 3 pale aqua bags, 3 pale indigo bags, 4 pale tan bags.
light white bags contain 4 clear fuchsia bags, 1 dark silver bag, 5 shiny olive bags.
bright magenta bags contain 4 pale beige bags.
posh tan bags contain 5 plaid salmon bags.
pale tomato bags contain 1 posh salmon bag.
bright purple bags contain 4 mirrored coral bags.
dark orange bags contain 5 drab olive bags, 4 faded yellow bags.
light olive bags contain 2 striped brown bags, 1 wavy tan bag.
clear red bags contain 4 light chartreuse bags.
wavy coral bags contain 3 muted purple bags, 1 bright tomato bag.
dull teal bags contain 3 dull tan bags, 5 faded silver bags, 3 striped orange bags.
dark crimson bags contain no other bags.
muted yellow bags contain 1 dull teal bag, 3 drab olive bags.
vibrant orange bags contain 2 striped beige bags.
faded gold bags contain 4 wavy magenta bags, 4 mirrored teal bags.
clear indigo bags contain 1 dark gray bag, 2 shiny magenta bags, 1 vibrant olive bag.
plaid olive bags contain 5 striped lavender bags, 5 faded orange bags.
dotted salmon bags contain 1 pale tan bag, 5 light magenta bags.
dull silver bags contain 4 mirrored tan bags, 3 light tan bags, 2 dark black bags.
plaid fuchsia bags contain 2 mirrored bronze bags.
dotted beige bags contain 2 faded orange bags, 3 clear orange bags.
dark fuchsia bags contain 2 clear turquoise bags, 1 vibrant indigo bag, 5 wavy blue bags, 3 dotted brown bags.
muted gray bags contain 4 faded magenta bags, 5 mirrored salmon bags, 5 vibrant purple bags.
muted aqua bags contain 1 vibrant turquoise bag.
wavy indigo bags contain 1 dotted salmon bag, 4 light violet bags.
plaid black bags contain 3 faded teal bags, 5 wavy teal bags.
muted plum bags contain 4 vibrant teal bags, 4 pale aqua bags, 2 light bronze bags, 4 pale silver bags.
striped salmon bags contain 3 shiny turquoise bags, 5 drab tan bags, 3 clear silver bags.
mirrored black bags contain 1 dotted chartreuse bag, 4 dull olive bags, 2 dim coral bags.
striped black bags contain 1 vibrant black bag, 3 faded coral bags, 3 wavy gray bags.
faded beige bags contain 3 dim olive bags, 5 vibrant bronze bags, 4 mirrored gold bags.
striped fuchsia bags contain 5 dotted yellow bags.
light indigo bags contain 2 pale gold bags, 3 light black bags.
mirrored silver bags contain 4 dark lime bags, 3 clear orange bags, 3 clear lime bags.
vibrant black bags contain 3 posh silver bags, 2 dull aqua bags, 2 dark black bags.
vibrant tomato bags contain 5 striped beige bags, 3 posh coral bags, 4 posh brown bags.
clear chartreuse bags contain 2 dull bronze bags, 2 drab turquoise bags.
dotted coral bags contain 1 dull brown bag, 5 clear teal bags, 3 mirrored olive bags.
drab beige bags contain 5 dotted olive bags, 5 dotted chartreuse bags, 4 dark coral bags, 3 wavy yellow bags.
wavy plum bags contain 1 dotted blue bag, 5 vibrant brown bags.
posh brown bags contain 2 dark plum bags, 3 shiny white bags, 2 striped plum bags, 3 dim lavender bags.
pale lime bags contain 5 posh orange bags, 2 posh magenta bags.
plaid tomato bags contain 4 shiny gray bags, 2 faded olive bags.
shiny purple bags contain 5 mirrored tan bags, 3 faded red bags, 1 clear coral bag, 4 posh purple bags.
vibrant red bags contain 3 wavy tomato bags, 5 mirrored magenta bags.
dim lavender bags contain 4 dim red bags.
dull beige bags contain 2 shiny gold bags.
vibrant cyan bags contain 4 shiny magenta bags.
striped chartreuse bags contain 2 dark plum bags.
pale magenta bags contain 2 drab tomato bags, 2 striped chartreuse bags, 4 clear crimson bags, 5 plaid green bags.
wavy teal bags contain 1 muted gold bag.
plaid tan bags contain 2 plaid turquoise bags, 5 pale aqua bags.
striped lime bags contain 1 light yellow bag, 1 drab coral bag, 1 posh blue bag.
light turquoise bags contain 2 clear brown bags, 1 pale fuchsia bag, 1 vibrant teal bag, 3 dark salmon bags.
dim indigo bags contain 4 dark red bags, 3 drab violet bags, 5 bright white bags.
faded crimson bags contain 4 dotted brown bags.
vibrant gold bags contain 2 pale turquoise bags, 2 vibrant tan bags.
drab turquoise bags contain 3 wavy teal bags.
dark silver bags contain 3 clear turquoise bags, 2 posh green bags, 3 dim coral bags, 1 clear coral bag.
mirrored bronze bags contain 3 light black bags, 3 dim olive bags, 5 posh purple bags, 2 dim lime bags.
dim gold bags contain 2 dotted lavender bags, 1 dotted olive bag.
dull aqua bags contain no other bags.
shiny red bags contain 5 vibrant magenta bags, 2 dull silver bags.
dull orange bags contain 2 drab orange bags.
striped violet bags contain 4 mirrored tan bags, 5 clear crimson bags, 3 dotted lavender bags, 5 light plum bags.
faded blue bags contain 4 striped blue bags, 1 light silver bag, 5 drab black bags.
dotted aqua bags contain 5 plaid fuchsia bags, 1 bright plum bag, 4 shiny red bags.
shiny lavender bags contain 5 wavy indigo bags, 5 wavy beige bags.
faded salmon bags contain 4 shiny red bags.
muted orange bags contain 4 dim silver bags, 3 clear turquoise bags, 4 pale red bags, 2 pale olive bags.
drab silver bags contain 1 vibrant yellow bag, 3 pale tomato bags, 1 dull silver bag.
dim white bags contain 5 drab orange bags, 3 dark fuchsia bags, 4 muted coral bags, 2 shiny aqua bags.
muted lavender bags contain 3 faded lime bags, 5 light plum bags.
pale blue bags contain 4 dotted chartreuse bags.
clear bronze bags contain 4 dull magenta bags, 3 mirrored fuchsia bags, 2 shiny lime bags, 4 plaid violet bags.
clear magenta bags contain 4 wavy blue bags, 1 dotted lavender bag, 1 shiny olive bag.
dotted gold bags contain 3 shiny coral bags, 5 bright tan bags.
vibrant violet bags contain 4 bright olive bags.
posh fuchsia bags contain 1 striped crimson bag, 5 mirrored gold bags, 3 muted beige bags.
faded white bags contain 5 plaid purple bags, 3 posh lime bags, 3 clear blue bags.
dim plum bags contain 1 posh purple bag, 2 pale indigo bags, 1 dark lavender bag.
dull lime bags contain 2 striped magenta bags, 5 drab tomato bags.
plaid orange bags contain 5 faded tan bags, 1 posh aqua bag, 2 faded red bags, 2 clear red bags.
muted white bags contain 2 striped white bags.
bright gold bags contain 3 wavy beige bags, 2 faded coral bags, 5 drab chartreuse bags, 1 vibrant brown bag.
pale maroon bags contain 2 plaid salmon bags, 1 muted turquoise bag.
posh cyan bags contain 1 dotted brown bag, 3 shiny magenta bags, 2 pale brown bags.
striped tomato bags contain 5 vibrant lime bags, 1 dark black bag.
dotted magenta bags contain 2 wavy turquoise bags, 2 striped teal bags, 5 dotted yellow bags, 2 drab brown bags.
wavy lavender bags contain 5 light turquoise bags.
clear plum bags contain 4 faded tomato bags, 3 clear yellow bags, 5 drab silver bags, 5 dim yellow bags.
muted silver bags contain 5 pale maroon bags, 2 posh silver bags, 2 dotted yellow bags, 1 muted indigo bag.
muted indigo bags contain 5 dull beige bags, 4 striped orange bags, 2 wavy fuchsia bags, 3 muted magenta bags.
light teal bags contain 1 clear magenta bag, 4 faded olive bags, 5 dotted purple bags, 4 clear coral bags.
posh purple bags contain 2 bright tomato bags, 5 dark gray bags, 5 clear coral bags, 5 clear turquoise bags.
muted lime bags contain 1 mirrored bronze bag.
pale coral bags contain 1 vibrant lime bag, 4 vibrant black bags, 2 vibrant magenta bags.
mirrored orange bags contain 1 dim orange bag, 4 faded purple bags, 5 dull bronze bags, 5 dull green bags.
vibrant bronze bags contain 1 muted gold bag.
plaid white bags contain 4 dull yellow bags.
dark white bags contain 2 drab magenta bags, 5 dull beige bags.
dull olive bags contain 2 muted teal bags, 4 posh purple bags, 2 muted lime bags.
dull fuchsia bags contain 1 wavy olive bag, 4 bright aqua bags, 5 vibrant purple bags, 3 shiny purple bags.
dim purple bags contain 5 light brown bags.
mirrored purple bags contain 5 clear coral bags.
dim bronze bags contain 3 dull teal bags, 3 posh blue bags, 1 dull purple bag, 4 striped chartreuse bags.
light brown bags contain 1 plaid olive bag, 1 vibrant tan bag, 2 dotted orange bags.
faded turquoise bags contain 2 dotted fuchsia bags, 5 muted orange bags.
striped gold bags contain 3 drab olive bags, 1 vibrant green bag, 3 shiny gold bags.
light black bags contain 3 shiny magenta bags, 4 posh green bags.
dull indigo bags contain 2 muted bronze bags, 5 plaid magenta bags.
light red bags contain 4 dim teal bags, 3 muted yellow bags, 1 dotted gray bag.
dark coral bags contain 3 dotted chartreuse bags, 3 bright orange bags, 4 shiny magenta bags.
dark cyan bags contain 3 striped magenta bags.
plaid bronze bags contain 3 shiny blue bags.
muted cyan bags contain 1 posh green bag.
wavy orange bags contain 1 posh green bag, 1 vibrant cyan bag, 1 dark maroon bag, 1 dull teal bag.
shiny salmon bags contain 1 dotted yellow bag, 1 striped olive bag, 4 dull teal bags, 3 drab magenta bags.
pale turquoise bags contain 4 bright crimson bags, 5 dotted fuchsia bags, 1 faded turquoise bag, 5 muted turquoise bags.
dotted silver bags contain 5 dark orange bags, 4 dotted olive bags, 4 pale bronze bags, 3 dull yellow bags.
dotted red bags contain 4 dull white bags, 2 shiny plum bags, 2 pale gold bags.
striped blue bags contain 4 shiny orange bags, 1 striped orange bag.
dotted tan bags contain 3 dark brown bags, 3 wavy yellow bags, 5 dotted fuchsia bags.
shiny green bags contain 2 dark olive bags, 2 clear indigo bags, 2 striped salmon bags.
wavy tan bags contain 4 mirrored purple bags, 3 mirrored blue bags, 3 clear turquoise bags, 4 vibrant purple bags.
dark brown bags contain 2 shiny olive bags, 1 dull aqua bag.
plaid cyan bags contain 3 dull aqua bags, 1 muted teal bag.
dark tomato bags contain 2 faded lime bags, 2 dark cyan bags, 4 vibrant gray bags.
dark gold bags contain 3 pale teal bags, 3 posh chartreuse bags, 3 pale tan bags, 2 drab magenta bags.
striped indigo bags contain 4 striped black bags, 3 vibrant black bags, 2 mirrored turquoise bags, 3 dark coral bags.
faded lavender bags contain 1 muted turquoise bag, 3 dull bronze bags, 3 bright red bags, 2 dull turquoise bags.
wavy black bags contain 3 dotted purple bags.
faded plum bags contain 4 plaid lavender bags, 5 dark lavender bags, 5 dark black bags, 3 striped crimson bags.
posh olive bags contain 2 striped beige bags, 1 plaid crimson bag.
dim green bags contain 5 faded brown bags.
mirrored magenta bags contain 5 clear yellow bags.
pale red bags contain 2 dotted purple bags, 5 dotted tan bags, 2 dark brown bags.
bright black bags contain 1 dark maroon bag.
dark gray bags contain no other bags.
mirrored plum bags contain 5 bright orange bags, 3 muted purple bags.
bright turquoise bags contain 5 vibrant yellow bags.
posh beige bags contain 4 dim green bags, 3 posh gold bags.
bright maroon bags contain 2 muted blue bags, 5 shiny plum bags.
bright brown bags contain 2 muted black bags.
shiny yellow bags contain 5 muted olive bags, 5 wavy tomato bags, 1 mirrored green bag.
dim tomato bags contain 5 drab chartreuse bags.
posh turquoise bags contain 4 drab green bags, 2 dim gold bags.
posh blue bags contain 3 shiny bronze bags, 3 dull white bags, 3 clear beige bags, 4 dull cyan bags.
wavy green bags contain 5 pale bronze bags, 3 dull maroon bags, 4 dark purple bags.
posh violet bags contain 4 plaid fuchsia bags, 1 clear salmon bag.
bright beige bags contain 1 faded maroon bag, 2 dim brown bags, 1 dull chartreuse bag.
dim violet bags contain 5 vibrant bronze bags.
mirrored gold bags contain 5 light violet bags, 2 mirrored tan bags.
mirrored lavender bags contain 5 dotted white bags, 3 wavy chartreuse bags, 2 shiny lavender bags.
pale silver bags contain 4 mirrored turquoise bags, 3 faded coral bags, 5 wavy yellow bags, 5 shiny gold bags.
shiny lime bags contain 1 plaid indigo bag.
dim cyan bags contain 4 dim teal bags.
wavy violet bags contain 2 plaid beige bags.
striped brown bags contain 4 plaid silver bags, 4 striped violet bags, 2 muted gold bags, 1 plaid yellow bag.
pale chartreuse bags contain 3 dark coral bags, 2 faded olive bags.
mirrored fuchsia bags contain 1 plaid lavender bag, 5 dark coral bags, 5 light green bags, 1 dotted bronze bag.
dotted cyan bags contain 5 posh gold bags, 5 vibrant red bags.
shiny gold bags contain 2 dark brown bags, 2 mirrored coral bags, 1 faded olive bag, 1 posh bronze bag.
striped coral bags contain 3 clear red bags, 2 mirrored crimson bags.
striped beige bags contain 5 plaid salmon bags, 4 dim lime bags, 3 dark brown bags.
plaid lavender bags contain 2 dotted olive bags, 4 faded olive bags, 4 shiny magenta bags.
muted crimson bags contain 3 shiny purple bags.
mirrored crimson bags contain 2 muted teal bags, 3 dotted purple bags, 5 pale teal bags.
posh salmon bags contain 5 vibrant turquoise bags, 3 muted black bags, 3 pale indigo bags, 2 shiny plum bags.
vibrant coral bags contain 2 drab magenta bags, 2 faded red bags, 4 plaid magenta bags, 4 muted cyan bags.
shiny crimson bags contain 1 dotted beige bag, 2 mirrored turquoise bags, 5 clear gray bags, 1 dim lavender bag.
posh bronze bags contain 5 clear coral bags, 1 dotted lime bag, 5 faded olive bags, 3 dim red bags.
faded purple bags contain 1 plaid white bag, 4 posh lime bags, 5 pale turquoise bags.
posh magenta bags contain 2 dull turquoise bags, 2 dark black bags, 1 bright red bag.
faded brown bags contain 5 mirrored bronze bags, 1 mirrored coral bag, 5 muted white bags, 5 dim orange bags.
faded violet bags contain 4 pale white bags, 1 bright tomato bag, 4 posh gray bags.
mirrored teal bags contain 1 pale crimson bag, 4 striped lime bags, 5 plaid purple bags.
drab crimson bags contain 5 mirrored lime bags.
posh maroon bags contain 5 faded olive bags, 4 bright black bags, 5 muted tomato bags.
muted magenta bags contain 5 shiny olive bags, 4 posh purple bags.
muted olive bags contain 3 dull plum bags, 4 clear turquoise bags, 4 clear purple bags.
dark tan bags contain 4 vibrant teal bags, 3 dotted chartreuse bags, 5 clear crimson bags.
dotted violet bags contain 4 dull cyan bags, 2 wavy blue bags, 1 posh yellow bag.
mirrored turquoise bags contain 4 pale crimson bags, 2 faded lime bags.
muted teal bags contain 5 vibrant indigo bags, 2 bright tomato bags, 5 drab magenta bags, 2 dim red bags.
plaid plum bags contain 1 mirrored bronze bag, 5 dim orange bags, 2 dim beige bags.
posh gray bags contain 1 faded coral bag, 2 dark crimson bags.
dotted tomato bags contain 1 plaid silver bag, 4 clear gold bags, 1 faded violet bag.
shiny teal bags contain 2 muted plum bags, 3 mirrored olive bags.
posh orange bags contain 1 faded aqua bag, 1 wavy indigo bag, 2 muted magenta bags, 2 faded beige bags.
light tan bags contain 1 faded lime bag.
pale cyan bags contain 4 drab black bags, 2 wavy black bags.
dark olive bags contain 4 dark gray bags, 5 drab olive bags, 2 light coral bags.
drab olive bags contain 1 dotted fuchsia bag, 4 dull tan bags, 3 plaid turquoise bags.
vibrant brown bags contain 3 striped bronze bags, 1 dull yellow bag, 5 wavy yellow bags, 4 drab cyan bags.
wavy gray bags contain 1 faded lime bag, 3 shiny purple bags, 3 vibrant cyan bags, 2 dotted fuchsia bags.
pale gray bags contain 2 striped orange bags, 1 plaid green bag, 2 faded orange bags, 3 mirrored blue bags.
posh crimson bags contain 3 clear orange bags, 5 faded indigo bags.
vibrant teal bags contain 5 dark black bags, 4 dim silver bags.
faded bronze bags contain 2 posh bronze bags, 4 bright tomato bags.
plaid violet bags contain 3 plaid indigo bags, 5 dull black bags, 1 striped indigo bag, 2 drab blue bags.
muted black bags contain 1 pale indigo bag.
bright green bags contain 5 vibrant orange bags, 3 muted orange bags, 1 pale violet bag, 1 muted olive bag.
wavy chartreuse bags contain 1 dark fuchsia bag, 1 vibrant teal bag, 1 wavy purple bag.
dotted gray bags contain 3 plaid chartreuse bags, 2 faded violet bags.
dull violet bags contain 4 wavy gold bags.
bright tomato bags contain no other bags.
plaid purple bags contain 2 drab lavender bags, 3 dim red bags, 5 dark chartreuse bags, 5 dotted brown bags.
vibrant chartreuse bags contain 5 striped orange bags, 4 faded blue bags, 1 plaid salmon bag.
light silver bags contain 5 posh purple bags, 5 light tan bags, 4 pale fuchsia bags.
dim salmon bags contain 4 shiny lime bags, 3 light tan bags, 5 dim orange bags, 5 mirrored coral bags.
plaid gray bags contain 2 bright coral bags, 2 plaid tan bags, 3 dark crimson bags, 4 dotted lavender bags.
dull gold bags contain 2 mirrored indigo bags.
light blue bags contain 2 striped gray bags, 1 wavy tan bag, 4 clear crimson bags.
wavy brown bags contain 3 light coral bags, 4 dotted maroon bags, 5 dim silver bags, 2 plaid black bags.
drab black bags contain 4 dull yellow bags.
shiny plum bags contain 5 drab olive bags, 4 dark tan bags.
dim red bags contain 2 light violet bags, 4 clear coral bags.
vibrant salmon bags contain 5 striped violet bags, 2 pale silver bags, 5 clear orange bags.
dull crimson bags contain 1 vibrant blue bag, 4 posh red bags, 5 muted bronze bags, 4 dim olive bags.
vibrant purple bags contain 3 muted turquoise bags, 5 striped gold bags.
vibrant plum bags contain 1 dull teal bag, 5 posh purple bags.
shiny silver bags contain 1 clear plum bag.
plaid beige bags contain 1 shiny magenta bag, 5 posh gray bags.
dull tan bags contain 1 posh purple bag.
mirrored coral bags contain 3 pale aqua bags.
dim brown bags contain 2 dark turquoise bags, 4 dark orange bags, 3 posh olive bags, 2 light chartreuse bags.
muted gold bags contain no other bags.
drab lavender bags contain 5 shiny purple bags, 5 dotted bronze bags.
muted red bags contain 3 drab lavender bags, 1 dim tomato bag, 3 light olive bags, 2 dull aqua bags.
dotted lime bags contain 4 drab magenta bags, 1 dark black bag, 2 posh purple bags.
plaid turquoise bags contain no other bags.
clear orange bags contain 5 shiny magenta bags, 4 light indigo bags, 1 muted yellow bag.
dim gray bags contain 4 posh fuchsia bags, 5 clear gray bags, 2 clear beige bags.
muted tan bags contain 5 vibrant turquoise bags, 2 mirrored gold bags.
shiny white bags contain 5 dim indigo bags, 4 shiny blue bags, 5 clear lavender bags, 3 dim teal bags.
pale lavender bags contain 1 faded green bag, 3 light plum bags, 1 dim bronze bag.
bright plum bags contain 2 mirrored turquoise bags, 2 shiny orange bags, 5 bright tomato bags, 1 bright aqua bag.
vibrant tan bags contain 5 dark lavender bags.
wavy olive bags contain 4 dull salmon bags, 5 mirrored purple bags.
vibrant yellow bags contain 2 striped white bags, 3 pale gold bags, 4 muted gold bags, 2 dull cyan bags.
clear maroon bags contain 4 faded turquoise bags, 3 bright blue bags, 4 dark brown bags, 2 shiny indigo bags.
muted tomato bags contain 4 dim aqua bags, 5 light black bags.
striped lavender bags contain 4 striped gold bags, 4 dark turquoise bags.
dull chartreuse bags contain 1 dotted purple bag, 2 wavy indigo bags, 2 posh blue bags.
dark blue bags contain 2 drab orange bags.
clear coral bags contain 1 muted gold bag, 5 drab magenta bags, 5 bright tomato bags.
clear black bags contain 1 faded coral bag.
dotted blue bags contain 3 shiny gold bags, 3 mirrored purple bags, 5 pale maroon bags.
muted turquoise bags contain 3 posh green bags.
dotted indigo bags contain 1 mirrored tan bag.
dotted olive bags contain 2 vibrant indigo bags, 4 dull purple bags, 4 dull salmon bags.
dull yellow bags contain 1 dim red bag, 3 pale fuchsia bags, 5 shiny gold bags, 4 dull aqua bags.
wavy white bags contain 2 pale salmon bags, 2 striped beige bags, 1 pale aqua bag.
clear beige bags contain 3 dull yellow bags, 2 shiny red bags.
light gold bags contain 2 striped magenta bags, 2 pale salmon bags.
dotted brown bags contain 1 drab blue bag.
posh gold bags contain 1 wavy purple bag, 1 clear magenta bag, 3 clear turquoise bags.
bright yellow bags contain 2 dull maroon bags, 2 muted olive bags, 5 wavy teal bags.
striped plum bags contain 2 striped magenta bags.
striped turquoise bags contain 4 pale magenta bags, 3 muted green bags, 4 pale blue bags.
dull cyan bags contain 4 plaid cyan bags, 1 vibrant cyan bag, 5 shiny violet bags, 3 drab lavender bags.
vibrant silver bags contain 3 vibrant gray bags, 3 light tomato bags.
light lime bags contain 5 plaid crimson bags.
posh yellow bags contain 1 mirrored blue bag, 1 bright orange bag, 2 drab lavender bags.
dull blue bags contain 4 pale chartreuse bags, 2 light maroon bags.
faded black bags contain 4 mirrored brown bags.
bright bronze bags contain 5 dim silver bags.
drab salmon bags contain 1 plaid purple bag, 4 vibrant crimson bags.
drab maroon bags contain 5 shiny gray bags, 4 dotted tan bags.
plaid red bags contain 5 dotted indigo bags.
posh chartreuse bags contain 4 posh silver bags, 4 pale white bags, 3 light chartreuse bags, 1 light black bag.
wavy yellow bags contain 5 plaid turquoise bags, 1 dark gray bag.
vibrant beige bags contain 4 pale gray bags, 4 light blue bags.
light yellow bags contain 3 dim fuchsia bags, 2 clear violet bags, 4 clear teal bags.
dim black bags contain 1 wavy beige bag, 1 dark lavender bag.
shiny bronze bags contain 1 faded lime bag, 5 vibrant green bags.
striped bronze bags contain 1 vibrant cyan bag, 1 faded tomato bag, 5 faded silver bags, 5 dim violet bags.
dim olive bags contain 5 clear fuchsia bags, 4 dark maroon bags.
muted blue bags contain 1 plaid salmon bag, 3 posh purple bags, 5 drab orange bags.
pale fuchsia bags contain 3 dull teal bags.
drab tan bags contain 4 dull fuchsia bags, 2 dark violet bags.
striped white bags contain no other bags.
bright gray bags contain 4 dark bronze bags, 2 vibrant lavender bags, 5 faded cyan bags.
pale aqua bags contain 1 dim red bag.
posh tomato bags contain 3 bright orange bags, 1 dull bronze bag.
mirrored yellow bags contain 5 posh brown bags, 2 plaid brown bags, 2 muted gold bags.
drab cyan bags contain 2 dull silver bags, 1 light silver bag, 5 vibrant gray bags, 5 mirrored crimson bags.
wavy gold bags contain 2 muted plum bags.
plaid teal bags contain 4 shiny black bags, 4 bright olive bags, 3 mirrored maroon bags.
posh black bags contain 4 faded orange bags.
striped silver bags contain 5 muted cyan bags.
bright orange bags contain 1 dull tan bag, 5 dull aqua bags, 5 plaid turquoise bags, 1 bright tomato bag.
clear lime bags contain 1 pale violet bag.
vibrant gray bags contain 5 bright black bags, 1 vibrant purple bag, 5 mirrored coral bags, 5 plaid magenta bags.
mirrored salmon bags contain 2 faded coral bags, 4 striped silver bags.
dim coral bags contain 4 striped white bags, 1 light violet bag, 1 dark brown bag.
dark indigo bags contain 5 vibrant cyan bags.
dotted bronze bags contain 1 drab olive bag, 4 bright orange bags, 1 striped yellow bag.
dotted plum bags contain 3 wavy beige bags, 2 striped magenta bags.
mirrored aqua bags contain 2 wavy purple bags, 5 dull brown bags, 1 clear maroon bag, 4 posh orange bags.
dark green bags contain 2 pale yellow bags, 4 vibrant cyan bags, 4 dim turquoise bags.
dotted chartreuse bags contain 2 drab magenta bags, 1 clear coral bag, 5 striped white bags.
pale brown bags contain 5 faded olive bags, 4 light turquoise bags, 2 bright blue bags.
drab gold bags contain 4 wavy chartreuse bags, 2 plaid aqua bags, 4 wavy orange bags.
plaid blue bags contain 4 faded fuchsia bags, 3 pale bronze bags, 5 faded violet bags.
bright lime bags contain 2 muted turquoise bags.
clear cyan bags contain 1 muted crimson bag, 5 dotted gold bags, 5 dotted crimson bags.
bright indigo bags contain 4 muted turquoise bags, 2 shiny black bags.
posh lavender bags contain 1 faded tan bag, 4 dark coral bags, 2 muted plum bags.
striped maroon bags contain 4 dull magenta bags, 5 pale tan bags, 2 dark teal bags, 3 dotted gray bags.
light aqua bags contain 1 faded lime bag.
shiny coral bags contain 2 dim orange bags, 4 striped beige bags.
vibrant green bags contain 3 shiny magenta bags, 4 drab magenta bags, 4 posh silver bags, 2 shiny orange bags.
plaid maroon bags contain 3 dark coral bags, 5 dim green bags, 4 dim lime bags.
dull red bags contain 4 shiny fuchsia bags.
light crimson bags contain 2 dull tomato bags.
pale beige bags contain 4 muted gold bags.
dotted lavender bags contain 1 muted gold bag, 1 dim violet bag.
dim orange bags contain 2 faded chartreuse bags.
clear white bags contain 3 posh teal bags.
muted brown bags contain 5 light cyan bags, 4 drab purple bags, 2 light black bags, 1 dark indigo bag.
striped aqua bags contain 2 faded aqua bags.
dotted turquoise bags contain 2 shiny white bags, 1 striped lavender bag.
dim maroon bags contain 3 drab tan bags, 4 muted tomato bags, 2 striped lavender bags, 3 plaid silver bags.
drab tomato bags contain 1 wavy gray bag, 2 vibrant teal bags.
striped green bags contain 4 vibrant tomato bags, 3 dull brown bags, 4 light tomato bags.
dull magenta bags contain 2 plaid chartreuse bags, 2 light indigo bags.
vibrant lavender bags contain 1 dull purple bag, 2 shiny crimson bags.
faded olive bags contain 1 dark gray bag.
bright aqua bags contain 2 pale crimson bags.
wavy aqua bags contain 3 light gray bags, 1 posh red bag.
shiny turquoise bags contain 4 dotted black bags, 4 drab gray bags, 1 muted magenta bag.
muted green bags contain 5 striped white bags, 3 vibrant turquoise bags, 1 dull turquoise bag.
faded yellow bags contain 2 dim red bags.
clear fuchsia bags contain 1 dark salmon bag, 2 dim lime bags, 5 drab magenta bags, 2 faded orange bags.
bright coral bags contain 4 wavy gray bags, 1 dull tan bag.
muted bronze bags contain 3 drab turquoise bags, 5 clear aqua bags, 4 dull aqua bags, 2 dim violet bags.
clear violet bags contain 1 plaid salmon bag, 5 dark lavender bags.
dark teal bags contain 2 clear teal bags, 3 dull chartreuse bags, 3 wavy purple bags.
posh green bags contain 5 dark crimson bags, 2 dotted chartreuse bags.
faded magenta bags contain 1 striped silver bag, 5 light beige bags, 1 posh olive bag, 5 vibrant coral bags.
clear crimson bags contain 3 drab gray bags.
pale indigo bags contain 1 faded red bag.
dull brown bags contain 1 clear purple bag.
clear brown bags contain 2 mirrored coral bags, 5 pale white bags, 1 dotted bronze bag, 5 wavy orange bags.
drab aqua bags contain 1 vibrant chartreuse bag, 4 drab tomato bags, 2 shiny gold bags.
shiny black bags contain 2 dull lime bags.
dotted purple bags contain 3 faded orange bags, 1 dotted chartreuse bag, 4 pale indigo bags.
posh lime bags contain 5 dotted tan bags, 1 shiny magenta bag.
drab plum bags contain 3 plaid silver bags, 2 dark aqua bags, 2 bright crimson bags, 1 posh purple bag.
striped gray bags contain 1 posh purple bag.
light magenta bags contain 5 dark brown bags.
striped magenta bags contain 5 faded orange bags, 2 faded red bags, 2 posh silver bags, 3 dark gray bags.
dark bronze bags contain 1 faded coral bag, 1 shiny plum bag, 5 shiny white bags.
bright teal bags contain 4 dark tan bags.
mirrored tomato bags contain 1 muted tomato bag, 2 dull yellow bags, 2 posh white bags.
bright white bags contain 5 faded silver bags, 5 dark chartreuse bags.
wavy turquoise bags contain 3 drab fuchsia bags, 3 dull violet bags, 4 dark tomato bags.
posh coral bags contain 5 shiny gold bags, 3 vibrant olive bags, 3 dotted brown bags, 2 dim chartreuse bags.
dotted fuchsia bags contain 5 dim coral bags, 5 drab gray bags, 4 striped white bags, 4 bright orange bags.
vibrant magenta bags contain 3 plaid salmon bags, 1 dotted lime bag, 1 clear crimson bag, 2 dotted tan bags.
faded cyan bags contain 3 faded violet bags, 2 light green bags.
dark red bags contain 1 dim violet bag, 4 vibrant olive bags, 5 drab olive bags, 5 faded yellow bags.
faded aqua bags contain 5 bright plum bags, 3 light black bags, 1 muted yellow bag, 3 mirrored bronze bags.
wavy blue bags contain 5 wavy teal bags, 5 shiny magenta bags, 5 drab olive bags, 5 dark coral bags.
striped orange bags contain 4 striped white bags, 3 dim red bags, 3 clear coral bags, 4 shiny gold bags.
pale orange bags contain 3 dim green bags, 3 dark olive bags.
drab violet bags contain 3 striped gold bags, 5 plaid magenta bags.
dark salmon bags contain 5 clear coral bags, 2 muted gold bags, 5 bright tomato bags.
mirrored lime bags contain 2 plaid maroon bags, 1 shiny olive bag, 2 vibrant coral bags.
dull black bags contain 3 vibrant brown bags, 4 clear black bags.
clear gray bags contain 5 drab cyan bags, 3 dotted salmon bags.
light maroon bags contain 5 plaid fuchsia bags, 4 muted purple bags.
drab white bags contain 5 shiny gray bags, 5 posh orange bags.
dim chartreuse bags contain 3 wavy blue bags, 1 bright aqua bag.
dotted black bags contain 5 pale bronze bags.
striped purple bags contain 3 vibrant black bags.
shiny tomato bags contain 2 faded blue bags.
drab coral bags contain 2 pale beige bags.
striped cyan bags contain 2 dark lime bags, 5 light plum bags.
dotted yellow bags contain 4 dotted bronze bags.
dark maroon bags contain 1 dark silver bag, 1 dark black bag, 2 striped silver bags, 5 striped white bags.
drab chartreuse bags contain 4 muted maroon bags.
plaid lime bags contain 2 dim teal bags, 2 posh chartreuse bags.
dim fuchsia bags contain 4 mirrored turquoise bags, 4 light gray bags, 5 mirrored black bags, 3 faded yellow bags.
bright cyan bags contain 4 plaid crimson bags, 3 dim plum bags, 4 mirrored olive bags, 4 muted plum bags.
striped olive bags contain 3 shiny purple bags.
dull bronze bags contain 2 drab tomato bags, 3 dull teal bags, 4 dim silver bags, 4 faded turquoise bags.
wavy purple bags contain 4 wavy black bags, 4 clear purple bags, 2 dark fuchsia bags.
wavy silver bags contain 2 plaid tomato bags.
plaid magenta bags contain 3 faded red bags, 2 vibrant black bags.
dark chartreuse bags contain 1 dim red bag, 4 vibrant olive bags, 4 dark black bags.
striped teal bags contain 1 mirrored fuchsia bag, 1 wavy black bag, 5 dim yellow bags, 1 dotted orange bag.
dull green bags contain 3 posh tomato bags, 4 bright plum bags.
drab lime bags contain 2 shiny blue bags, 5 light bronze bags, 4 dim magenta bags, 3 mirrored crimson bags.
faded green bags contain 1 posh fuchsia bag, 5 dim plum bags.
light violet bags contain 2 dark crimson bags, 3 dark gray bags, 5 muted gold bags.
bright fuchsia bags contain 4 dull tomato bags, 4 wavy fuchsia bags, 4 clear salmon bags.
drab green bags contain 4 vibrant black bags, 5 pale salmon bags, 4 pale olive bags, 5 clear crimson bags.
muted salmon bags contain 2 clear magenta bags, 3 wavy crimson bags.
shiny maroon bags contain 2 dim crimson bags.";
        #endregion

        #region Day 8
        public static string Day8 = @"acc -5
nop +333
acc +45
jmp +288
acc -9
jmp +1
acc +27
jmp +464
acc +34
jmp +478
jmp +356
acc +10
acc +20
acc +29
acc -10
jmp +359
acc +29
acc +31
acc +36
acc +42
jmp +502
acc +14
jmp +45
jmp +499
acc -19
acc +4
acc +24
nop +460
jmp +465
acc +29
acc +6
acc +25
jmp +355
acc -10
acc +50
jmp -27
acc +46
acc +2
acc -18
acc +8
jmp +85
nop +264
acc +44
jmp +310
acc +23
acc -15
acc -12
jmp +290
acc -5
acc +4
acc -7
jmp +248
acc +23
jmp +434
nop -6
jmp +239
jmp +1
acc -19
jmp +67
acc +40
acc +21
acc +24
jmp +366
acc +38
acc +15
acc -2
jmp +542
acc +27
acc +21
acc +44
acc +31
jmp -60
jmp -51
acc +14
jmp +254
acc +43
acc -3
acc +30
jmp -5
acc +12
jmp +330
acc +4
jmp +81
nop +107
acc -12
nop +98
jmp +467
jmp +111
acc +0
acc +48
acc -4
jmp +184
jmp +310
acc +5
acc +1
acc +49
jmp +477
jmp +279
acc +12
acc -7
nop +51
jmp +125
jmp +1
acc -6
acc -19
acc -10
jmp +109
acc +28
acc +4
jmp +422
acc +12
jmp +152
jmp -71
acc -8
nop +252
nop +303
jmp -4
acc +1
jmp +200
acc -2
jmp +453
jmp +443
acc +5
acc -18
jmp +304
jmp +414
acc +15
jmp +271
acc +22
jmp +371
acc +29
acc +29
acc -17
jmp +166
jmp +49
acc -4
acc -6
jmp +461
acc +21
acc +49
jmp +458
acc +27
acc +3
acc -12
jmp +7
jmp +216
nop +385
acc +0
acc +11
acc +13
jmp +343
nop +273
acc +38
acc -2
jmp +61
nop +8
jmp +135
acc +13
acc +46
jmp +239
acc +38
acc +6
jmp +225
nop +337
jmp +66
acc +49
acc +10
jmp +167
acc -18
acc +32
jmp +107
nop +195
acc +39
jmp +391
acc +13
jmp +227
jmp +71
nop +340
acc +30
jmp +19
acc +42
acc +34
jmp +349
acc +46
jmp -130
nop +383
acc +45
acc -17
acc +13
jmp +354
acc +39
acc +26
nop +55
jmp -100
acc -16
acc +13
acc -1
jmp +395
acc +33
nop +106
acc -14
acc -7
jmp -74
acc +0
acc -8
jmp -28
nop +265
acc +27
acc +30
acc +23
jmp -112
acc +22
acc +7
acc +2
jmp +71
acc -6
acc +15
nop -89
acc +24
jmp +92
jmp +353
jmp -104
acc +19
acc +12
acc +12
jmp -132
acc +20
acc +27
jmp -60
jmp -170
acc +13
acc +15
jmp +114
acc +3
acc +13
acc -16
acc +50
jmp +124
acc +28
acc -10
acc +0
acc +21
jmp +192
acc +2
acc +17
acc +18
jmp +318
acc +41
acc +34
acc +0
acc -5
jmp +17
nop -131
acc +29
acc +46
nop +238
jmp +172
jmp +1
acc +14
acc +32
acc -15
jmp +331
jmp +209
jmp +189
acc +1
nop +163
acc +46
jmp -77
acc +0
jmp -131
acc +21
acc +8
acc +26
acc +12
jmp +72
jmp +258
jmp +183
acc +17
acc -12
acc +15
jmp +5
nop +85
acc +23
acc +40
jmp +53
acc +24
jmp +257
acc -10
acc +34
acc +49
jmp -178
acc +28
jmp +164
jmp +250
acc -14
acc +32
acc +13
jmp +96
jmp -290
acc -19
jmp -276
acc -15
nop +271
acc -16
jmp +264
acc +36
jmp -172
acc -11
acc +33
nop -261
jmp +77
nop +70
acc +6
nop -98
acc +8
jmp -158
acc +37
jmp +17
acc -2
acc -13
jmp -52
acc -9
jmp -116
acc +18
acc +9
acc -12
jmp +8
jmp +226
jmp +284
acc -2
nop +44
nop -320
nop -259
jmp -312
jmp -286
acc +18
jmp +224
acc +46
acc +29
jmp +1
acc +18
jmp +123
acc +31
nop -209
nop -39
jmp -171
acc -12
jmp -53
acc +19
acc +42
nop -317
acc -6
jmp -122
jmp -90
jmp +1
jmp -248
acc +0
jmp -34
acc +33
acc -8
jmp -312
acc +47
acc -10
acc -4
jmp -281
jmp +106
acc -17
acc +0
acc +17
nop +229
jmp +54
acc +31
acc +23
jmp -88
jmp -301
jmp +105
jmp -264
acc -9
acc -17
acc +25
jmp +120
jmp -274
jmp +140
nop +35
jmp -146
acc +31
jmp -63
acc +14
acc +45
acc +48
acc +7
jmp -246
jmp +108
acc +18
acc -8
jmp -12
acc +6
jmp +163
nop -80
nop -340
jmp -63
jmp -126
acc +26
acc +6
acc +5
acc -2
jmp +111
acc +47
acc +20
jmp +39
acc +38
acc +3
acc -13
acc +27
jmp -263
nop +9
acc +50
jmp -149
acc -18
jmp -245
acc +2
acc +40
acc -4
jmp -302
acc +10
acc +20
jmp -220
jmp -93
nop +43
acc -18
acc +6
jmp +1
jmp -307
jmp +75
jmp -177
acc +8
acc +31
acc +47
jmp -321
acc +22
jmp +1
acc +9
acc +32
jmp -56
acc -13
nop -140
acc +24
jmp -368
jmp -285
acc +38
acc +32
jmp +1
jmp -205
acc +47
acc +21
jmp -304
jmp -17
acc +9
jmp -399
nop -233
acc +18
jmp -63
acc +45
jmp -335
acc +35
acc +9
acc -12
jmp -19
acc +8
acc +48
jmp -179
acc +37
acc +15
jmp -182
acc +2
acc +22
acc +7
jmp -271
jmp -288
jmp -345
acc +21
nop -107
acc +17
jmp -462
acc +41
jmp +1
jmp -158
nop -310
acc +38
nop +28
acc +24
jmp -32
jmp -375
acc +20
acc +15
acc +11
acc -3
jmp -186
acc -15
jmp -40
acc +38
acc +27
acc +50
acc +8
jmp -406
acc +15
acc +39
jmp -409
nop -396
acc -14
acc -5
jmp -40
nop -156
acc +3
acc -3
acc +22
jmp -16
acc +9
jmp +68
nop -109
acc +18
jmp -198
nop -455
nop -195
jmp +1
nop -3
jmp -46
acc +40
acc +26
acc +47
jmp -509
jmp -92
jmp -166
nop -335
acc +6
acc +1
acc +28
jmp +44
jmp -79
acc -18
acc +13
jmp -10
jmp +66
acc +29
acc +34
jmp +1
acc +44
jmp -129
acc -5
acc +41
acc +48
acc +28
jmp +16
acc -1
acc +30
acc -4
jmp +52
acc +1
acc +37
jmp -312
acc +14
nop -340
jmp -341
jmp -55
nop -366
acc +14
jmp -185
jmp -450
acc -4
acc -4
acc +37
jmp -93
jmp -170
jmp +1
acc -13
acc +47
acc +29
jmp -456
acc -12
acc -9
jmp -397
acc -11
acc +6
jmp -207
acc +18
jmp -387
nop -268
acc +40
acc +26
jmp -21
acc +47
jmp -91
acc -15
jmp -227
nop -466
acc +4
acc -19
jmp -231
acc +29
acc +15
acc +0
acc +35
jmp -303
acc +28
acc +36
acc +34
acc -11
jmp -168
acc +48
jmp -521
acc +28
jmp -25
acc +47
acc +16
acc -13
acc +11
jmp +1";
        #endregion

        #region Day 9
        public static string Day9 = @"37
1
33
42
17
34
27
44
26
39
3
43
30
22
9
38
7
28
21
4
50
14
35
12
5
6
71
8
15
10
11
13
16
53
17
20
18
19
23
24
9
22
25
26
21
27
28
14
67
29
30
31
33
59
32
34
35
37
40
42
54
41
39
36
23
51
61
58
43
44
102
47
46
52
53
78
55
56
65
57
101
66
114
59
80
64
62
67
69
79
87
135
133
89
90
122
108
223
113
126
115
124
116
119
151
125
121
123
128
191
129
197
348
156
234
244
179
202
198
246
224
221
228
240
231
478
235
331
242
248
285
249
251
257
308
377
463
354
335
582
381
400
419
433
445
449
456
483
477
473
559
484
490
491
536
724
611
508
565
662
831
689
716
818
864
845
819
852
878
894
905
1301
950
957
963
1197
974
1170
1417
1762
2607
1073
1224
1227
1757
1520
1405
1580
1924
1742
2367
3262
1967
3100
1799
1862
1907
1931
4109
1937
2198
2201
2243
2478
2297
3148
2997
5515
2985
3204
3319
5186
3322
3736
3706
3661
5282
3730
4340
4679
5517
5240
3868
4135
7716
6378
4444
7812
7454
6352
5982
7072
13054
6853
6940
7442
7436
6983
10808
7367
8970
7865
11427
16042
10661
14307
10117
11584
23862
12309
10426
10796
12334
12835
15910
12922
14848
13793
13836
14376
14350
26336
22713
15232
16337
16835
38670
26170
20543
20778
20913
64840
25420
29198
22735
26028
23130
41402
25757
26715
26758
27629
28169
28186
28726
29582
31569
32067
42255
33172
49845
50125
41321
41691
43648
93773
45865
48763
48492
49450
75207
89513
52515
54387
76165
54927
57751
56355
56912
58308
61151
63636
65239
87186
74493
109914
115220
95708
85339
110742
110601
109314
97255
97942
101965
106902
108870
107442
207169
205622
111282
113267
114663
150822
213247
124787
197673
139732
182594
364069
181047
183281
301330
187304
195197
402366
204844
199220
199907
208867
214344
296174
218724
224549
239450
225945
319507
254395
334103
264519
327036
365677
364328
395104
564235
448317
370585
382501
386524
590273
423568
946736
414251
408774
423211
589309
489068
443273
544056
465395
480340
518914
661139
651043
591555
778579
730005
734913
753086
757109
847896
1021628
1538291
1149164
889114
831985
1342395
1219073
852047
954463
987329
1941792
908668
945735
984309
999254
1892977
1242598
1326468
3834769
1492022
2194442
1487999
1910742
1589094
1679881
1684032
1721099
1740653
2476331
2536079
1797782
1806510
2762789
1907922
1854403
1930044
2325722
1983563
2226907
2982817
2569066
3150520
2814467
3167880
3477663
3077093
3285781
3363913
3424685
3400980
3405131
3461752
3538435
3604292
3837966
3652185
3714432
4081310
3762325
3784447
5407707
5632038
4552629
4795973
5383533
5646159
5891560
5982347
6244973
6915059
8267061
7148360
7318724
6806111
6862732
6866883
7142727
7190620
7366617
7414510
7436632
10647179
7546772
10428011
8337076
12836779
9348602
13107705
12531893
11275093
12754292
11873907
20474322
14242743
13668843
18423453
13729615
13672994
13948838
14009610
14057503
14851142
14557237
18013796
14961282
14983404
18821865
15883848
17685678
19612169
28519985
20623695
23149000
23806986
25284703
25822745
28286852
27341837
27398458
27402609
29040907
27621832
27682604
28006341
28067113
28614740
29408379
29518519
45972530
29944686
30867252
38309373
43566452
37297847
40235864
43772695
44430681
46955986
49091689
52687312
54109597
54740295
54801067
65912587
65304188
57140351
75607220
56297344
56073454
96779839
58023119
50047984
74639947
60811938
67242533
69176625
107488379
77533711
81728528
84008559
88203376
91386667
99139673
104788279
108071103
109541362
131780298
104849051
112370798
121377642
106121438
117109282
106345328
173182736
125265652
110859922
149187657
128054471
160563292
151251092
146710336
169931904
159262239
165737087
188857610
203988724
190526340
208681035
209637330
210970489
214390413
285553534
215708973
362040346
212466766
216981360
217205250
231610980
387573149
236125574
351089632
274764807
279305563
297961428
305972575
471709662
324999326
348119849
354594697
379383950
407507700
595092923
499943947
579021706
510428194
426857179
428175739
429448126
444077746
429672016
434186610
665797590
946182555
732148038
510890381
879327897
769671090
604304889
1390260301
630971901
679594023
673119175
788781307
733978647
807559689
856529195
938603933
855032918
954505940
939876320
857847755
857623865
863858626
873749762
1523645345
1400642991
1893109873
1115195270
1465396321
1461928754
1634099963
1235276790
1277424064
1304091076
1310565924
1714153060
1664088884
1522759954
1718891544
2381493100
3024718984
2161938831
1712656783
1715471620
1737608388
4259995774
1721482491
1979053896
1988945032
2350472060
3064731875
2512700854
4329525956
2927325075
4176789738
3428128403
4095646160
4375297799
2614657000
2833325878
3186848838
4231592398
3260368342
4981850833
5346026732
4822270910
4336139491
4642796695
4902320458
3459090879
3700536387
7064918276
3967998928
5773069196
5183797938
5127357854
5440025929
8409979236
6846249398
8361411337
6093694220
6073747879
5447982878
7154847766
6020174716
6447217180
6719459221
6960904729
7668535315
13034652608
7159627266
7427089807
8101887574
8642888817
8586448733
9415981806
9095356782
12619318594
13801437266
12400930658
10567383783
10888008807
14115752495
11468157594
13520784027
11521730757
12602830644
11895200058
12467391896
12739633937
21043819475
13680363950
14586717073
15095625122
15261514840
15528977381
16013538540
25828898623
26108447830
17681805515
18511338588
19983365589
21455392590
37665171104
22035541377
22089114540
22356166401
28616369184
22989888351
31878565647
23416930815
24362591954
27326351010
25207025833
26419997887
28941878790
43718364421
29682342195
39458217076
36193144103
37618091921
33695344055
54868453998
59073484511
39966731178
62875147891
41438758179
43490933967
44124655917
44391707778
45079002891
45346054752
66784568086
67541586732
71121100374
47779522769
49569617787
51627023720
54148904623
83972666872
63377686250
96706026611
67300434116
69888488158
108643102298
106366081858
155779511122
115245756291
95751679637
81405489357
93065781899
84929692146
87615589884
159370412208
134499309933
99494959375
137188922274
97349140556
149064577162
99406546489
101928427392
103718522410
105775928343
144783175607
167237628714
130678120366
152230126262
233994269308
323017139836
166335181503
178754629913
187110549259
169021079241
262989308351
172545282030
253950771387
421188400101
196844099931
205270887718
265830140878
359655831289
460206062110
244189722096
250993004554
313804254848
357669293797
396508261244
528288027554
458587430455
336258707955
341566361271
318565307765
335356260744
777152738220
338880463533
347775709154
356131628500
365865179172
369389381961
590209479342
402114987649
516823145432
441033822027
449460609814
583070185629
661579964002
562755029861
579545982840
564797259402
894153206726
674236724277
653921568509
660131669036
654824015720
657445771298
666341016919
674696936265
683131969898
890494431841
910530739015
703907337654
721996807672
1564731156118
1026835153259
851575597463
843148809676
1327920980921
1003788851888
1109592278850
1127552289263
1144343242242
1819040178507
1218718827911
1219621275122
1357828906163
1308745584229
1311367339807
1312269787018
1340577741196
1323786788217
1341037953184
1686920821786
1987492051918
1952741088526
1694724407139
2061736243817
1565145617348
1846937661564
2030624005147
1855364449351
3097084330768
2640190767939
2113381130738
2237144568113
3035762360323
2438340103033
2543408063339
2530988614929
2528366859351
2623637126825
2905723358544
2635154128024
3981228721123
4737018257563
2664824741401
3293779041710
3877561666711
3259870024487
3412083278912
3541662068703
3420510066699
3595769622495
3702302110915
3885988454498
8718246978686
5288461868226
4675484671146
4350525698851
4765511427464
4966706962384
5163520987375
8271254293641
5152003986176
7306498521197
6367126852316
5299978869425
7145858478985
8614579924274
7610395723338
6996081152625
6671953303399
6680380091186
8052827809766
7243964179618
11042611523462
10717652551167
7298071733410
7588290565413
8236514153349
14302579673822
9116037126315
13380091351738
9317232661235
11132638279780
12407485166993
10315524973551
10451982855601
11519130838492
11667105721741
17352551279664
23832074207339
13352333394585
13668034456024
13676461243811
13915917483017
19833689677482
14268670656599
20248675406095
17961616730785
25969685395563
14886362298823
15824804718762
16704327691728
20635167964807
18433269787550
19431562099866
19769215516836
19632757634786
26323402650010
23926616005485
23804316250186
30950692938358
23186236560233
32985091029371
34102360334081
40267925599593
27583951939041
32109731031361
27592378726828
28184588139616
37339495656535
30711167017585
31590689990551
32529132410490
33319632086373
34258074506312
59135281077974
43559373640271
51585860903165
37864831887416
42818994195019
39401973151622
47817345774402
50778615287061
69455521877967
46990552810419
54136929498591
75348126605509
55176330665869
55768540078657
91046540886654
55776966866444
82075420280714
58303545744413
85682177661818
70393964297906
85887497683454
64119822401041
71184463973789";
        #endregion

        #region Day 10
        public static string Day10 = @"47
61
131
15
98
123
32
6
137
111
25
28
107
20
99
36
2
97
88
124
138
75
112
52
122
78
46
110
41
64
63
16
93
104
105
91
27
45
119
14
1
65
62
118
37
79
77
19
71
35
130
69
5
44
9
48
125
136
103
140
53
126
106
55
129
139
87
68
21
85
76
31
113
12
100
24
96
82
13
70
72
86
26
117
58
132
114
40
54
133
51
92";
        #endregion

        #region Day 11
        public static string Day11 = @"LLLLL.LLLL.LLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLL.LLLLLLLLLLLL.LL.LLLLLLLLL.LLL
LLLLLLLL.L.LLLLLLL.LLLLLLLLLLLL.LLLLLLLLLLLLL.LLLLLLLLL.LLL.LLL.LLLLL.LLLL.L..LLLLLL.LLLLLLLL
LLLLLLLLLLLLLLLLLLLLLLLLLLL.LL.LLLLLLLLLLLLLL..LLLLLLLL.L.LLLLLLLLLLL.LLLLLL.LLLLLLLLLLLLLLLL
LLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLL.LLLLL.LL.LLLLLLLLLLLLLLLLLLLLLLL.LLLLLLLLLL.LLL.LLLLL.LL
LLLLLLLLLL.LLLLL.LLL.LLLLLL.LLLLLLL..LLLLLLLL.LLLLLLLLL.LLLLLLL.LLLLL.LLLLLL..LLLLLL.LLLLLLLL
LLLLLLLLLL..LLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLLLL.LL.LLLL.LLLLLLLLLLLL.LLLLLLL.LLLLLL.L
L.LL.L...LL......LLL........LL...L.L.L...L...L....L........LL......L.............LL..LL.L...L
LLLLLLLLL..LL.LLLL.L.LL.LLL.LLLLLLL.LLLLLLLLL.LLLLLL.LLLLLLLLLLLLLLLLLLLLLLL.LLLLLLL.LLLLLLLL
LLLLLL.LLL.LLLLLLL.LLLLLLLL.LLLLLLL.LLLLLL.LL.LLLLLLLLL.L.LLLLL.LLLLL.L.LLLL.LLLLLLLLLLLLLLLL
LLLLLLLLLL.L.LLLLLLLLL.LLL..LLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLL.LLLLL.LLLLLL.LLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLLLLLLLLL.LLLLLLLLLLLL.LLLLLLLLL.LLLLLLLLLLLLLLLLLL.LLLL.L.LLLL.LLLLLLL.LLLLLLLL
LLLLLLLL.L.LLLLLLL.LLLL.LLLLLLLLLLLL.LLLLLLLL.LLLLLLLLL.LLLLLLL.L.LLLLLLLLLL.LLLLLLLLLLLLLL.L
...L..L..L..L...L..L...L............L......L.....L...LLL.........L..L.LL.LL.LL.LLLL..........
LLLLLLLLLLLLL.LLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLL.LLLLLLLLLLLL.LLL.LLL.LLLLLLLL
LL.LLLLLL..LLLLLLL.LLL.LLLL.LLLLLLL.LLLLLLLL.LLLLLLLLLLLLLLLLLL.LLLLL.LLLLL.LLLLLLLL.LLLLLLLL
LLLLLLLLLLLLLLLLLL.LLLLLLLL.L.LLLLL.LLLLLLLLLLLLLL.LLLLLLLLLLLL.LL.LL.LLLLLLLLLLLLLLLLLL.LLLL
LLLLLLLLLL.LLLLLLL.LLLLLLLL..LLLLLL.LLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLL.L.LL.LLLLL
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LLLLLLLLLLLLLL.LL.LLLLLLLLL.LLLLLLL.LL.LL.LLL.LLLLLLLLLL.LLLLLLLL
LLLL.LLLLLLLLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLLLLLLLLLLLLLL.LLLL.LL.LLLLL.LLLLLL.LLLLLLL.LLL.LLLL
LLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL.LL.LLLLLLL.LL.LLLLLLLLLL.LLLLLLLLLLLLLL.LLLLLLLL
LLLLLL.LLL..LL.LLL.LLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLL.LLL.LLLLLLLLLLLLL.LLLLL.LLLLLLLLLLLLLLLLL
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLLLL.LLLL.LLL.LLLLLLLL..LLLLLLLLLLLLLLLLLL.LLLLLLLLL
.LL...L......LLL.....L..LL..L.....L.LLLL.L......L.......L...L....L.L..L..LL.L.L.....L.L......
LLLLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLL.LL.LLLLLLLLL.LLLLLLLL.LLLLLLLLLLLLLLL.LLL.LL.LLLLL
LLLLLLLLLL.LL.LLLL.LLLLLLLLLLLLL.LL.LLLLLLLLLLL.LLLLLL.LLLLLLLLLLLLL..LLLLLLLL.LL.LL.LLLLLLLL
LLLLLLLLL.LLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLL.L..LLLLLLLLL.LLLLLLLLLLLLLLLL
LLLLLLLLLL.LL.LLLL.LLLLLLLLLLLLLLLL.LLLLLL.LL.LLLLLLLLLLLLLLLLL.LLLLL.LLLLLL.LLLLLLLLLLLLLLLL
LLLLLLLLLLLLLLLLLL.LLLLLLLL.LLL.LLL.LLLL.LLLL.LLLLLLLLL.LLLLLLL.LLLLLLLLLLLLLLL.LLLL..LLLLLLL
LLLLLLLLLL.LLLLLLLLLLLL.LLL.LLLLLLLLLLLLLLLLLLLLLLL.LLL.LLLLLLLLLLLLL.LLLLLL.LLLLLLLLLLLLLL.L
LLLLLL.LLLLLLLLLLLLLLLLLLLL.LLL.LLLLLLLLLLLLL.LLLLLLLLL.L.L.LLL.LLLLL..LLLLLLLLLLLLL.LLLLLLLL
LLLLLL.LLLLLLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLLLL..LLLLLLL.LLLLLLLLLLLLLLLLL.LLLLLLL.LLL.LLLLLLLL
LLLLL...L..L.L..L.L..LL..L..L...L.LL.LLLLL.L...........L....LLLLLL..L..L..L.LL.LL...LL.L.....
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL.L.LLLLLLLLLLL.LLLL
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLL..LLLLL.LLLLLLLLLL.LLLLL.LLLLLL.LLLLLLL.LLLLLLLL
L.LLLLLLLL.LLLLLLL.LLLLLLLLLLLLLLL..LLLLL.LL.LLLLLLLLLL.LLLLLLL.LLLLL.LLLLLL.LLLLLL..LLLLLLLL
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLL.LLLLLLLLL..LLLLLLLL.LLLLLLL.LLLLL.LL.LLL.LLLL.LL.LL.LLLLL
.L....L.L.L...LL..L.L.LL..L.LL..LL..LLL.LL..L..L.LL..L.......L.....L...LL......LLLL...L.....L
LLLLLLLLLL.LLLLLLLLLLLL.L.L.LLL.LLL.LLLL..LLL.LLLLL.LLL.LLLLLLLLLLLL..LLLLLLLLLLLLLL..LLLLLLL
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LL.LLLL.LLLL.LLLLLLLLL.LLLL.LLLLLLL.LLLLL.LLLLL..LLLLLL.LLLLLLLLL
LLLLLLLLLL.LLL.LLLLL.LLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLLLLLLLL.LLLLLL.LLLLLLLLLLLLLLLL
LLLLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLL.LLLL.LLLL.LLLLLLLLLLLLL.LLLLLL.LLLLLLL.LLLLLLLL
LLLLLLLLL..LLLLLLL.LLLLLLLL.LLLLLLL.LLLL.LLLL.LLLLLLLLL.LLLLLLL.L.LLL.LLLLLLLLLLLLLLLLLLLLLLL
LLLLLLLLLLLLLL.LLL.LLLLLLLL.LLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLLL.LLLLLL..LLLLLLLL
L......LLLL...LLL.L..L.LL...LL.L.....L.LL...L...LL..LL..LLL..L.L.LLL..L..L...L.LLL..L....L...
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLL..LLLLLLLL.LLLLLLLLL.LL..LLLLLLLLL.LLLLLLLLLLLLLLLLLLL.LLL
LLLLLLLLLL..LLLLLL..LLLLLLL..LLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLL.LLLLL.LLLLLLLLLLLLLL.L.LLLLLL
LLLLLLLLLLLLLLLLLL.LLLLLLLLLLL..LLLLLLLLLLLLL.LLLLLLLLLLLLLLLLL.LLLLLLLLLLLL.LLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLL.LLLLLL.LLLLLLLLLL.LLLLL
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLL.L.LLL.LLLLLL.LLL.LLL.LL.LLLLL
..LL.LLLLL.L...L...LL.......L.........LL..L.LLL.....LL..L.L.LL..L..L.L..L.L..L...L.L.......LL
LLLLLLLLLL.LLL.L.L.LLLLLLLLLLL.LL.L.LLLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLL.LLLLL.LLLLLLLLLLLLLLLLL
LLLLLLLLLL.LLLL.LLLLLL..LLL.LLLLLLLLLLLLLLLLL.LLLLLLLLLLL.LLLLL.LLLLLLLLLLLL.LLLLLLL.L..LLLLL
LLLLLLLLLLLLLLLLLLL..LLLLLLLL.LL.LL.LL.LLLLLL.LLLL.LLLLLLLL..LL.LLLLLLLLLLLL.LLLLLLL.L.LLLLLL
LLLLLLLLL..LLLLLLL.LLLLLL.LLLLLLLLLLLLLLL.L.LLLLLLLLLLL.LLLLLLLLLLLLL.LLLLLL.LLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLLLLL.LLLLL..LLLLLLLLLLLL.LLLLLL.LLLLL.LLL.LLLLLLL.LLLLL.LLLLL..LLLLLLL.LLLLLLLL
LLLLLLLL..LLLLLLLL.LLLLLLLL.LLLLLLLLLLL.LLLLL.LLLLLLLLL.LLLLLLL.LLLLL.LLLLLL.LLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLL.LL.LLLL.LLL.LLLLLLLLLLLLLLLLLLLLL.LLLLL.LLLLLLL.LLLLL.LLLLLL.LLLLLLL..LLLLL.L
LL...LL........L.L....L..L....LLL.L...LL.L......L.LL..L...L.L.L.L....L..L.L...L.L.L....L.L..L
LLLLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLL.L.LLLLLLLL.LLLLLLLL.LLLLLL..LLLLLLL.LLLL.LLLLLLL.LLLLLLLL
LLLLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLLLLLLLLLLLL.LLLLLLLLLLLL.LL.LLLLLLLLLLLLL
LLLLLLLLL..LLLLLLL.LLLLL.LLLLLLL.LL.LLLLLLLLL.LLLLL.LLL.LLLLLLL..LLLL.LLL.LL.LLLLLLL.LLLLLLLL
LL.LLLLLLL.LLLLLLL.LLLLL.LL.LLLLLLL.LLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLL.LLLL
L.LL.LLLLLLLLLLLLL.LLLLLLLL.LL.L.LLLLLLLLLLLLLLLLLLLLLL.LLLLLLLLLLLLL.LLLLL..LLLLLLL.LLLLLLLL
LLLLL.LLLL.LLLLLLL.LLLLLLLL.L.L.LL.LLLLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLL.LLLLLL.LLLLLLLLLLLLLLLL
LLLLLLLLLL.LLLLLLLLLLLLL.LL.LLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLL.LLLLLLLLLL.L.LLLL.LL.LL.LLLLL
...L.....L............L...L.LL.L.L.L.L...LLL.L.LL.......LL.L.LLLL..LL......L...LLL..L....L...
LLLLLLLL.L.LLLLLLLLLLLLLLLL.LLLLLLL.LLLL.LLLL.LLLLLLLLLLLLLLL..LLLLLLLLLLL.L.LLLLLLLLLLLLLLLL
LLLLLLL..LLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLL.LLLLL.LLLLLL.LLLLLLL.LLLLLLLL
.LL.LLLL.L.LLLLLLLLLLLLLLLLLLLLLLLL.LLLLLLLLLLLL.LLLLL..LL.LL.L.LLLLL.LLLLLLLLLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLL..LLLLLLLL.LLL.LLLLLLLLLLLLL.L.LLL.LLLLLL.L.LLLLL.LLLLLLL.
LLLLLLLLLLLL.L.LLL.LLLLLLLL.LL.LLLL.LLLLLLLLL.LLL.L.LLL.LLLLLLL.LLLLLLLLLLLL.LLLLLL..LLLLL..L
.L..LL.......L..L..L.L........LL...LL.LLLLL........L.LLL......L..L.....L.LL....LLL.....LLL.L.
.LLLLLLL.L.LLLLL.L.LLLLLLLL.LLLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLL..LLLLLLLLLLL.LLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLL.LL.LLLLLLLLL.LLLLLLLLLLLLLLLLL.LLLLL.L.LLLLLLLLLLLL.LLLLLLLL
LLLLLLLLLLLLLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLLLLLLLL.LLLLLL.LLL.LLLLLLLLLLLL
LLLLLLLLLLLLLLLLLL.LL.LL.LL.LLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLL
LLL.LLLLLL.LLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLLLLLLLLLLLLLL.LLLLLLL..LLLLLLLLLLL.LLLLLLL.LLLLLLLL
LLLLL.LLLLLLLLLLLL.LL.LLLLL.LLLLL.L.LLLLLL.L..LLLLLLLL..LLLL.LLL.LLLLLLLLLLL.LLLLLLLLLLLL.LLL
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLLLL.LLLLLLLLLLLLLLLLLLLL.LLLLLLLL
LL..L....L...L.L........L.L....L..L..L...L..L..L........L...LLL..L.LL..L........LL..........L
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLL.L.LLLLLLLLL.LLLL.LL.LLLLL.LLLLLLLLLLL.LL.LLLLLLL.
LLLLLLLLLL.LLLLLLL.LLLLLL.L.LL.L.LL.LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLLLLL.LLLLLL.LLL.LLL.L.LLLLLLLLL.LLLLLL.LL.LLLLLL..LLLLL.LLLLLL.LLL.LLLLLLLLLLLL
LLLLLLL.LL.LLLL.LL.LLLLLLLL.LLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLL.LLLLL.LLLLLLLLLLLLLL.LLLLLLLL
.LL.LLLLLLL.LLLLLLLLLLL.LLL.LLLLLLLLLLLLLLLLL.LLLLLLLLL.LLLL.LL.LLLLLLLLLLLLLLLLLLLLLLLLLLLLL
LL.LLLLLLLLLLLLLLL.LLLLLLL..LLLLLLLLLLLLLLLLL.LLLLLLLLL..LLLL.L.LLLLLLLLLLLLLL.LLLLL.LLLLLLLL
.....L.....LLLL......LL.L...LL...L..L........L...L..LL..LL.LL.LL..LL...L.......L..LLL..L...LL
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LLLLLLL.LLLLLLLLL.LLLLLLLLL.LLLLLLL.L.LLL.LLLLLL.LLLLLL..LLLLLLLL
.LLLLLLL.L.LL.LL.L.LLLLL.L..LLLLLLLLLLLLLLLLLLLLLLLL.LL.LLLLLLLLLLLLL.LLLLLL.LLLL.LL.LLLLLLLL
LLLLLLLLLL..LLLLLL.LLLLLLLL.LLLLLLL..LLLLLLLL.LLLLLLLLL.LLLLLLL.LLLLLLLLLLLLL.LL.LLL.LL.LLLL.
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LLLLLLL.LLL.LL.LL.LLLLL.LLL.LLLLLLLLLLLLLLLL.LLLLL.LLLLLLLLLLLLLL
LLLLLLLLLL.LLLLLLL.LLLLLLLL.LLLLLLLLL.LLLL.LL.LLL.L.LLL.LLLLLLL.LLL.L.LLLL.LLLLLLLLL.LLLLLLLL
..L...LLL..LL...L.L.LLLL..L.L...L..LLL...L...LLL.....L.....LL.L....L....L.L..L.L.L....L..L..L
LLLLLLLLLLLLLLLLLL.LLL.LLLL.LLLLLLL.LLLLLLLLL.LL.LLLL.LLLLL.L.L.LLLLL.LLLLLL.LLL.LLL.LLLLLL.L
LLLLLLLLLL.LLLLLLLLLLLLLLLL.LL.LL.L.LLLLLLLLL..LL.LLLLL.LLLLLLLLLLL.L.L.L.LL.LLLLLLL.LLLLLLLL
LLLLLLLLLL.LLLLLLL.LLLLLL.LLLLLL.LL.LLLLLLLLLLLLLLLLLLL.LL.LL.L.LLLLL.LLLLLLLLL.LLLL.LLLLLLLL
LLLLLLLLLL..LLLLLLLLLLLLLLLLLLLLLLL.LLLLLLLLL.LLLLLLL.L.LLLLLLL.LLLLLLLLLLLL.LLLLLLL..LLLLLLL
LL.LLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLLLLLLLLLLL.LLLLLLLLLLLLLLLLL.LLLLL.LLLLLL.LLL..LL.LLLLLLLL";
        #endregion

        #region Day 12
        public static string Day12 = @"N2
R90
S2
F40
R180
F50
R90
W1
F79
W4
F3
E3
F76
L90
N2
L90
S4
W2
S2
F96
R90
F64
E4
N5
R180
F54
S5
L180
N3
F33
E4
S4
L90
S2
E2
F37
N3
W2
L90
S5
R90
E1
F98
R270
E5
F21
L90
F84
E3
F21
S5
F42
E3
L270
E3
L180
S2
E4
S4
W4
R180
F92
N2
E2
R90
F55
S4
L90
E2
R90
N3
W1
L90
F92
R90
N2
L90
W4
R90
F57
N3
F99
E2
R90
S2
W4
L180
N3
F32
R90
F26
E1
N5
R90
S2
F66
W1
L90
F8
E4
S4
F27
W4
F78
R90
S1
E1
R180
W5
R90
F55
E1
F9
E3
F54
N4
W2
F21
W4
N2
L180
F45
S5
F62
L90
E1
N5
F36
W4
R90
N1
F35
E3
R90
W4
S3
R90
F31
R90
N3
W5
F98
N1
E3
R90
S5
S4
W3
F28
E3
F67
R90
F57
L90
L90
F39
N2
F49
W1
N4
L90
S1
F71
R90
F95
R90
F78
R180
N3
F41
N2
R180
W4
E4
N4
W5
R90
F59
S5
F26
R90
S4
F8
R90
S3
R90
F81
S2
W5
F82
L90
S4
F41
E3
R180
F83
W2
S4
W1
R90
S3
F53
W4
F79
N4
W3
E2
F14
W3
R90
F35
R90
F82
S5
R90
F46
E5
R90
S4
L270
E1
S4
W2
S2
E1
L90
F61
L90
F75
S1
F2
W2
R90
S3
W4
R180
F60
R90
S3
E1
F21
L180
F93
N5
F17
N2
F23
L90
F30
R180
F27
N2
F55
L90
S1
E5
R90
W4
F68
W2
R90
E1
N5
F12
S1
F26
E5
W1
L90
N1
L90
W3
R180
F90
L180
E3
F9
L90
F50
L90
S5
F5
F46
R90
E4
L90
F8
S5
W3
S5
W3
F84
R90
F61
S3
F41
R90
F78
L90
E5
N5
F95
N2
F15
R180
S2
L90
E4
L90
S4
R90
F81
F35
R90
F98
E1
S5
N2
F58
L90
F89
W1
L90
F8
L90
N2
F68
S5
W1
S1
F87
L90
F57
W1
R180
F30
E2
F69
W2
R90
S3
R90
W5
N5
F46
R180
F99
F84
S5
F78
S3
F36
N1
W5
L90
W3
R90
S1
R90
F66
R180
L180
S5
F54
L180
F79
E5
L90
E3
F33
S2
S2
L90
F78
E4
F90
L90
F33
S4
W2
F44
L90
S3
W1
L180
F42
N5
F17
R90
E4
L90
E5
S5
E2
N5
W2
R90
W3
L90
F2
W2
N2
F62
N3
L90
N2
W3
F86
N3
W2
L90
E5
R90
L180
F42
N4
E3
F62
E5
F78
R90
N3
R90
W1
N1
E4
L90
F91
N4
W5
F25
N3
R90
E1
R90
E1
N2
F21
L90
F97
S2
L90
S2
E4
F48
S1
R90
F4
N5
R90
R90
N4
F11
W5
N4
F52
L90
W2
L90
R180
F77
S5
F52
N5
W5
N4
L270
W5
F93
S2
F98
N1
E5
N1
L90
F30
L180
W1
S5
L90
S1
R90
R90
F43
E2
F72
W1
N3
F90
L90
N5
F60
E4
R90
E3
L90
S3
F41
S2
L90
N1
F49
L90
S5
F30
S3
R90
F72
R180
N2
W3
S1
R90
N3
E4
S4
F43
R180
S2
E5
S1
L90
W4
L90
E2
N3
F52
N1
F4
W5
F59
L90
W2
N3
W5
S3
L90
W2
N5
E1
F70
S5
L90
E4
R90
R180
E3
S4
F53
E4
L180
F41
S1
E1
L90
W3
S2
F16
S4
R90
R90
W5
N4
E4
N5
L90
E1
N3
F100
S1
F88
S3
F10
W3
L180
S4
L180
N1
E1
S3
F77
W5
E3
L270
N3
L90
F51
N5
F79
S4
E2
L90
E2
F40
R90
S5
W1
L90
N4
S4
R180
E4
S4
R90
F49
L90
S5
E4
R90
S5
L90
S4
R90
E1
N3
F4
E2
L90
E4
R180
S4
E2
F92
E3
L180
S1
R180
W5
R90
F67
S1
R90
F48
W2
L90
S5
F72
R90
N5
E3
F64
F63
W5
F72
W1
F56
R270
E2
N3
F67
S4
F78
E2
R90
E3
L90
S1
F97
F14
L270
W1
F79
N5
E1
S1
R180
W2
F39
S1
W4
R90
E2
F51
S3
E3
L90
S3
F78
R90
S1
W1
F77
S5
F71
S4
L90
W3
F39
S2
E4
S3
F19
E2
N2
E3
F78
N3
F10
S2
W4
L90
W2
W2
L90
F67
N4
W4
R90
S1
W1
F42
E5
N3
R180
W3
S4
W2
S5
W5
L90
E4
R180
N1
L180
E5
L90
N3
E4
F23
S1
F84
R90
E1
L180
E5
E2
F54
E2
S5
E4
L270
L90
R90
E2
R90
N1
L90
F82
L90
W5
F83
S1
F73
L180
F39
F72
S1
L90
W5
R90
S1
W5
F27
W1
L90
N3
W4
F85
L90
N1
R90
S5
E4
S3
R180
F22
S1
F21
S5
L90
E2
F18
S5
E4
S3
F84";
        #endregion

        #region Day 13
        public static string Day13 = @"1015292
19,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,743,x,x,x,x,x,x,x,x,x,x,x,x,13,17,x,x,x,x,x,x,x,x,x,x,x,x,x,x,29,x,643,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,23";
        #endregion

        #region Day 14
        public static string Day14 = @"mask = 001X11X1X010X1X1010XX10X100101011000
mem[43398] = 563312
mem[51673] = 263978
mem[18028] = 544304215
mask = X0100001101XX11100010XX110XX11111000
mem[24151] = 2013
mem[15368] = 19793
mem[45005] = 478
mem[1842] = 190808161
mem[36033] = 987
mem[26874] = 102
mask = 00X0000110110X000110010101XX0X010001
mem[9507] = 7
mem[50019] = 16475608
mem[4334] = 129799
mem[37373] = 182640
mem[28170] = 534617265
mem[6432] = 354252
mem[36752] = 834628
mask = 10100000101101100110X001X0X001100X10
mem[36664] = 30481
mem[6532] = 103013119
mem[45659] = 15629
mem[19533] = 167227
mem[40461] = 344193233
mem[6217] = 26713310
mask = X0XX010100110101X0001101X11100X100X0
mem[38530] = 6202
mem[53032] = 13775
mem[39333] = 1003152
mem[3932] = 1240562
mem[59246] = 12638
mask = 0X1X010100X001X0011000X10000011X11X1
mem[51007] = 43736089
mem[32553] = 977
mem[5131] = 323347526
mem[21451] = 176282356
mem[22857] = 118
mem[50924] = 217
mask = 001111X110100X11X100000011110111X100
mem[3954] = 3854
mem[19628] = 6778501
mem[29233] = 104
mem[18456] = 135287
mem[10018] = 379
mem[14384] = 969770374
mask = 0X1X000110X1X10001100110100X01000001
mem[25112] = 62086
mem[22964] = 2379583
mem[45021] = 1429003
mask = X010XX11X0110110011XX000011000110001
mem[17265] = 180092
mem[36033] = 495818745
mem[28455] = 7765821
mask = 01000X0110110001100X01X1111000XXX001
mem[395] = 37390
mem[6432] = 962
mem[10247] = 130364
mem[10136] = 529
mem[62469] = 62129
mask = 00001100X010X110011X0000111X0X100XX1
mem[8811] = 206575
mem[37066] = 41
mem[53499] = 1505104
mem[22863] = 59636084
mem[50013] = 45392
mem[29757] = 1343911
mask = 001000011010X10X10000001XX011X000010
mem[23068] = 8829046
mem[49194] = 614470096
mask = XX1X000110X101100X101100X01101X00X11
mem[62870] = 3995829
mem[61328] = 18642
mem[50232] = 70531300
mem[48827] = 17923
mem[12416] = 530017
mem[33496] = 181946
mask = 00100X0110XX00011X00000X101100X10100
mem[64825] = 5590
mem[55315] = 3210
mem[532] = 92226
mask = 00X0XX0000100110011010X0X00XX101011X
mem[35112] = 1037772
mem[46051] = 5636
mem[32440] = 5415168
mem[6812] = 64661
mask = XX1111110010110X010XXX0010X0110XX001
mem[2313] = 14107547
mem[57582] = 3420940
mask = 0X100X01101X011101001001111101110X1X
mem[1584] = 1309601
mem[45021] = 142440
mem[52855] = 22177947
mask = 001X1X11XX101101010010X01X00X100X000
mem[51649] = 224687001
mem[30137] = 16118
mem[49157] = 1286
mask = 0011010X001X0101110X01000X11011X0100
mem[8527] = 483
mem[23222] = 60397
mem[47303] = 4597311
mask = 1010001110X00X0010000X100X0X01011100
mem[61912] = 65321
mem[28793] = 217
mem[3216] = 2226
mem[15267] = 196
mem[12210] = 634690438
mask = XX100001100101011XX000001X0X00X10X10
mem[52112] = 232196
mem[5131] = 8215922
mem[21390] = 97675
mem[60773] = 295919
mem[10967] = 188393052
mem[30137] = 40094772
mask = 01100X00001X0111X11001011X000X1010X0
mem[47036] = 8270917
mem[26111] = 3884
mem[48992] = 3941
mem[21396] = 9612429
mask = X01X01X0100X101001XX00001X1101X00110
mem[47036] = 785762
mem[20586] = 91901152
mem[38530] = 338166139
mem[29577] = 753085
mask = X01001010001011100X01XX010010011X000
mem[50548] = 31352881
mem[17969] = 264
mem[12532] = 122897915
mask = 100X0X011011110001001XX0100110010101
mem[7092] = 488918089
mem[5131] = 2146748
mem[13662] = 1422934
mem[54353] = 299758672
mem[17622] = 15998
mem[12416] = 48024869
mem[15520] = 925305185
mask = X010000100X1000X1000X10010X010100010
mem[49608] = 17989
mem[5478] = 192384
mem[7958] = 729
mask = 0010X001X1100X1X1X000100XX10100X1100
mem[47164] = 170643
mem[1049] = 151435402
mem[24631] = 47998921
mask = 101001XX0X1X010110011101000110110111
mem[39780] = 29719
mem[20606] = 714268
mem[40889] = 367330023
mem[6414] = 28304231
mem[63401] = 1417
mask = 1X10010X00X10101100X010110X1100X1001
mem[38445] = 392
mem[14087] = 19086
mem[36110] = 7609
mem[61683] = 24000
mem[55077] = 2975
mem[2109] = 446867
mask = 011000011X010X0X1X100110100001X1XX10
mem[32849] = 150162
mem[22563] = 3985
mem[10602] = 225990962
mask = 00X00X01X00001X1X0000X00101100100110
mem[43197] = 134523909
mem[65396] = 266246531
mem[54292] = 263069
mem[7677] = 99022189
mem[16568] = 15208393
mask = 1X100X011X1000111000XX1100011X00X0X1
mem[2877] = 1577
mem[39731] = 1276
mem[10602] = 844609393
mem[13447] = 4710
mask = 0010X00110XX0101100X00XX100110X10100
mem[10466] = 93198549
mem[21290] = 624
mem[2948] = 51676784
mem[23734] = 6032
mem[29894] = 48902591
mem[271] = 60066
mask = 1011000110X1011X001X11110100110111X0
mem[1843] = 1562
mem[1049] = 9936
mem[14474] = 305948608
mem[40634] = 680784423
mem[9394] = 12344199
mask = 000011X000100X10011X000X001100X10X10
mem[37198] = 9587
mem[40486] = 15533376
mem[28252] = 1625
mem[59079] = 166206
mask = 0010010110110XXXXX0X0001100100000X00
mem[40119] = 168760390
mem[63012] = 1016
mem[6964] = 13134
mem[6116] = 19700991
mem[60039] = 492285
mask = 10X000011X10001X100000111X10000101X0
mem[532] = 16484832
mem[48228] = 18188385
mem[65048] = 14886349
mem[29631] = 1088356
mask = 0100X00110110X00XXX010110X11X111100X
mem[24637] = 561045
mem[62166] = 62287574
mem[395] = 1350
mem[46447] = 15165
mask = 01101XX100110X1X11001X0X0X0X01011110
mem[29481] = 88712206
mem[8052] = 965421
mask = X0X0010100X1X101101X111010X11010X10X
mem[27388] = 41257883
mem[22151] = 2499234
mem[17067] = 1210879
mask = 00100001X0X10X0110000000101XX001010X
mem[148] = 43621
mem[23734] = 243862817
mask = 101000XX1010001110000X1X00011X0X0100
mem[19226] = 7783454
mem[47036] = 32167689
mem[54708] = 28465363
mem[25775] = 13654
mem[38159] = 226030009
mem[33886] = 22797977
mem[47934] = 34738195
mask = 1010000110110111000101011X0XX1X111X1
mem[8015] = 1639518
mem[32888] = 89628061
mem[19414] = 3293870
mem[45803] = 3055
mem[2849] = 517315
mem[7103] = 1807237
mask = 0010010X1X110111011010001X010X10X110
mem[32337] = 14059
mem[7162] = 22418419
mem[62068] = 491160015
mem[52514] = 62411508
mem[21998] = 16113734
mem[14899] = 4165873
mask = 01000101101001100110XX01011XX0X00XX0
mem[63651] = 706
mem[27388] = 269141496
mem[16791] = 90544
mem[58514] = 2084386
mem[6512] = 82029923
mask = XX100X0X1011011X0110X00000011X10XX10
mem[25492] = 51834825
mem[39104] = 11018
mem[31518] = 5721690
mask = X01001X0101101110X10010111X11X110000
mem[16817] = 43478591
mem[49714] = 32182
mem[7715] = 20391
mem[36282] = 511726
mem[2709] = 58604
mask = 10100001101000XX10001001X110001101X0
mem[21290] = 96121933
mem[4581] = 935753770
mem[10322] = 214308733
mem[22563] = 955
mem[21998] = 174320
mask = 001X010010XXXX10011010010XX01X0X0100
mem[27908] = 10394
mem[58731] = 17043901
mem[12207] = 89277
mem[50189] = 70951683
mem[40310] = 1070062397
mask = X11001X11010X1100110X101X0100X0X01X0
mem[48661] = 35809
mem[6512] = 466
mem[22172] = 9259291
mask = 001X0001X001010XX001000010X1001X1111
mem[45021] = 7965
mem[10414] = 132450
mask = X11001111X10111001X001010X001X00011X
mem[22734] = 23954922
mem[18333] = 522531412
mem[21084] = 2928539
mask = 0010000X00110101XX0XX111100100100110
mem[10793] = 30167743
mem[54236] = 15119211
mem[46526] = 34600696
mask = X010010100X10101X0011X0010000X1XXXX1
mem[40874] = 107825637
mem[12207] = 5066
mem[64061] = 12594443
mem[14677] = 104815480
mem[47294] = 27328513
mem[36871] = 99385
mem[55732] = 3825863
mask = 0010X10X101X011101X000011X1100110000
mem[39282] = 9472566
mem[19564] = 55941
mem[8527] = 26084
mem[10265] = 130187
mem[6432] = 865842
mem[20931] = 1702
mask = 00110X0010X01X1001101X0000XX1XX0110X
mem[54465] = 11299
mem[13022] = 487449
mask = 00100XXX101101101101111010X100100000
mem[20710] = 1510193
mem[1742] = 2963920
mem[15368] = 241191
mem[48928] = 8865
mask = 0X10XX0100X10X011000X01101X0X1101110
mem[33496] = 157963055
mem[10527] = 1744363
mem[25912] = 24812738
mem[53894] = 65229499
mem[27656] = 195539
mem[56053] = 84622
mem[58013] = 503836980
mask = 00X0000X10X1011X0X10110011111X000011
mem[21324] = 100568910
mem[11832] = 25433857
mem[15696] = 65297
mask = 00100X010011010110XX01X11XX1001X0111
mem[1742] = 5701
mem[50038] = 1734
mem[3338] = 10181349
mem[64950] = 715735117
mem[3094] = 6261
mask = 01XX01001010X1X101XX000010X111110XX0
mem[30706] = 34032209
mem[57669] = 953918
mem[2368] = 18511
mem[58246] = 14197924
mem[12602] = 3821248
mem[37932] = 73626
mask = 00X0000110X000011XX0000011100X000000
mem[44726] = 577645454
mem[31822] = 2444199
mask = X0100X01X110001X1X0000X01010X0X00111
mem[40204] = 167462
mem[13234] = 334
mem[55553] = 649450
mem[18698] = 152213289
mem[56964] = 1004699
mem[17434] = 557
mask = 0X1000011111XX01X01XX10X0100000000X1
mem[54363] = 171716
mem[27133] = 813977
mem[25112] = 478238
mem[2734] = 2300
mem[23972] = 7597
mask = 0110X1X1X011011XX10010X11100X1111X00
mem[21998] = 2245
mem[39814] = 10501801
mem[16186] = 807
mask = X010000110100001100X0011X11000XX0010
mem[21976] = 1290104
mem[45127] = 1447
mem[19564] = 679
mem[8927] = 40098844
mem[43124] = 2060353
mem[17227] = 11511
mask = X110X00110X10101111000X0000X10111110
mem[25530] = 23237
mem[55910] = 1785756
mem[38723] = 1821559
mem[30849] = 4089
mem[532] = 661
mask = 0X10010XX01X011X011000011X01X1X001X0
mem[17212] = 6523
mem[37424] = 480
mem[40862] = 449969985
mem[28474] = 40994780
mem[21577] = 36128
mem[39066] = 7501680
mask = X100X101100X01X001101001X11001111110
mem[35142] = 186426
mem[28005] = 1296725
mem[57552] = 433183
mem[26566] = 56636
mem[4581] = 1646680
mem[35799] = 2658
mask = 001011010001X00110X00X10000001011X10
mem[13674] = 62138919
mem[63552] = 11168
mem[11669] = 56357099
mask = 101000XX1010XX0010000X10XX1100010010
mem[22434] = 34054009
mem[19261] = 856
mem[24828] = 7024
mem[34924] = 648168
mem[22917] = 9557844
mask = 10100X0X1011111100X1010000000111100X
mem[63552] = 11477437
mem[23072] = 8131648
mem[19002] = 1064
mem[23946] = 183
mem[2440] = 1277
mask = X0100001101X000X100001001X1X00110110
mem[55553] = 49381
mem[25631] = 41125881
mem[62633] = 590643
mask = 0X1000011001010001XX11X0010100000000
mem[10466] = 506832
mem[23072] = 5583
mem[45005] = 8337603
mem[59216] = 7005456
mask = 0XXX000111111001X0100100001000000100
mem[19928] = 48311172
mem[22974] = 815
mem[34266] = 13786112
mem[1742] = 9648313
mem[1094] = 162
mem[55709] = 31320282
mask = 0XX0000110100XX1X001001001X001000001
mem[22346] = 340600
mem[39104] = 935807
mem[64441] = 570
mem[56853] = 3313
mem[22434] = 1892025
mask = 01X000X1X0110XXXX1100110000X00101000
mem[26813] = 23072664
mem[9142] = 282783543
mem[29807] = 14754
mem[56288] = 62827
mask = 01X0010X1XX001X001X010X011100100X100
mem[47774] = 352023
mem[5938] = 132498542
mem[24828] = 8444211
mem[55829] = 238313735
mask = 0010010100X101X10XX0X10X110100XX1X00
mem[1908] = 30255
mem[40461] = 1524854
mem[21752] = 3313
mem[38177] = 164
mem[32888] = 20182288
mem[17656] = 2560835
mask = 01000101101XX11X0110X10X0000X1XX0000
mem[18028] = 6424701
mem[11832] = 73576
mem[18812] = 15408
mask = X010X00XXXX1X1100100X1101X1111110010
mem[31868] = 1008118155
mem[16970] = 560
mem[6414] = 659729
mask = 00100X0110X1010001X0XXX1X00100100X00
mem[60039] = 1335434
mem[22051] = 4352989
mem[23413] = 8881
mem[5131] = 3574
mem[31132] = 1822377
mem[59227] = 3565275
mem[55044] = 629
mask = 101001010001X101100X1XX01X01100X1111
mem[2119] = 6096
mem[25137] = 4534409
mem[34466] = 2697336
mem[24201] = 506176
mem[25286] = 110343
mask = 0XX00001101100011000010X101X0X11X100
mem[29807] = 25323
mem[12207] = 27513971
mem[9003] = 1398544
mem[28341] = 50817018
mem[30137] = 115
mem[42114] = 67247621
mask = 0000X1000010X1X0011010111X0X01X11X10
mem[48228] = 2010329
mem[45718] = 71839
mem[33886] = 136902
mem[51771] = 2015
mask = X000000110X101000100000010010X0011X0
mem[4243] = 33894587
mem[64857] = 8145
mem[45718] = 97465094
mem[53834] = 3359009
mask = 0010X101000101011X0X01X0101000000001
mem[24012] = 379049
mem[39780] = 26758
mem[59983] = 495835
mem[37409] = 1160
mem[52514] = 6321
mem[27459] = 147
mem[41942] = 217105
mask = 00100001100101011X101X0X10111XX10000
mem[17846] = 10118006
mem[59737] = 2963
mem[34644] = 35114650
mem[13172] = 143244
mem[5938] = 51096
mem[44123] = 3352
mask = 001000X1101101X001X001XX00110101X0X0
mem[38177] = 2168495
mem[11075] = 7671
mem[47735] = 2437651
mem[57709] = 103925776
mem[9577] = 253960744
mem[61912] = 713476954
mem[10466] = 509335816
mask = 0X10X00110010100011000001X01001X0001
mem[46526] = 985771
mem[63247] = 474051554
mem[22968] = 581
mem[29811] = 4967030
mem[57544] = 438283695
mem[7042] = 308851
mask = 001XXX010011X10XX100010X1100X0010100
mem[22856] = 10167
mem[25967] = 196716
mem[17344] = 30111
mem[3954] = 21193
mask = 011000XX1101000X00X00101001111000000
mem[45718] = 22050
mem[4315] = 28856671
mem[3954] = 1669054
mask = XXX000011X11X1000XX011X100X100010101
mem[25208] = 62883413
mem[40039] = 460470
mem[27976] = 317910
mem[6549] = 3697104
mem[34078] = 112
mem[62178] = 479428706
mask = 0110000111X1X0X1X0100100XXX001XX0010
mem[532] = 4184102
mem[25575] = 20376
mem[59465] = 35723765
mem[32827] = 2041066
mem[21963] = 519238
mem[56441] = 22508
mask = X0100001101101000100000X0011X01000X0
mem[34078] = 36026
mem[12451] = 602257
mask = 10X0010100X1X10110010010100X0X100011
mem[52291] = 3349730
mem[51550] = 12311148
mem[27235] = 986707194
mem[7958] = 2162
mem[36824] = 3705422
mask = 011X0X0111010000X110000X1X01XX0XX100
mem[21004] = 1994888
mem[10900] = 11111
mem[24854] = 1327
mem[45320] = 1739644
mem[29894] = 1918
mem[62034] = 165719
mask = 1XX10X10100X1010X1000100X0X10X001X00
mem[54431] = 68179
mem[48498] = 269569
mem[25492] = 53144423
mem[24130] = 510
mem[9579] = 22225
mask = 0X00X101101011X00110X0001001010011X0
mem[54156] = 597982
mem[3020] = 27476
mem[18748] = 105524
mem[37066] = 28361301
mem[43484] = 19990814
mem[18698] = 635178
mask = X0100001101000X1100X0110XX11111X1000
mem[44272] = 88008
mem[11075] = 919
mem[41491] = 2905
mem[4898] = 32296
mem[10607] = 10054
mem[28252] = 31037";
        #endregion

        #region Day 15
        public static string Day15 = @"17,1,3,16,19,0";
        #endregion

        #region Day 16
        public static string Day16 = @"departure location: 30-260 or 284-950
departure station: 29-856 or 863-974
departure platform: 32-600 or 611-967
departure track: 44-452 or 473-965
departure date: 36-115 or 129-950
departure time: 50-766 or 776-972
arrival location: 40-90 or 104-961
arrival station: 40-864 or 887-971
arrival platform: 32-920 or 932-964
arrival track: 45-416 or 427-959
class: 47-536 or 557-964
duration: 33-229 or 246-969
price: 25-147 or 172-969
route: 32-328 or 349-970
row: 50-692 or 709-964
seat: 49-292 or 307-964
train: 28-726 or 748-954
type: 37-430 or 438-950
wagon: 46-628 or 638-973
zone: 39-786 or 807-969

your ticket:
89,193,59,179,191,173,61,73,181,67,71,109,53,79,83,113,107,139,131,137

nearby tickets:
499,362,657,143,723,763,935,454,473,586,372,489,214,753,765,65,307,56,853,934
228,664,709,361,856,645,363,171,901,199,62,513,667,808,350,890,715,687,257,721
531,490,628,88,947,252,832,917,686,571,362,874,837,588,849,140,655,407,90,611
777,194,111,386,90,598,153,363,373,566,191,945,559,819,289,487,480,590,691,756
186,366,196,844,655,659,845,109,355,330,71,176,397,380,822,776,369,447,658,949
783,763,570,184,260,448,856,717,913,646,774,657,841,917,910,193,941,408,110,577
378,832,670,59,155,911,209,74,57,947,144,514,411,89,690,856,854,286,597,132
319,684,204,909,342,206,204,943,620,52,516,115,430,319,84,618,593,211,596,581
66,146,910,225,107,112,918,679,255,757,125,946,863,892,78,525,373,913,87,838
674,653,588,220,448,941,112,924,474,358,513,649,766,500,847,197,639,836,373,573
76,115,776,726,598,361,480,772,356,129,506,380,202,810,932,756,408,81,786,889
311,620,848,357,483,754,181,300,284,60,259,674,596,289,397,113,894,82,291,754
84,600,751,223,595,478,198,2,402,428,355,517,947,933,624,59,598,711,752,513
286,664,786,775,510,363,666,229,914,938,196,509,410,528,681,943,477,850,640,595
365,17,624,375,640,483,208,258,189,222,889,655,852,326,109,428,758,616,889,288
566,195,201,949,205,517,672,762,180,886,679,63,596,664,678,596,894,384,321,212
576,316,480,579,925,328,498,194,191,132,106,863,210,416,351,663,643,218,366,942
776,60,256,400,368,664,469,53,949,689,569,253,319,717,387,591,845,66,499,566
620,831,379,712,614,176,682,666,83,197,438,916,535,504,218,377,627,155,429,855
853,525,865,487,190,940,810,251,786,618,895,580,908,86,578,136,781,562,498,759
671,625,256,592,177,500,523,50,443,692,650,128,441,832,626,491,252,405,661,756
597,722,922,721,319,388,653,484,83,142,781,451,106,499,515,672,113,374,911,311
211,716,176,715,627,286,327,531,566,587,916,431,57,371,894,677,778,193,710,447
559,508,196,178,844,78,114,662,142,558,514,378,375,694,718,314,384,89,816,562
290,578,105,478,229,260,752,496,187,937,137,898,69,399,980,638,441,84,589,574
413,592,841,498,793,214,947,582,827,940,446,572,259,623,213,355,853,360,60,195
481,764,308,384,131,247,595,756,221,361,175,256,407,255,155,752,781,524,403,640
74,765,914,319,715,86,316,612,197,640,827,760,926,106,520,818,229,824,623,404
576,504,694,379,75,528,638,257,482,852,837,563,575,389,686,217,852,684,314,184
481,679,312,796,358,71,376,830,209,946,69,591,939,357,59,284,522,626,218,825
355,812,691,377,833,938,139,233,643,764,663,692,722,576,495,843,450,536,359,174
367,749,373,379,69,753,711,81,445,265,405,624,759,814,311,623,228,290,682,195
475,841,833,415,13,809,478,70,191,228,934,760,765,508,398,655,907,850,487,54
198,841,184,84,492,945,429,72,753,572,510,408,827,289,177,559,890,837,987,291
427,198,192,666,319,369,565,861,512,216,81,899,388,761,683,933,529,515,673,674
355,188,827,614,178,600,641,181,915,728,934,526,904,136,74,649,187,77,381,498
751,317,780,190,897,781,65,852,933,69,657,811,517,564,842,877,382,84,56,576
946,839,941,322,409,318,751,850,215,750,394,578,95,284,902,396,409,51,309,938
54,400,54,307,290,665,125,507,220,600,214,405,864,217,763,284,382,820,129,910
722,68,841,521,597,286,722,481,451,145,448,389,61,844,650,487,250,781,149,70
175,323,322,831,857,137,616,686,408,175,709,903,214,917,414,108,367,827,684,593
321,820,388,220,368,137,403,523,914,839,64,724,482,307,284,448,585,208,764,988
86,926,414,213,825,682,473,67,318,144,51,311,814,709,520,687,623,396,949,387
522,369,139,593,122,210,716,906,406,488,681,753,852,189,71,203,51,145,639,412
322,368,18,663,903,574,203,363,391,643,205,474,182,517,855,486,890,315,725,82
60,582,819,529,180,140,570,402,191,594,863,146,614,631,51,593,828,583,184,78
856,622,291,412,503,532,563,217,404,352,349,406,675,277,292,765,748,493,691,626
647,849,636,64,479,715,623,132,322,408,524,385,212,328,289,104,82,689,908,650
197,444,249,763,892,86,775,441,172,664,379,596,910,584,574,376,665,899,174,847
375,906,894,75,895,82,250,367,350,581,912,927,370,888,938,413,109,909,570,481
104,831,565,413,849,533,861,394,361,511,292,573,567,218,832,135,717,851,134,223
847,59,369,147,509,80,184,527,199,430,614,687,621,823,132,787,569,109,665,112
109,983,680,682,191,65,139,643,687,228,828,317,211,784,851,757,672,64,616,173
403,640,763,704,504,381,201,933,144,597,825,79,496,246,783,113,84,650,720,81
356,82,615,375,222,361,182,255,112,845,612,298,891,887,382,839,380,691,405,660
251,535,590,228,181,710,663,485,856,929,389,892,573,247,856,313,679,397,50,850
895,673,675,513,838,181,135,696,599,327,583,720,486,104,691,777,115,536,724,920
519,3,55,711,820,572,614,401,751,664,856,779,726,687,672,415,680,536,481,684
692,781,22,72,586,906,845,356,852,590,777,372,593,579,369,663,619,317,134,138
623,484,723,936,810,940,824,676,362,172,494,864,360,133,402,911,307,634,828,428
899,308,410,558,557,452,69,84,65,5,187,849,493,223,441,349,499,677,67,915
222,758,668,351,782,373,825,839,369,615,904,186,815,379,897,380,863,911,60,981
576,710,508,63,506,888,638,476,626,616,863,494,434,898,715,759,946,208,932,84
495,255,259,810,586,387,804,69,564,73,140,628,890,205,213,363,709,683,401,78
79,582,688,536,114,106,622,289,395,417,786,384,175,55,850,855,327,189,135,254
379,895,255,498,766,523,291,782,944,839,615,687,892,824,479,77,619,359,702,680
250,144,611,936,220,851,509,379,260,821,532,859,79,673,906,624,211,780,349,448
75,643,392,826,139,579,479,618,372,563,823,913,106,286,639,594,455,895,388,776
370,785,939,918,661,841,582,655,678,109,850,673,423,864,290,677,762,834,383,686
19,906,528,475,779,174,519,389,715,816,247,500,350,250,54,897,57,942,369,443
114,684,251,944,56,944,228,217,666,778,650,605,488,284,643,516,754,561,659,938
292,429,906,605,935,88,140,197,721,438,257,590,324,900,325,785,290,522,491,619
249,52,649,558,605,914,494,286,373,132,892,766,709,355,655,321,218,473,906,562
612,908,180,854,475,825,61,516,363,945,896,176,563,763,361,221,847,205,631,112
936,57,379,279,534,211,889,254,198,381,645,764,318,377,619,850,78,750,181,83
413,130,497,366,65,307,255,686,181,368,997,560,781,205,69,863,401,809,829,683
137,227,53,486,826,676,221,896,219,358,981,370,638,528,649,404,482,942,809,676
918,172,942,948,821,911,291,906,393,438,757,702,215,383,178,219,350,894,757,403
526,913,180,376,653,617,837,935,896,373,343,184,257,660,784,406,901,904,377,689
189,388,628,55,723,82,51,369,808,365,137,668,664,596,325,672,360,316,797,939
88,905,90,284,619,394,326,415,836,891,618,207,754,226,766,572,444,567,177,991
685,531,67,314,673,136,325,283,659,374,403,60,914,492,478,648,190,320,592,692
137,146,504,678,691,247,673,438,999,315,639,536,917,416,212,187,896,198,785,452
855,205,111,600,562,785,489,226,209,941,518,384,700,579,315,657,74,654,591,558
444,360,672,367,819,108,757,136,621,838,323,785,478,809,492,376,161,508,901,586
205,500,513,902,408,909,573,663,891,290,326,389,477,376,810,392,596,382,846,989
141,366,489,481,716,350,589,63,655,558,451,143,289,594,866,933,711,63,213,380
320,144,949,726,314,576,639,80,287,143,616,713,945,623,228,927,374,385,856,499
215,135,351,409,69,619,658,639,567,716,227,888,768,66,115,320,77,718,748,369
614,894,572,515,842,371,253,570,479,574,292,394,463,659,840,227,763,757,777,83
401,887,116,854,751,683,137,898,104,649,596,779,935,65,570,220,147,850,781,379
353,808,187,909,895,710,192,903,784,779,755,788,896,292,719,428,843,681,407,313
83,722,826,829,392,616,655,484,520,720,53,243,145,360,63,594,512,443,850,200
588,889,187,889,581,215,290,319,821,440,601,220,482,307,217,690,758,512,942,372
722,183,134,843,807,783,509,657,411,859,561,401,763,719,756,380,756,375,443,207
289,853,562,190,181,652,988,942,315,225,813,355,404,107,429,442,311,377,259,855
527,780,526,323,449,218,721,714,84,200,806,717,619,587,821,399,570,518,778,893
836,309,893,780,943,779,657,256,528,296,512,476,131,641,828,223,807,683,562,313
855,192,440,557,427,710,776,755,653,920,191,763,631,664,897,580,845,916,659,66
179,559,920,399,906,720,822,942,852,408,617,690,599,937,949,356,195,982,251,318
942,990,386,328,473,669,52,324,531,889,854,322,594,762,536,135,672,501,612,229
627,500,725,255,136,370,403,58,77,838,674,620,858,307,438,189,392,674,568,446
529,415,438,661,287,511,200,250,179,388,596,582,176,529,514,978,945,106,312,564
587,392,947,762,110,387,495,115,452,572,416,676,189,751,208,985,478,828,442,638
439,935,614,357,919,131,385,247,674,113,835,766,908,373,134,257,996,487,145,836
855,619,843,711,577,427,392,661,485,257,293,504,315,450,190,389,581,197,823,711
617,816,838,636,257,58,935,668,501,386,226,838,919,660,755,643,438,216,526,753
685,288,86,781,908,140,310,144,258,52,132,560,803,622,591,134,218,248,566,518
782,290,896,384,324,452,671,562,508,653,475,996,683,624,494,597,145,682,67,910
567,532,719,399,50,616,922,914,310,612,353,653,400,384,562,684,492,568,135,689
170,749,486,901,108,825,887,210,519,226,762,188,590,903,452,617,615,834,363,517
308,497,496,400,530,512,939,647,514,359,435,625,186,580,66,206,718,491,657,50
558,698,326,195,809,89,135,378,518,846,692,204,716,573,176,369,583,752,227,651
399,982,611,404,619,317,86,486,524,673,51,489,751,318,766,486,826,249,381,815
597,856,16,375,61,137,906,888,439,669,782,893,902,398,785,900,671,319,710,136
311,653,224,823,442,220,902,603,367,54,945,195,897,67,60,360,814,450,246,665
358,579,849,813,315,308,825,681,827,114,857,352,481,440,212,944,836,312,501,439
55,129,66,942,133,137,208,50,478,205,200,916,851,487,889,824,989,691,490,364
308,381,688,440,135,516,816,504,287,625,750,355,663,714,176,391,229,989,904,284
213,780,407,912,573,535,585,825,897,842,588,469,893,321,567,831,90,138,613,863
853,119,581,373,84,761,217,640,752,383,226,650,357,322,438,503,292,559,291,492
684,874,935,786,89,852,940,397,643,51,722,647,79,50,817,86,585,822,611,575
777,890,911,915,114,452,635,204,79,289,310,449,628,247,358,948,894,307,350,893
724,685,936,485,887,82,835,870,481,615,616,751,131,505,111,82,912,438,50,405
520,826,224,592,411,284,834,668,902,657,661,897,758,369,933,809,704,501,222,777
624,628,105,260,663,502,447,436,310,851,836,208,911,591,718,755,844,647,854,497
762,175,201,4,644,111,427,490,713,409,531,143,577,75,287,663,172,411,911,57
285,225,187,359,57,911,315,56,892,398,216,533,784,916,10,690,912,133,939,328
754,649,519,319,315,807,640,749,668,404,594,514,175,587,637,582,188,90,219,427
906,146,274,173,229,562,480,518,897,821,53,913,319,63,189,571,485,613,509,353
502,646,58,352,505,660,575,133,671,811,586,350,850,802,474,360,911,136,564,753
106,62,354,247,935,626,978,371,834,214,586,76,659,532,938,444,388,443,668,574
132,473,368,718,80,754,751,947,903,772,284,256,650,258,251,682,197,619,758,501
77,892,401,675,270,935,183,658,578,497,373,209,807,187,209,85,205,251,367,57
750,209,834,811,665,898,361,64,114,685,258,943,644,532,129,614,924,76,377,405
195,428,488,67,935,19,375,392,484,450,892,416,260,650,256,194,397,483,113,557
474,852,196,427,375,977,639,643,536,785,66,452,326,623,828,309,909,373,138,113
219,225,818,84,639,380,856,676,372,82,76,577,517,758,942,182,604,194,400,357
908,622,568,81,183,901,868,354,818,835,894,430,482,824,856,839,190,77,88,840
641,572,228,286,936,889,818,777,778,846,643,113,150,138,754,934,582,581,627,685
898,891,841,0,62,908,110,888,74,831,286,653,407,138,653,932,514,853,357,598
622,413,899,828,185,194,173,571,481,670,315,515,562,207,836,708,766,683,488,372
248,457,934,372,564,414,662,665,949,642,202,144,189,842,214,592,374,888,211,247
668,139,115,132,522,683,205,849,784,481,713,776,689,63,563,658,361,982,196,897
289,407,60,434,323,561,196,319,517,846,686,855,173,864,611,814,717,618,313,687
474,892,536,259,888,86,81,328,687,402,375,891,291,666,805,909,386,193,511,898
131,210,638,840,392,765,565,841,394,781,813,562,316,145,526,528,826,803,87,197
841,557,935,116,837,478,174,839,623,752,71,484,709,685,495,360,327,143,440,908
452,86,411,358,847,632,853,716,410,491,934,86,196,598,600,486,402,212,493,714
766,663,131,474,888,85,569,710,251,225,846,194,692,925,398,783,226,291,189,488
657,560,388,477,509,87,139,911,258,671,947,753,865,913,324,638,366,526,584,429
658,181,83,611,259,561,752,121,758,561,612,478,518,763,896,596,68,328,173,446
625,810,62,508,458,355,847,896,220,838,933,536,376,112,427,190,511,403,479,675
367,495,523,441,694,656,286,309,203,839,481,848,646,830,383,221,848,185,615,208
311,526,497,750,473,916,393,396,564,191,479,723,627,182,444,484,173,754,661,8
759,848,112,516,668,189,217,617,598,117,831,251,906,905,560,86,573,938,63,908
188,889,534,86,999,187,441,186,762,61,827,106,222,756,814,113,716,54,200,808
809,259,908,290,905,316,761,593,846,408,509,780,516,447,311,201,773,217,948,932
210,253,621,481,900,557,318,379,909,511,985,904,681,716,115,70,214,385,825,427
533,130,571,599,621,141,612,786,838,402,662,186,297,820,759,381,208,592,576,318
320,253,349,831,394,557,105,752,522,191,181,891,200,13,409,762,833,317,683,55
401,416,920,309,583,665,570,369,199,54,455,185,513,80,527,912,442,783,615,85
365,190,504,219,595,618,593,350,782,809,66,919,427,474,406,137,81,14,189,442
135,187,428,506,662,209,850,905,184,855,935,584,606,944,819,371,825,720,598,912
622,620,408,224,530,792,894,855,372,667,653,820,514,180,479,685,190,196,107,560
205,848,375,252,628,479,670,147,413,156,759,949,489,719,681,493,70,508,807,835
776,676,62,512,658,477,215,748,911,186,500,557,213,862,905,373,677,250,410,250
58,88,684,855,626,498,574,631,503,749,75,59,641,207,440,404,139,887,751,363
113,186,66,204,104,726,115,137,617,832,90,755,664,517,452,635,594,749,892,363
356,863,658,575,896,903,373,645,192,907,277,816,844,532,519,520,402,390,623,507
83,514,383,674,628,832,584,723,639,307,144,569,191,199,990,718,76,415,193,756
308,267,503,448,842,228,55,371,63,724,590,894,566,621,477,448,566,752,530,675
569,327,379,64,84,51,273,863,721,501,592,688,406,814,498,292,505,487,901,398
830,855,66,534,710,441,623,816,324,368,318,450,232,407,619,639,50,530,413,428
477,579,480,721,16,713,673,53,687,256,624,444,534,725,653,227,76,750,411,104
720,405,915,676,80,623,757,141,901,847,493,485,123,379,444,349,832,711,77,73
528,50,913,528,50,61,354,489,838,600,193,387,382,621,191,822,119,82,211,836
516,565,76,906,586,638,755,716,588,822,502,15,492,896,391,503,685,382,188,940
318,583,260,677,873,822,364,255,624,624,147,721,63,218,215,567,136,493,87,511
58,446,210,582,133,814,719,476,412,863,492,901,580,845,87,179,629,177,915,322
112,712,386,667,624,369,650,317,177,230,532,56,360,482,479,818,893,835,258,495
51,492,763,777,287,508,827,138,620,635,915,311,808,510,82,360,564,251,446,138
198,678,855,410,568,899,712,442,173,438,18,86,777,83,752,144,207,721,587,590
79,352,414,777,786,384,671,346,600,591,829,316,374,939,229,199,216,219,109,210
508,154,508,677,510,212,529,903,758,410,199,131,88,476,481,508,825,393,784,287
55,652,686,521,620,388,785,907,535,683,631,363,186,487,289,406,481,572,360,574
133,507,413,366,838,939,131,589,678,440,520,912,142,533,909,17,588,753,833,657
111,356,638,172,491,684,110,846,384,433,679,647,289,846,916,946,352,196,786,479
567,224,907,310,488,763,195,139,595,512,687,680,493,401,130,749,273,353,498,679
814,988,813,443,662,948,136,503,517,392,815,317,723,416,400,318,627,780,836,890
328,196,856,288,623,257,525,285,679,512,564,170,723,534,916,179,67,651,557,834
450,484,381,441,774,570,664,815,147,319,173,316,137,532,327,657,749,510,200,199
902,650,327,258,647,667,250,77,579,598,723,473,510,364,923,369,493,357,318,359
314,824,689,488,809,892,530,384,809,399,497,413,935,133,641,360,996,643,615,215
365,756,249,585,913,613,821,353,646,851,411,54,710,153,289,199,617,200,383,528
824,257,831,938,481,841,576,179,321,938,919,81,479,766,705,257,513,112,353,200
71,748,208,75,250,319,498,179,997,394,409,764,377,678,534,320,113,512,669,115
260,587,57,671,366,248,175,727,814,596,309,664,757,781,940,62,187,809,642,816
406,205,569,108,817,414,366,474,86,311,901,682,589,256,283,814,393,596,856,321
920,613,190,616,616,614,897,384,213,222,158,359,80,837,140,709,854,356,838,820
496,508,512,509,675,180,89,182,838,208,392,771,177,934,88,513,61,943,903,666
311,509,478,439,890,692,682,361,914,802,720,600,675,576,664,224,522,252,898,650
384,587,250,396,715,840,595,679,900,135,141,438,844,122,310,566,850,291,205,676
414,810,370,856,785,628,172,781,88,450,675,339,308,210,832,660,595,77,475,438
648,77,650,473,817,977,819,524,846,751,323,823,104,67,112,451,394,627,725,578
620,327,676,194,664,51,140,368,782,528,111,280,145,73,582,486,821,530,372,764
370,364,79,252,718,204,370,835,825,904,698,312,137,586,387,360,713,229,725,318
192,776,258,495,644,836,759,139,147,702,658,779,615,816,478,812,657,712,362,839
386,530,409,863,110,290,938,75,406,457,190,513,368,133,203,753,514,825,644,687
615,862,808,721,579,671,807,228,848,307,760,74,451,509,484,403,216,710,892,667
494,763,935,569,778,396,362,723,113,495,897,486,735,495,288,715,937,590,575,687
478,108,831,687,817,753,687,528,321,201,574,572,444,897,489,568,876,512,508,288
829,312,350,843,478,252,584,361,625,307,929,570,514,72,219,825,686,570,655,667
172,910,658,834,657,182,941,182,246,907,373,147,271,183,77,785,429,320,847,726
855,523,364,690,564,979,145,892,354,525,326,510,723,856,560,445,620,813,525,618
748,916,783,872,646,676,596,691,316,373,512,915,512,724,831,723,842,324,908,899
88,766,253,446,940,533,398,506,751,912,18,130,134,477,855,59,516,395,384,76
795,638,507,766,947,400,474,505,864,612,653,212,529,378,255,911,358,83,51,491
765,827,584,196,941,390,402,713,901,82,659,623,345,177,309,191,198,223,863,937
517,635,510,64,415,765,901,365,723,574,623,850,191,917,943,643,413,523,757,830
218,854,225,812,523,170,593,357,761,291,621,914,584,60,64,198,65,501,848,939
83,683,222,750,135,847,864,325,80,668,717,630,757,427,137,430,176,777,809,670
285,570,846,471,759,572,627,721,753,68,414,920,521,677,517,713,901,475,569,67
135,319,70,328,495,618,78,479,414,782,107,64,489,378,227,444,861,314,441,179
766,557,983,577,443,573,225,74,259,680,659,932,916,452,560,763,374,214,446,644
54,509,310,204,580,484,889,371,935,619,520,188,622,653,491,229,66,926,828,688
560,311,146,911,907,864,696,386,328,776,393,387,947,364,212,688,677,446,361,64
79,259,476,926,67,375,59,397,750,909,652,947,568,786,208,107,200,830,527,173
88,836,587,134,901,176,896,287,68,574,903,562,82,877,75,651,561,813,569,209
480,566,575,639,198,80,935,142,446,948,784,360,716,835,318,171,536,112,843,835
519,711,796,581,590,382,317,514,389,488,410,663,840,578,450,906,947,841,507,663
87,569,890,497,368,361,58,413,145,592,144,221,292,445,72,941,791,60,218,814
389,810,597,156,190,186,507,725,638,349,86,405,644,947,254,201,391,573,758,826
500,829,643,858,193,191,485,584,370,940,841,820,832,327,565,389,816,677,819,485
81,499,388,812,201,848,823,288,715,650,868,307,322,53,780,360,650,491,711,896
816,443,408,197,316,936,487,792,350,492,287,717,350,201,444,111,579,73,409,289
509,369,890,671,529,444,428,783,170,139,889,649,905,511,508,410,380,399,203,310
808,711,711,500,512,227,85,810,940,604,786,74,667,684,400,174,480,224,522,88
105,897,172,780,432,722,287,529,824,524,52,250,136,219,223,249,384,938,559,856
827,618,152,625,143,933,529,254,400,257,780,144,182,599,576,376,113,199,353,624
515,253,651,450,659,382,814,254,522,535,906,180,350,358,725,441,135,591,921,54
290,354,895,627,399,945,355,311,385,496,756,986,893,80,52,308,414,313,572,599
211,363,501,189,635,891,131,623,493,109,854,572,190,246,206,562,808,365,889,250
63,792,659,400,946,835,724,641,566,180,824,892,199,528,175,563,687,571,314,557";
        #endregion

        #region Day 17

        #endregion

        #region Day 18

        #endregion

        #region Day 19

        #endregion

        #region Day 20

        #endregion

        #region Day 21

        #endregion

        #region Day 22

        #endregion

        #region Day 23

        #endregion

        #region Day 24

        #endregion

        #region Day 25

        #endregion
    }
}
