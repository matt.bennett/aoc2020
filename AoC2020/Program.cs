﻿using AoC2020.Days;
using System;

namespace AoC2020
{
    class Program
    {
        static void Main(string[] args)
        {
            string selection = string.Empty;

            ShowMenu();

            while (selection.ToUpper() != "X")
            {
                Console.Write("Enter a day-challenge, or command:");
                selection = Console.ReadLine();

                switch (selection.Trim().ToUpper())
                {
                    case "1-1":
                        Console.WriteLine($"1-1: {Day01.Part1()}");
                        break;
                    case "1-2":
                        Console.WriteLine($"1-2: {Day01.Part2()}");
                        break;
                    case "2-1":
                        Console.WriteLine($"2-1: {Day02.Part1()}");
                        break;
                    case "2-2":
                        Console.WriteLine($"2-2: {Day02.Part2()}");
                        break;
                    case "3-1":
                        Console.WriteLine($"3-1: {Day03.Part1()}");
                        break;
                    case "3-2":
                        Console.WriteLine($"3-2: {Day03.Part2()}");
                        break;
                    case "4-1":
                        Console.WriteLine($"4-1: {Day04.Part1()}");
                        break;
                    case "4-2":
                        Console.WriteLine($"4-2: {Day04.Part2()}");
                        break;
                    case "5-1":
                        Console.WriteLine($"5-1: {Day05.Part1()}");
                        break;
                    case "5-2":
                        Console.WriteLine($"5-2: {Day05.Part2()}");
                        break;
                    case "6-1":
                        Console.WriteLine($"6-1: {Day06.Part1()}");
                        break;
                    case "6-2":
                        Console.WriteLine($"6-2: {Day06.Part2()}");
                        break;
                    case "7-1":
                        Console.WriteLine($"7-1: {Day07.Part1()}");
                        break;
                    case "7-2":
                        Console.WriteLine($"7-2: {Day07.Part2()}");
                        break;
                    case "8-1":
                        Console.WriteLine($"8-1: {Day08.Part1()}");
                        break;
                    case "8-2":
                        Console.WriteLine($"8-2: {Day08.Part2()}");
                        break;
                    case "9-1":
                        Console.WriteLine($"9-1: {Day09.Part1()}");
                        break;
                    case "9-2":
                        Console.WriteLine($"9-2: {Day09.Part2()}");
                        break;
                    case "10-1":
                        Console.WriteLine($"10-1: {Day10.Part1()}");
                        break;
                    case "10-2":
                        Console.WriteLine($"10-2: {Day10.Part2()}");
                        break;
                    case "11-1":
                        Console.WriteLine($"11-1: {Day11.Part1()}");
                        break;
                    case "11-2":
                        Console.WriteLine($"11-2: {Day11.Part2()}");
                        break;
                    case "12-1":
                        Console.WriteLine($"12-1: {Day12.Part1()}");
                        break;
                    case "12-2":
                        Console.WriteLine($"12-2: {Day12.Part2()}");
                        break;
                    case "13-1":
                        Console.WriteLine($"13-1: {Day13.Part1()}");
                        break;
                    case "13-2":
                        Console.WriteLine($"13-2: {Day13.Part2()}");
                        break;
                    case "14-1":
                        Console.WriteLine($"14-1: {Day14.Part1()}");
                        break;
                    case "14-2":
                        Console.WriteLine($"14-2: {Day14.Part2()}");
                        break;
                    case "15-1":
                        Console.WriteLine($"15-1: {Day15.Part1()}");
                        break;
                    case "15-2":
                        Console.WriteLine($"15-2: {Day15.Part2()}");
                        break;
                    case "16-1":
                        Console.WriteLine($"16-1: {Day16.Part1()}");
                        break;
                    case "16-2":
                        Console.WriteLine($"16-2: {Day16.Part2()}");
                        break;
                    case "17-1":
                        Console.WriteLine($"17-1: {Day17.Part1()}");
                        break;
                    case "17-2":
                        Console.WriteLine($"17-2: {Day17.Part2()}");
                        break;
                    case "18-1":
                        Console.WriteLine($"18-1: {Day18.Part1()}");
                        break;
                    case "18-2":
                        Console.WriteLine($"18-2: {Day18.Part2()}");
                        break;
                    case "19-1":
                        Console.WriteLine($"19-1: {Day19.Part1()}");
                        break;
                    case "19-2":
                        Console.WriteLine($"19-2: {Day19.Part2()}");
                        break;
                    case "20-1":
                        Console.WriteLine($"20-1: {Day20.Part1()}");
                        break;
                    case "20-2":
                        Console.WriteLine($"20-2: {Day20.Part2()}");
                        break;
                    case "21-1":
                        Console.WriteLine($"21-1: {Day21.Part1()}");
                        break;
                    case "21-2":
                        Console.WriteLine($"21-2: {Day21.Part2()}");
                        break;
                    case "22-1":
                        Console.WriteLine($"22-1: {Day22.Part1()}");
                        break;
                    case "22-2":
                        Console.WriteLine($"22-2: {Day22.Part2()}");
                        break;
                    case "23-1":
                        Console.WriteLine($"23-1: {Day23.Part1()}");
                        break;
                    case "23-2":
                        Console.WriteLine($"23-2: {Day23.Part2()}");
                        break;
                    case "24-1":
                        Console.WriteLine($"24-1: {Day24.Part1()}");
                        break;
                    case "24-2":
                        Console.WriteLine($"24-2: {Day24.Part2()}");
                        break;
                    case "25-1":
                        Console.WriteLine($"25-1: {Day25.Part1()}");
                        break;
                    case "25-2":
                        Console.WriteLine($"25-2: {Day25.Part2()}");
                        break;
                    case "M":
                        ShowMenu();
                        break;
                }
            }
        }

        static void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("#-#: Day and Challenge #");
            Console.WriteLine("M: Redraw Menu");
            Console.WriteLine("X: Exit");
        }        
    }
}
