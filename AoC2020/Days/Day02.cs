﻿using System.Linq;

namespace AoC2020.Days
{
    public class Day02
    {
        public static string Part1()
        {
            //// solution during contest
            //var strings = Util.SplitStringOnNewLine(Data.Day2);
            //var passwords = new List<Password>();

            //foreach (var s in strings)
            //{
            //    passwords.Add(new Password(s));
            //}

            //return passwords.Where(x => x.IsValid).Count().ToString();

            // Trying to reduce lines of code after the fact.
            int count = 0;
            Util.SplitStringOnNewLine(Data.Day2).ForEach(s => count += new Password(s).IsValid ? 1 : 0);
            return count.ToString();
        }

        public static string Part2()
        {
            // solution during contest
            //var strings = Util.SplitStringOnNewLine(Data.Day2);

            //var passwords = new List<Password>();

            //foreach (var s in strings)
            //{
            //    passwords.Add(new Password(s));
            //}

            //return passwords.Where(x => x.IsValidPart2).Count().ToString();

            // Trying to reduce lines of code after the fact.
            int count = 0;
            Util.SplitStringOnNewLine(Data.Day2).ForEach(s => count += new Password(s).IsValidPart2 ? 1 : 0);
            return count.ToString();
        }

        public class Password
        {
            public Password(string initString)
            {
                var strings = Util.SplitStringOnSpace(initString);
                LowCount = int.Parse(strings[0].Substring(0, strings[0].IndexOf("-")));
                HighCount = int.Parse(strings[0].Substring(strings[0].IndexOf("-") + 1));
                TargetLetter = strings[1].Replace(":", string.Empty).Trim();
                PasswordText = strings[2].Trim();
            }
            public int LowCount { get; set; }
            public int HighCount { get; set; }
            public string TargetLetter { get; set; }
            public string PasswordText { get; set; }
            public bool IsValid
            {
                get
                {
                    int count = PasswordText.Count(x => x == char.Parse(TargetLetter));

                    return count >= LowCount && count <= HighCount;
                }
            }
            public bool IsValidPart2
            {
                get
                {
                    return PasswordText.Substring(LowCount - 1, 1) == TargetLetter ^ PasswordText.Substring(HighCount - 1, 1) == TargetLetter;
                }
            }
        }
    }
}
