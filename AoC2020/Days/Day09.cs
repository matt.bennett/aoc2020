﻿using System.Collections.Generic;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day09
    {
        public static string Part1()
        {
            var preambleLength = 25;

            var numbers = Util.SplitStringOnNewLine(Data.Day9).Select(x => long.Parse(x)).ToList();

            var currPos = preambleLength;

            for (int i = preambleLength; i < numbers.Count; i++)
            {
                var start = i - preambleLength;
                if (!CheckValue(numbers.GetRange(start, preambleLength), numbers[i]))
                {
                    return numbers[i].ToString();
                }
            }                      

            return "broken";
        }

        public static string Part2()
        {
            long target = 50047984;
            var numbers = Util.SplitStringOnNewLine(Data.Day9).Select(x => long.Parse(x)).ToList();
           
            for (int i = 0; i < numbers.Count; i++)
            {
                long sum = 0;
                var nums = new List<long>();
                var index = i;
                while (sum < target)
                {
                    nums.Add(numbers[index]);
                    sum += numbers[index];
                    index += 1;
                }
                if (sum == target)
                {
                    return (nums.Min() + nums.Max()).ToString();
                }
            }

            return "broken";
        }

        public static bool CheckValue(List<long> numbers, long checkVal)
        {
            numbers.Sort();
            var lptr = 0;
            var rptr = numbers.Count - 1;

            while (lptr < rptr)
            {
                if (numbers[lptr] + numbers[rptr] == checkVal)
                {
                    return true;
                }
                else if (numbers[lptr] + numbers[rptr] < checkVal)
                {
                    lptr += 1;
                }
                else
                {
                    rptr -= 1;
                }
            }

            return false;
        }
    }
}
