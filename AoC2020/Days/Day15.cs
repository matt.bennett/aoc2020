﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day15
    {
        public static string Part1()
        {
            return PlayGame(2020);
        }

        public static string Part2()
        {
            return PlayGame(30000000);
        }

        public static string PlayGame(long numberOfTurns)
        {
            var inputs = Util.SplitStringOnComma<int>(Data.Day15);
            var game = new List<long>();
            var inputCounter = 0;
            var loopCounter = 0;
            long currSpoken = 0;
            long lastNumberSpoken = -1;
            var log = new Dictionary<long, (long, long)>();

            while (loopCounter < numberOfTurns)
            {
                if (log.ContainsKey(lastNumberSpoken) && log[lastNumberSpoken].Item2 != -1)
                {
                    currSpoken = log[lastNumberSpoken].Item1 - log[lastNumberSpoken].Item2;
                }
                else
                {
                    if (inputCounter > inputs.Count() - 1)
                    {
                        currSpoken = 0;
                    }
                    else
                    {
                        currSpoken = inputs[inputCounter];
                        inputCounter += 1;
                    }
                }

                if (log.ContainsKey(currSpoken))
                {
                    (long, long) newValue = (loopCounter, log[currSpoken].Item1);
                    log[currSpoken] = newValue;
                }
                else
                {
                    log.Add(currSpoken, (loopCounter, -1));
                }

                game.Add(currSpoken);
                lastNumberSpoken = currSpoken;
                loopCounter += 1;
            }

            var answer = game.Last();

            return answer.ToString();
        }
    }
}
