﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day10
    {
        public static List<long> adapters;
        public static Graph myGraph;
        private static Dictionary<string, long> cache;

        public static string Part1()
        {
            adapters = Util.SplitStringOnNewLine(Data.Day10).Select(x => long.Parse(x)).ToList();

            adapters.Add(0);
            adapters.Add(adapters.Max() + 3);

            adapters = adapters.OrderBy(a => a).ToList();

            int oneCount = 0;
            int threeCount = 0;

            for (int i = 1; i < adapters.Count; i++)
            {
                oneCount += adapters[i] - adapters[i - 1] == 1 ? 1 : 0;
                threeCount += adapters[i] - adapters[i - 1] == 3 ? 1 : 0;
            }

            var answer = oneCount * threeCount;

            return answer.ToString();
        }

        public static string Part2()
        {
            adapters = Util.SplitStringOnNewLine(Data.Day10).Select(x => long.Parse(x)).ToList();
            adapters.Add(0);
            adapters.Add(adapters.Max() + 3);
            adapters = adapters.OrderBy(a => a).ToList();

            // Build Graph
            myGraph = new Graph();

            foreach (var v in adapters)
            {
                myGraph.AddVertex(new Vertex { Value = v });
            }

            foreach (var v in adapters)
            {
                var validChoices = adapters.Where(a => a - v <= 3 && a > v).ToList();
                foreach (var c in validChoices)
                {
                    myGraph.AddEdge(v, c);
                }
            }

            // search graph
            cache = new Dictionary<string, long>();
            Stopwatch s = new Stopwatch();
            s.Start();
            var r = PathCount(adapters.Min(), adapters.Max());
            s.Stop();
            Debug.WriteLine($"Elapsed Milliseconds: {s.ElapsedMilliseconds}");
            Debug.WriteLine($"Elapsed Ticks: {s.ElapsedTicks}");
            return r.ToString();
        }       
            
        private static long PathCount(long start, long finish)
        {
            long count = 0;

            if (start == finish)
            {
                return 1;
            }
            else
            {
                foreach (var e in myGraph.AdjacencyList.Find(x => x.Value == start).Edges)
                {
                    if (cache.ContainsKey($"{e}"))
                    {
                        count += cache[$"{e}"];
                    }
                    else
                    {
                        var value = PathCount(e, finish);
                        cache.Add($"{e}", value);
                        count += value;
                    }
                }
            }

            return count;
        }

        public class Vertex
        {
            public Vertex()
            {
                Edges = new List<long>();
            }

            public long Value { get; set; }

            public List<long> Edges { get; set; }
        }

        public class Graph
        {
            public Graph()
            {
                AdjacencyList = new List<Vertex>();
            }

            public List<Vertex> AdjacencyList { get; set; }

            public void AddVertex(Vertex v)
            {
                if (!AdjacencyList.Any(x => x.Value == v.Value))
                {
                    AdjacencyList.Add(v);
                }
            }

            public void AddEdge(long v1, long v2)
            {
                AdjacencyList.Find(x => x.Value == v1).Edges.Add(v2);
                //AdjacencyList.Find(x => x.Value == v2).Edges.Add(v1);
            }
        }
    }   
}
