﻿using System.Collections.Generic;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day16
    {
        public static string Part1()
        {
            LoadInput(out List<Rule> rules, out Ticket myTicket, out List<Ticket> nearbyTickets);

            long sum = 0;

            foreach (var t in nearbyTickets)
            {
                foreach (var v in t.Values)
                {
                    if (!rules.Any(r => r.Validate(v)))
                    {
                        sum += v;
                    }
                }
            }

            // condensed the lines above, for the fun of it.
            //nearbyTickets.ForEach(t => t.Values.ForEach(v => { sum += !rules.Any(r => r.Validate(v)) ? v : 0; }));

            return sum.ToString();
        }

        public static string Part2()
        {
            LoadInput(out List<Rule> rules, out Ticket myTicket, out List<Ticket> nearbyTickets);
            var validNearbyTickets = new List<Ticket>();
           
            foreach (var t in nearbyTickets)
            {
                var isValid = true;
                foreach (var v in t.Values)
                {
                    if (!rules.Any(r => r.Validate(v)))
                    {
                        isValid = false;
                    }
                }

                if (isValid)
                {
                    validNearbyTickets.Add(t);
                }
            }

            // part 2
            var fieldList = new List<string>();
            var ticketFieldValues = new Dictionary<int, List<long>>();
            // flatten field values
            foreach (var t in validNearbyTickets)
            {
                for (int i = 0; i < t.Values.Count; i++)
                {
                    if (ticketFieldValues.ContainsKey(i))
                    {
                        ticketFieldValues[i].Add(t.Values[i]);
                    }
                    else
                    {
                        ticketFieldValues.Add(i, new List<long> { t.Values[i] });
                    }
                }
            }

            // see which fields could fit each set of data
            var possibleFields = new Dictionary<int, List<string>>();

            foreach (var optionSet in ticketFieldValues)
            {
                foreach (var r in rules)
                {
                    if (optionSet.Value.All(o => r.Validate(o)))
                    {
                        if (possibleFields.ContainsKey(optionSet.Key))
                        {
                            possibleFields[optionSet.Key].Add(r.Name);
                        }
                        else
                        {
                            possibleFields.Add(optionSet.Key, new List<string> { r.Name });
                        }
                    }
                }
            }

            // de-dupe
            while (possibleFields.Any(p => p.Value.Count > 1))
            {
                var singleFields = possibleFields.Where(p => p.Value.Count == 1).Select(s => s.Value.First()).ToList();

                foreach (var singleField in singleFields)
                {
                    possibleFields.Where(p => p.Value.Count > 1 && p.Value.Contains(singleField)).ToList().ForEach(x => x.Value.Remove(singleField));
                }
            }

            // map to ticket
            foreach (var p in possibleFields)
            {
                myTicket.Fields.Add((p.Value.First(), myTicket.Values[p.Key]));
            }

            var departureFields = myTicket.Fields.Where(f => f.Item1.StartsWith("departure")).Select(s => s.Item2).ToList();
            long answer = departureFields.Aggregate((x, y) => x * y);
           
            return answer.ToString();
        }

        public static void LoadInput(out List<Rule> rules, out Ticket myTicket, out List<Ticket> nearbyTickets)
        {
            var inputData = Util.SplitStringOnNewLine(Data.Day16);

            rules = new List<Rule>();
            myTicket = new Ticket();
            nearbyTickets = new List<Ticket>();
            var mode = "rules";

            foreach (var line in inputData)
            {
                if (line == "your ticket:")
                {
                    mode = "myTicket";
                }
                else if (line == "nearby tickets:")
                {
                    mode = "nearbyTickets";
                }
                else
                {
                    switch (mode)
                    {
                        case "rules":
                            var ruleArr = line.Split(":");
                            var ruleText = ruleArr[0];
                            var ruleValues = ruleArr[1].Split("or");
                            var newRule = new Rule();
                            newRule.Name = ruleText;
                            foreach (var rule in ruleValues)
                            {
                                var range = rule.Split("-");
                                newRule.ValidRanges.Add((long.Parse(range[0]), long.Parse(range[1])));
                            }
                            rules.Add(newRule);
                            break;
                        case "myTicket":                            
                            var myVal = line.Split(",").ToList().Select(x => long.Parse(x)).ToList();
                            myTicket.Values.AddRange(myVal);
                            break;
                        case "nearbyTickets":
                            var tixVal = line.Split(",").ToList().Select(x => long.Parse(x)).ToList();
                            nearbyTickets.Add(new Ticket { Values = tixVal });                            
                            break;
                    }
                }
            }
        }

        public class Rule
        {
            public Rule()
            {
                ValidRanges = new List<(long, long)>();
            }

            public string Name { get; set; }

            public List<(long,long)> ValidRanges { get; set; }

            public bool Validate(long input)
            {
                bool returnValue = ValidRanges.Any(x => x.Item1 <= input && x.Item2 >= input);
                return returnValue;
            }
        }

        public class Ticket
        {
            public Ticket()
            {
                Values = new List<long>();
                Fields = new List<(string, long)>();
            }
                        
            public List<long> Values { get; set; }

            public List<(string,long)> Fields { get; set; }
        }
    }
}
