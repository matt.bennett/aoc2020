﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace AoC2020.Days
{
    public static class Day07
    {
        public static List<string> processedColors;
        public static List<string> foundColors;
        public static int bagSum;
        public static string Part1()
        {
            var bagData = Util.SplitStringOnNewLine(Data.Day7);
            var bagRules = new List<BagRule>();

            foreach (var data in bagData)
            {
                bagRules.Add(new BagRule(data));
            }

            processedColors = new List<string>();
            foundColors = new List<string>();
            var sum = FindBagContainerOptions("shiny gold", bagRules);
            var x = foundColors.Distinct().Count();
            return x.ToString(); // WHY?
        }


        public static string Part2()
        {
            var bagData = Util.SplitStringOnNewLine(Data.Day7);
            var bagRules = new List<BagRule>();

            foreach (var data in bagData)
            {
                bagRules.Add(new BagRule(data));
            }

            bagSum = 0;
            var sum = FindBagChildrenOptions("shiny gold", bagRules);
            return sum.ToString();
        }

        public class BagRule
        {
            public BagRule(string ruleData)
            {
                Contents = new Dictionary<string, int>();
                ParseRuleData(ruleData);
            }

            public string Color { get; set; }

            public Dictionary<string, int> Contents { get; set; }

            private void ParseRuleData(string ruleData)
            {
                Color = ruleData.Substring(0, ruleData.IndexOf("contain")).Replace("bags", string.Empty).Trim();
                var colors = ruleData.Substring(ruleData.IndexOf("contain")).Replace("contain", string.Empty).Replace("bags", string.Empty).Replace("bag", string.Empty).Replace(".", string.Empty).Trim();
                var colorList = Util.SplitStringOnComma(colors);
                foreach (var c in colorList)
                {
                    if (c != "no other")
                    {
                        var parts = Util.SplitStringOnSpace(c);
                        var num = int.Parse(parts[0]);
                        var desc = c.Replace(parts[0], string.Empty).Trim();

                        Contents.Add(desc, num);
                    }
                }
            }
        }

        public static int FindBagContainerOptions(string color, List<BagRule> bagRules)
        {
            var bagHolders = bagRules.Where(x => x.Contents.ContainsKey(color) && !processedColors.Contains(x.Color)).ToList();

            foundColors.AddRange(bagHolders.Select(x => x.Color).ToList());
            var sum = 0;
            
            foreach (var holder in bagHolders)
            {
                sum += 1;
                processedColors.Add(holder.Color);
                FindBagContainerOptions(holder.Color, bagRules);
            }

            return sum;
        }

        public static int FindBagChildrenOptions(string color, List<BagRule> bagRules)
        {
            var bagChildren = bagRules.Where(x => x.Color == color).ToList();
            var sum = 0;

            foreach (var holder in bagChildren)
            {
                foreach (var subBag in holder.Contents)
                {
                    sum += subBag.Value + subBag.Value * FindBagChildrenOptions(subBag.Key, bagRules);
                }
            }

            return sum;
        }
    }
}
