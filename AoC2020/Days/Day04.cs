﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AoC2020.Days
{
    public static class Day04
    {
        public static string Part1()
        {
            var strings = Util.SplitStringOnNewLine(Data.Day4, StringSplitOptions.None);

            var currPassport = string.Empty;
            var passports = new List<Passport>();

            foreach (var s in strings)
            {
                if (s == string.Empty)
                {
                    passports.Add(new Passport(currPassport));
                    currPassport = string.Empty;
                }
                else
                {
                    currPassport = $"{currPassport} {s}";
                }
            }
            passports.Add(new Passport(currPassport));

            var x = passports.Count(x => x.IsValid);
            return x.ToString();
        }

        public static string Part2()
        {
            var strings = Util.SplitStringOnNewLine(Data.Day4, StringSplitOptions.None);

            var currPassport = string.Empty;
            var passports = new List<Passport>();

            foreach (var s in strings)
            {
                if (s == string.Empty)
                {
                    passports.Add(new Passport(currPassport));
                    currPassport = string.Empty;
                }
                else
                {
                    currPassport = $"{currPassport} {s}";
                }
            }
            passports.Add(new Passport(currPassport));

            var x = passports.Count(x => x.IsValidPart2);
            return x.ToString();
        }

        public class Passport
        {
            public Passport(string passportDescription)
            {
                var parts = Util.SplitStringOnSpace(passportDescription);

                foreach (var part in parts)
                {
                    var pieces = part.Split(":");

                    switch (pieces[0])
                    {
                        case "byr":
                            byr = pieces[1];
                            break;
                        case "iyr":
                            iyr = pieces[1];
                            break;
                        case "eyr":
                            eyr = pieces[1];
                            break;
                        case "hgt":
                            hgt = pieces[1];
                            break;
                        case "hcl":
                            hcl = pieces[1];
                            break;
                        case "ecl":
                            ecl = pieces[1];
                            break;
                        case "pid":
                            pid = pieces[1];
                            break;
                        case "cid":
                            cid = pieces[1];
                            break;
                    }
                }
            }

            public string byr { get; set; }
            public string iyr { get; set; }
            public string eyr { get; set; }
            public string hgt { get; set; }
            public string hcl { get; set; }
            public string ecl { get; set; }
            public string pid { get; set; }
            public string cid { get; set; }

            public bool IsValid
            {
                get
                {
                    return !string.IsNullOrEmpty(byr) 
                        && !string.IsNullOrEmpty(iyr) 
                        && !string.IsNullOrEmpty(eyr) 
                        && !string.IsNullOrEmpty(hgt) 
                        && !string.IsNullOrEmpty(hcl) 
                        && !string.IsNullOrEmpty(ecl)
                        && !string.IsNullOrEmpty(pid);
                }
            }

            public bool IsValidPart2
            {
                get
                {
                    bool requiredFields = !string.IsNullOrEmpty(byr) && !string.IsNullOrEmpty(iyr) && !string.IsNullOrEmpty(eyr) && !string.IsNullOrEmpty(hgt) && !string.IsNullOrEmpty(hcl) && !string.IsNullOrEmpty(ecl)
                        && !string.IsNullOrEmpty(pid);

                    // short circuit to reduce need for more intense checks below
                    if (!requiredFields) return requiredFields;

                    bool byrValid = byr.Length == 4 && int.TryParse(byr, out int a) && a >= 1920 && a <= 2002;
                    bool iyrValid = iyr.Length == 4 && int.TryParse(iyr, out int b) && b >= 2010 && b <= 2020;
                    bool eyrValid = eyr.Length == 4 && int.TryParse(eyr, out int c) && c >= 2020 && c <= 2030;

                    bool hgtValid = false;
                    if (hgt.Contains("cm"))
                    {
                        var x = hgt.Replace("cm", string.Empty);
                        hgtValid = int.TryParse(x, out int d) && d >= 150 && d <= 193;
                    }
                    else if (hgt.Contains("in"))
                    {
                        var x = hgt.Replace("in", string.Empty);
                        hgtValid = int.TryParse(x, out int e) && e >= 59 && e <= 76;
                    }

                    bool hclValid = Regex.Match(hcl, @"^#(?:[0-9a-f]{3}){2}$").Success;

                    bool eclValid = new string[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" }.Contains(ecl);

                    bool pidValid = Regex.Match(pid, @"^\d{9}$").Success;

                    return requiredFields && byrValid && iyrValid && eyrValid && hgtValid && hclValid && eclValid && pidValid;
                }
            }
        }
    }
}
