﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace AoC2020.Days
{
    public static class Day11
    {
        public static string Part1()
        {
            var data = Util.SplitStringOnNewLine(Data.Day11);
            var rowData = new char[data[0].Length, data.Count];

            for (int i = 0; i < data.Count; i++)
            {
                var charArr = data[i].ToCharArray();
                for (int c = 0; c < charArr.Length; c++)
                {
                    rowData[c, i] = charArr[c];
                }
            }

            var changeCount = -1;
            var numRows = data.Count;
            var seatsInRow = data[0].Length;
            var adjacentSeats = new List<char>();

            while (changeCount != 0)
            {
                var newArrangement = new char[seatsInRow, numRows];
                changeCount = 0;
                for (int r = 0; r < numRows; r++)
                {
                    for (int s = 0; s < seatsInRow; s++)
                    {
                        switch (rowData[s,r])
                        {
                            case '.':
                                newArrangement[s, r] = '.';
                                break;
                            case 'L':
                                adjacentSeats = GetAdjacentSeats(s, r, rowData);
                                if (!adjacentSeats.Contains('#'))
                                {
                                    newArrangement[s, r] = '#';
                                    changeCount += 1;
                                }
                                else
                                {
                                    newArrangement[s, r] = 'L';
                                }
                                break;
                            case '#':
                                adjacentSeats = GetAdjacentSeats(s, r, rowData);
                                if (adjacentSeats.Count(x => x == '#') >= 4)
                                {
                                    newArrangement[s, r] = 'L';
                                    changeCount += 1;
                                }
                                else
                                {
                                    newArrangement[s, r] = '#';
                                }
                                break;
                        }
                    }
                }

                rowData = newArrangement;
            }

            var answer = rowData.Cast<char>().ToList().Count(x => x == '#');

            return answer.ToString();
        }

        public static List<char> GetAdjacentSeats(int seat, int row, char[,] rowData)
        {
            var returnValue = new List<Char>();

            if (seat > 0 && row > 0)
            {
                var topLeft = rowData[seat - 1, row - 1];
                returnValue.Add(topLeft);
            }

            if (row > 0)
            {
                var top = rowData[seat, row - 1];
                returnValue.Add(top);
            }

            if (seat > 0)
            {
                var left = rowData[seat - 1, row];
                returnValue.Add(left);
            }

            if (row < rowData.GetLength(1) - 1)
            {
                var bottom = rowData[seat, row + 1];
                returnValue.Add(bottom);
            }

            if (seat < rowData.GetLength(0) -1)
            {
                var right = rowData[seat + 1, row];
                returnValue.Add(right);
            }

            if (row < rowData.GetLength(1) - 1 && seat < rowData.GetLength(0) - 1)
            {
                var bottomRight = rowData[seat + 1, row + 1];
                returnValue.Add(bottomRight);
            }

            if (seat < rowData.GetLength(0) - 1 && row > 0)
            {
                var topRight = rowData[seat + 1, row - 1];
                returnValue.Add(topRight);
            }

            if (row < rowData.GetLength(1) - 1 && seat > 0)
            {
                var bottomLeft = rowData[seat - 1, row + 1];
                returnValue.Add(bottomLeft);
            }       
            
            return returnValue;
        }

        public static string Part2()
        {
            var data = Util.SplitStringOnNewLine(Data.Day11);
            var rowData = new char[data[0].Length, data.Count];

            for (int i = 0; i < data.Count; i++)
            {
                var charArr = data[i].ToCharArray();
                for (int c = 0; c < charArr.Length; c++)
                {
                    rowData[c, i] = charArr[c];
                }
            }

            var changeCount = -1;
            var numRows = data.Count;
            var seatsInRow = data[0].Length;
            var adjacentSeats = new List<char>();

            while (changeCount != 0)
            {
                var newArrangement = new char[seatsInRow, numRows];
                changeCount = 0;
                for (int r = 0; r < numRows; r++)
                {
                    for (int s = 0; s < seatsInRow; s++)
                    {
                        switch (rowData[s, r])
                        {
                            case '.':
                                newArrangement[s, r] = '.';
                                break;
                            case 'L':
                                adjacentSeats = GetAdjacentSeatsPart2(s, r, rowData);
                                if (!adjacentSeats.Contains('#'))
                                {
                                    newArrangement[s, r] = '#';
                                    changeCount += 1;
                                }
                                else
                                {
                                    newArrangement[s, r] = 'L';
                                }
                                break;
                            case '#':
                                adjacentSeats = GetAdjacentSeatsPart2(s, r, rowData);
                                if (adjacentSeats.Count(x => x == '#') >= 5)
                                {
                                    newArrangement[s, r] = 'L';
                                    changeCount += 1;
                                }
                                else
                                {
                                    newArrangement[s, r] = '#';
                                }
                                break;
                        }
                    }
                }

                rowData = newArrangement;
                //PrintRowData(rowData);
            }

            var answer = rowData.Cast<char>().ToList().Count(x => x == '#');

            return answer.ToString();
        }

        public static void PrintRowData(char[,] rowData)
        {
            for (int r = 0; r < rowData.GetLength(1); r++)
            {
                var str = string.Empty;
                for (int s = 0; s < rowData.GetLength(0); s++)
                {
                    str += rowData[s, r];
                }
                Debug.WriteLine(str);
            }

            Debug.WriteLine(string.Empty);
        }

        public static List<char> GetAdjacentSeatsPart2(int seat, int row, char[,] rowData)
        {
            var returnValue = new List<Char>();

            // up
            for (int r = row - 1; r >= 0; r--)
            {
                var d = rowData[seat, r];
                returnValue.Add(d);
                if (d == '#' || d =='L') break;
            }

            // down
            for (int r = row + 1; r < rowData.GetLength(1); r++)
            {
                var d = rowData[seat, r];
                returnValue.Add(d);
                if (d == '#' || d == 'L') break;
            }

            // left
            for (int s = seat - 1; s >= 0; s--)
            {
                var d = rowData[s, row];
                returnValue.Add(d);
                if (d == '#' || d == 'L') break;
            }

            // right
            for (int s = seat + 1; s < rowData.GetLength(0); s++)
            {
                var d = rowData[s, row];
                returnValue.Add(d);
                if (d == '#' || d == 'L') break;
            }

            // up left
            var currS = seat - 1;
            var currR = row - 1;
            while (currS >= 0 && currR >= 0)
            {
                var d = rowData[currS, currR];
                returnValue.Add(d);
                if (d == '#' || d == 'L') break;
                currS -= 1;
                currR -= 1;
            }

            // up right
            currS = seat + 1;
            currR = row - 1;
            while (currS < rowData.GetLength(0) && currR >= 0)
            {
                var d = rowData[currS, currR];
                returnValue.Add(d);
                if (d == '#' || d == 'L') break;
                currS += 1;
                currR -= 1;
            }

            // down left
            currS = seat - 1;
            currR = row + 1;
            while (currS >= 0 && currR < rowData.GetLength(1))
            {
                var d = rowData[currS, currR];
                returnValue.Add(d);
                if (d == '#' || d == 'L') break;
                currS -= 1;
                currR += 1;
            }

            // down right
            currS = seat + 1;
            currR = row + 1;
            while (currS < rowData.GetLength(0) && currR < rowData.GetLength(1))
            {
                var d = rowData[currS, currR];
                returnValue.Add(d);
                if (d == '#' || d == 'L') break;
                currS += 1;
                currR += 1;
            }

            return returnValue;
        }
    }
}
