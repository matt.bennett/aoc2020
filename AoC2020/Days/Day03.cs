﻿using System.Collections.Generic;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day03
    {
        public static string Part1()
        {
            var lines = Util.SplitStringOnNewLine(Data.Day3);
            var slopeInfo = new List<SlopeLine>();
            
            foreach (var line in lines)
            {
                slopeInfo.Add(new SlopeLine(line));
            }

            var currRight = 0;
            var numTrees = 0;

            foreach (var slope in slopeInfo)
            {
                while (currRight >= slope.ExtendedLine.Count)
                {
                    slope.ExtendedLine.AddRange(slope.SingleLine);
                }

                numTrees += slope.ExtendedLine[currRight] ? 1 : 0;

                currRight += 3;
            }

            return numTrees.ToString();
        }

        public static string Part2()
        {
            var lines = Util.SplitStringOnNewLine(Data.Day3);
            var slopeInfo = new List<SlopeLine>();

            foreach (var line in lines)
            {
                slopeInfo.Add(new SlopeLine(line));
            }

            var right1 = 0;
            var right3 = 0;
            var right5 = 0;
            var right7 = 0;
            var right12 = 0;

            long numTrees1 = 0;
            long numTrees3 = 0;
            long numTrees5 = 0;
            long numTrees7 = 0;
            long numTrees12 = 0;

            var skipThisLine = false;

            foreach (var slope in slopeInfo)
            {
                while (right7 >= slope.ExtendedLine.Count)
                {
                    slope.ExtendedLine.AddRange(slope.SingleLine);
                }

                numTrees1 += slope.ExtendedLine[right1] ? 1 : 0;
                numTrees3 += slope.ExtendedLine[right3] ? 1 : 0;
                numTrees5 += slope.ExtendedLine[right5] ? 1 : 0;
                numTrees7 += slope.ExtendedLine[right7] ? 1 : 0;
                numTrees12 += slope.ExtendedLine[right12] && !skipThisLine ? 1 : 0;

                right1 += 1;
                right3 += 3;
                right5 += 5;
                right7 += 7;
                right12 += !skipThisLine ? 1 : 0;
                skipThisLine = !skipThisLine;
            }
            
            return (numTrees1 * numTrees3 * numTrees5 * numTrees7 * numTrees12).ToString();
        }
        // 294228896 wrong
        // 3818892840 wrong
        // 3919390020 wrong

        public class SlopeLine
        {
            public SlopeLine(string slopeDescription)
            {
                SingleLine = new List<bool>();
                ExtendedLine = new List<bool>();

                var charArr = slopeDescription.ToArray<char>();
                foreach (char c in charArr)
                {
                    var val = c == char.Parse("#") ? true : false;
                    SingleLine.Add(val);
                    ExtendedLine.Add(val);
                }                
            }

            public List<bool> SingleLine { get; set; }

            public List<bool> ExtendedLine { get; set; }


        }
    }
}
