﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day14
    {
        public static string Part1()
        {
            var inputData = Util.SplitStringOnNewLine(Data.Day14);
            var mask = string.Empty;
            var register = new Dictionary<int, MemoryAddress>();

            for (int i = 0; i < inputData.Count; i++)
            {
                var split = inputData[i].Split(" = ");

                if (split[0].Contains("mask"))
                {
                    mask = split[1].Trim();
                }
                else
                {
                    var key = int.Parse(split[0].Replace("mem[", string.Empty).Replace("]", string.Empty));

                    if (!register.ContainsKey(key))
                    {
                        register.Add(key, new MemoryAddress(key, mask));
                    }

                    string valString = Convert.ToString(int.Parse(split[1]), 2);

                    register[key].BitMask = mask;
                    register[key].WriteValue(valString);
                }
            }

            var sum = register.Values.Sum(x => x.ValueAsLong);

            return sum.ToString();
        }

        public static string Part2()
        {
            var inputData = Util.SplitStringOnNewLine(Data.Day14);
            var mask = string.Empty;
            var bank = new MemoryBank();

            for (int i = 0; i < inputData.Count; i++)
            {
                var split = inputData[i].Split(" = ");

                if (split[0].Contains("mask"))
                {
                    mask = split[1].Trim();
                    bank.BitMask = mask;
                }
                else
                {
                    var address = Convert.ToString(int.Parse(split[0].Replace("mem[", string.Empty).Replace("]", string.Empty)), 2);
                    var value = int.Parse(split[1]);

                    bank.UpdateValues(value, address);                    
                }
            }

            var sum = bank.MemoryAddresses.Sum(x => x.Value);

            return sum.ToString();
        }

        public class MemoryAddress
        {
            private List<int> maskOne;
            private List<int> maskZero;
            private string mask;

            public MemoryAddress(int registerNumber, string bitMask)
            {
                Value = "000000000000000000000000000000000000";
                RegisterNumber = registerNumber;
                BitMask = bitMask;
            }

            public int RegisterNumber { get; set; }

            public string BitMask { 
                get 
                {
                    return mask;
                } 
                set 
                {
                    mask = value;
                    UpdateMask();
                } 
            }

            public string Value { get; set; }

            public long ValueAsLong
            {
                get 
                {
                    return Convert.ToInt64(Value, 2);
                }
            }

            public void UpdateMask()
            {
                var maskArr = mask.ToArray();
                maskOne = new List<int>();
                maskZero = new List<int>();

                for (int i = 0; i < maskArr.Length; i++)
                {
                    if (maskArr[i] == '1')
                    {
                        maskOne.Add(i);
                    }
                    else if (maskArr[i] == '0')
                    {
                        maskZero.Add(i);
                    }
                }
            }

            public void WriteValue(string newValue)
            {
                string prepend = string.Empty;
                for(int i = 0; i < 36-newValue.Length; i++)
                {
                    prepend += "0";
                }
                newValue = prepend + newValue;
                var maskedValue = ApplyMask(newValue);
                Value = maskedValue; 
            }

            private string ApplyMask(string newValue)
            {
                var temp = newValue.ToArray();
                foreach (int i in maskOne)
                {
                    temp[i] = '1';
                }
                foreach (int i in maskZero)
                {
                    temp[i] = '0';
                }
                return String.Join("", temp);
            }
        }

        public class MemoryBank
        {
            private List<int> maskOne;
            private List<int> maskX;
            private string mask;
            private List<long> registers;

            public MemoryBank()
            {
                MemoryAddresses = new List<MemoryAddress2>();
                BitMask = "000000000000000000000000000000000000";
            }

            public List<MemoryAddress2> MemoryAddresses { get; set; }

            public string BitMask
            {
                get
                {
                    return mask;
                }
                set
                {
                    string prepend = string.Empty;
                    for (int i = 0; i < 36 - value.Length; i++)
                    {
                        prepend += "0";
                    }
                    mask = prepend + value;

                    UpdateMask();
                }
            }

            private void UpdateMask()
            {
                var maskArr = mask.ToArray();
                maskOne = new List<int>();
                maskX = new List<int>();

                for (int i = 0; i < maskArr.Length; i++)
                {
                    if (maskArr[i] == '1')
                    {
                        maskOne.Add(i);
                    }
                    else if (maskArr[i] == 'X')
                    {
                        maskX.Add(i);
                    }
                }
            }

            public void UpdateValues(int value, string address)
            {
                string prepend = string.Empty;
                for (int i = 0; i < 36 - address.Length; i++)
                {
                    prepend += "0";
                }
                address = prepend + address;

                var m = ApplyMask(address);

                registers = new List<long>();
                GetRegisters(m);

                foreach (var r in registers)
                {
                    var memAddr = MemoryAddresses.SingleOrDefault(x => x.RegisterNumber == r);

                    if (memAddr != null)
                    {
                        memAddr.Value = value;
                    }
                    else
                    {
                        MemoryAddresses.Add(new MemoryAddress2(r, value));
                    }
                }
            }

            private string ApplyMask(string newValue)
            {
                var temp = newValue.ToArray();
                foreach (int i in maskOne)
                {
                    temp[i] = '1';
                }
                foreach (int i in maskX)
                {
                    temp[i] = 'X';
                }
                return String.Join("", temp);
            }

            private void GetRegisters(string str)
            {
                if (!str.Contains("X"))
                {
                    registers.Add(Convert.ToInt64(str, 2));
                }
                else
                {
                    var firstIndexX = str.IndexOf("X");
                    var one = str.Remove(firstIndexX,1).Insert(firstIndexX, "1");
                    var zero = str.Remove(firstIndexX, 1).Insert(firstIndexX, "0");
                    GetRegisters(one);
                    GetRegisters(zero);
                }
            }
        }

        public class MemoryAddress2
        {
            public MemoryAddress2(long registerNumber, long value)
            {
                RegisterNumber = registerNumber;
                Value = value;
            }

            public long RegisterNumber { get; set; }                    

            public long Value { get; set; }
                       
        }
    }
}
