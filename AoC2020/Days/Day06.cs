﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day06
    {
        public static string Part1()
        {
            // Stealing Josh's idea about string -> char conversion using AddRange. Smart!
            var groupAnswers = new List<char>();
            var groupBuilder = new List<string>();
            long answerSum = 0;

            var customsData = Util.SplitStringOnNewLine(Data.Day6, StringSplitOptions.None);

            foreach (var answers in customsData)
            {
                if (answers == string.Empty)
                {                    
                    answerSum += groupAnswers.Distinct().Count();
                    groupAnswers = new List<char>();
                }
                else
                {
                    groupAnswers.AddRange(answers);
                }
            }
            answerSum += groupAnswers.Distinct().Count();

            return answerSum.ToString();
        }

        public static string Part2()
        {
            var groupAnswers = new List<List<string>>();
            var groupBuilder = new List<string>();

            var customsData = Util.SplitStringOnNewLine(Data.Day6, StringSplitOptions.None);

            foreach (var answers in customsData)
            {
                if (answers == string.Empty)
                {
                    groupAnswers.Add(groupBuilder);
                    groupBuilder = new List<string>();
                }
                else
                {
                    groupBuilder.Add(answers);
                }
            }
            groupAnswers.Add(groupBuilder);

            var splitAnswers = new List<string>();
            long answerSum = 0;

            foreach (var group in groupAnswers)
            {
                foreach (var personAnswers in group)
                {
                    foreach (var answer in personAnswers.ToCharArray())
                    {
                        splitAnswers.Add(answer.ToString());
                    }
                }

                var sharedAnswers = splitAnswers.GroupBy(x => x).Select(x => new { Key = x.Key, Count = x.Count() }).Where(x => x.Count == group.Count).ToList();
                answerSum += sharedAnswers.Distinct().Count();
                splitAnswers = new List<string>();
            }

            return answerSum.ToString();
        }
    }
}
