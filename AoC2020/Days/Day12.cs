﻿using System;

namespace AoC2020.Days
{
    public static class Day12
    {
        public static string Part1()
        {
            var instructions = Util.SplitStringOnNewLine(Data.Day12);
            var ship = new Ship { Heading = 90, X = 0, Y = 0 };

            foreach (var i in instructions)
            {
                var command = i.Substring(0, 1);
                var units = int.Parse(i[1..]);

                switch (command.ToLower())
                {
                    case "n":
                        ship.MoveNorth(units);
                        break;
                    case "s":
                        ship.MoveSouth(units);
                        break;
                    case "e":
                        ship.MoveEast(units);
                        break;
                    case "w":
                        ship.MoveWest(units);
                        break;
                    case "l":
                        ship.TurnLeft(units);
                        break;
                    case "r":
                        ship.TurnRight(units);
                        break;
                    case "f":
                        ship.MoveForward(units);
                        break;
                }
            }

            var manhattanDistance = Math.Abs(0 - ship.X) + Math.Abs(0 - ship.Y);
            return manhattanDistance.ToString();
        }

        public static string Part2()
        {
            var instructions = Util.SplitStringOnNewLine(Data.Day12);
            var ship = new Waypoint { ShipX = 0, ShipY = 0, WaypointX = 10, WaypointY = 1 };

            foreach (var i in instructions)
            {
                var command = i.Substring(0, 1);
                var units = int.Parse(i[1..]);

                switch (command.ToLower())
                {
                    case "n":
                        ship.MoveNorth(units);
                        break;
                    case "s":
                        ship.MoveSouth(units);
                        break;
                    case "e":
                        ship.MoveEast(units);
                        break;
                    case "w":
                        ship.MoveWest(units);
                        break;
                    case "l":
                        ship.TurnLeft(units);
                        break;
                    case "r":
                        ship.TurnRight(units);
                        break;
                    case "f":
                        ship.MoveForward(units);
                        break;
                }
            }

            var manhattanDistance = Math.Abs(0 - ship.ShipX) + Math.Abs(0 - ship.ShipY);
            return manhattanDistance.ToString();
        }                
    }

    public class Ship
    {
        public long X { get; set; }

        public long Y { get; set; }

        public int Heading { get; set; }

        public void MoveNorth(int units) => Y += units;

        public void MoveSouth(int units) => Y -= units;

        public void MoveEast(int units) => X += units;

        public void MoveWest(int units) => X -= units;

        public void MoveForward(int units)
        {
            switch (Heading)
            {
                case 0:
                    MoveNorth(units);
                    break;
                case 90:
                    MoveEast(units);
                    break;
                case 180:
                    MoveSouth(units);
                    break;
                case 270:
                    MoveWest(units);
                    break;
            }
        }

        public void TurnLeft(int units)
        {
            Heading -= units;
            if (Heading < 0)
            {
                Heading = 360 - Math.Abs(Heading);
            }
        }

        public void TurnRight(int units)
        {
            Heading += units;
            if (Heading >= 360)
            {
                Heading -= 360;
            }
        }
    }

    public class Waypoint
    {
        public long ShipX { get; set; }

        public long ShipY { get; set; }

        public long WaypointX { get; set; }

        public long WaypointY { get; set; }

        public void MoveNorth(int units) => WaypointY += units;

        public void MoveSouth(int units) => WaypointY -= units;

        public void MoveEast(int units) => WaypointX += units;

        public void MoveWest(int units) => WaypointX -= units;

        public void MoveForward(int units)
        {
            for (int i = 0; i < units; i++)
            {
                ShipX += WaypointX;
                ShipY += WaypointY;
            }
        }

        public void TurnLeft(int units)
        {
            var iterations = units / 90;
            for (int i = 0; i < iterations; i++)
            {
                var x = WaypointX;
                var y = WaypointY;
                WaypointX = 0 - y;
                WaypointY = x;
            }
        }

        public void TurnRight(int units)
        {
            var iterations = units / 90;
            for (int i = 0; i < iterations; i++)
            {
                var x = WaypointX;
                var y = WaypointY;
                WaypointX = y;
                WaypointY = 0 - x;
            }
        }
    }
}
