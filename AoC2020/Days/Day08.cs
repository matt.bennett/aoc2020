﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace AoC2020.Days
{
    public static class Day08
    {
        public static string Part1()
        {
            var instructionDefs = Util.SplitStringOnNewLine(Data.Day8);

            var instructions = new List<Instruction>();

            foreach (var instruction in instructionDefs)
            {
                var parts = Util.SplitStringOnSpace(instruction);
                instructions.Add(new Instruction { Operation = parts[0], Modifier = int.Parse(parts[1]) });
            }

            var accumValue = 0;

            var result = RunUntilDuplicate(0, instructions, accumValue, out bool terminatedSuccessfully);

            return result.ToString();
        }

        public static string Part2()
        {
            var instructionDefs = Util.SplitStringOnNewLine(Data.Day8);

            var instructions = new List<Instruction>();
            int order = 0;

            foreach (var instruction in instructionDefs)
            {
                var parts = Util.SplitStringOnSpace(instruction);
                instructions.Add(new Instruction { Operation = parts[0], Modifier = int.Parse(parts[1]), Order = order });
                order += 1;
            }

            var accumValue = 0;
            long result = 0;
            var nops = instructions.Where(x => x.Operation == "nop").Select(x => x.Order).ToList();
            var jmps = instructions.Where(x => x.Operation == "jmp").Select(x => x.Order).ToList();

            foreach (var nop in nops)
            {
                accumValue = 0;
                instructions[nop].Operation = "jmp";
                result = RunUntilDuplicate(0, instructions, accumValue, out bool terminatedSuccessfully);
                if (terminatedSuccessfully)
                {
                    return result.ToString();
                }
                else
                {
                    instructions[nop].Operation = "nop";
                    instructions.ForEach(x => x.RunCount = 0);
                }
            }

            foreach (var jmp in jmps)
            {
                accumValue = 0;
                instructions[jmp].Operation = "nop";
                result = RunUntilDuplicate(0, instructions, accumValue, out bool terminatedSuccessfully);
                if (terminatedSuccessfully)
                {
                    return result.ToString();
                }
                else
                {
                    instructions[jmp].Operation = "jmp";
                    instructions.ForEach(x => x.RunCount = 0);
                }
            }

            return "broken";
        }

        public class Instruction
        {
            public Instruction()
            {
                RunCount = 0;
            }

            public int Order { get; set; }

            public string Operation { get; set; }

            public int Modifier { get; set; }

            public int RunCount { get; set; }
        }

        private static long RunUntilDuplicate(int currentStep, List<Instruction> instructions, long accumValue, out bool terminatedSuccessfully)
        {
            if (currentStep >= instructions.Count)
            {
                terminatedSuccessfully = true;
                return accumValue;
            }

            if (instructions[currentStep].RunCount > 0)
            {
                terminatedSuccessfully = false;
                return accumValue;
            }
            else
            {
                instructions[currentStep].RunCount += 1;

                switch (instructions[currentStep].Operation.ToLower())
                {
                    case "nop":
                        currentStep += 1;
                        break;
                    case "acc":
                        accumValue += instructions[currentStep].Modifier;
                        currentStep += 1;
                        break;
                    case "jmp":
                        currentStep += instructions[currentStep].Modifier;
                        break;
                }
                
                return RunUntilDuplicate(currentStep, instructions, accumValue, out terminatedSuccessfully);
            }
        }
    }
}
