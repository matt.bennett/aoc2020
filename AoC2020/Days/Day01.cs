﻿namespace AoC2020.Days
{
    public static class Day01
    {
        public static string Part1()
        {
            var numbers = Util.SplitStringOnNewLine<int>(Data.Day1);

            for (int i = 0; i < numbers.Count; i++)
            {
                for (int x = 0; x < numbers.Count; x++)
                {
                    if (i != x)
                    {
                        if (numbers[i] + numbers[x] == 2020)
                        {
                            return (numbers[i] * numbers[x]).ToString();
                        }
                    }
                }
            }

            // This works for two numbers, and is more efficient... but unsure how it'd expand to three.
            //HashSet<int> set = new HashSet<int>();
            //foreach (int num in numbers)
            //{
            //    if (set.Contains(2020 - num))
            //    {
            //        return (num * (2020-num)).ToString();
            //    }
            //    set.Add(num);
            //}

            return "Finished Unsuccessfully";
        }

        public static string Part2()
        {
            var numbers = Util.SplitStringOnNewLine<int>(Data.Day1);

            for (int i = 0; i < numbers.Count; i++)
            {
                for (int x = 0; x < numbers.Count; x++)
                {
                    for (int z = 0; z < numbers.Count; z++)
                    {
                        if (i != x && i != z && x != z)
                        {
                            if (numbers[i] + numbers[x] + numbers[z] == 2020)
                            {
                                return (numbers[i] * numbers[x] * numbers[z]).ToString();
                            }
                        }
                    }
                }
            }

            return "Finished Unsuccessfully";
        }
    }
}
