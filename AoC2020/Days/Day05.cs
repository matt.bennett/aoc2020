﻿using System.Collections.Generic;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day05
    {
        public static string Part1()
        {
            var seatStrings = Util.SplitStringOnNewLine(Data.Day5);
            var seats = new List<SeatInfo>();

            foreach (var seatString in seatStrings)
            {
                seats.Add(new SeatInfo(seatString));
            }

            // This replaces most of the code above. More terse, but harder to debug if necessary.
            // Util.SplitStringOnNewLine(Data.Day5).ForEach(s => seats.Add(new SeatInfo(s)));

            var x = seats.OrderByDescending(x => x.SeatId).First().SeatId.ToString();
            return x;
        }

        public static string Part2()
        {
            var seatStrings = Util.SplitStringOnNewLine(Data.Day5);
            var seats = new List<SeatInfo>();

            foreach (var seatString in seatStrings)
            {
                seats.Add(new SeatInfo(seatString));
            }

            var tickets = seats.OrderBy(x => x.Row).OrderBy(x => x.Seat).ToList();

            for (int r = tickets.First().Row; r < tickets.Last().Row + 1; r++)
            {
                var row = tickets.Where(x => x.Row == r).ToList();

                if (row.Count != 8)
                {
                    var rowSeats = row.Select(x => x.Seat).ToList<int>();
                    var allSeats = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7 };
                    var s = allSeats.Where(x => !rowSeats.Contains(x)).First();
                    var seat = new SeatInfo(s, r);
                    return seat.SeatId.ToString();
                }
            }

            return "method failed";
        }

        public class SeatInfo
        {
            public SeatInfo(string seatData)
            {
                var row = seatData.Substring(0, 7);
                var seat = seatData.Substring(7);

                Row = CalcRow(row);
                Seat = CalcSeat(seat);
            }
            public SeatInfo(int s, int r)
            {
                Seat = s;
                Row = r;
            }
            public int Row { get; set; }
            public int Seat { get; set; }
            public int SeatId
            {
                get 
                {
                    return (Row * 8) + Seat;
                }
            }
            private int CalcRow(string row)
            {
                List<int> rowData = new List<int>();
                for (int i = 0; i < 128; i++)
                {
                    rowData.Add(i);
                }

                foreach (char d in row.ToArray())
                {
                    var rowMid = rowData.Count / 2;
                    
                    if (d.ToString() == "F")
                    {
                        rowData.RemoveRange(rowMid, rowMid);
                    }
                    else if (d.ToString() == "B")
                    {
                        rowData.RemoveRange(0, rowMid);
                    }
                }

                return rowData[0];
            }
            private int CalcSeat(string seat)
            {
                List<int> seatData = new List<int>();
                for (int i = 0; i < 8; i++)
                {
                    seatData.Add(i);
                }

                foreach (char d in seat.ToArray())
                {
                    var rowMid = seatData.Count / 2;

                    if (d.ToString() == "L")
                    {
                        seatData.RemoveRange(rowMid, rowMid);
                    }
                    else if (d.ToString() == "R")
                    {
                        seatData.RemoveRange(0, rowMid);
                    }
                }
                return seatData[0];
            }
        }
    }
}
