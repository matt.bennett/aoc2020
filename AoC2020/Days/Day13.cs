﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC2020.Days
{
    public static class Day13
    {
        public static string Part1()
        {           
            var input = Util.SplitStringOnNewLine(Data.Day13);
            var busString = Util.SplitStringOnComma(input[1]);

            long earliestDeparture = long.Parse(input[0]);
            var busses = new List<Bus>();

            foreach (var busSpec in busString)
            {
                if (int.TryParse(busSpec, out int busNum))
                {
                    busses.Add(new Bus(busNum, earliestDeparture));
                }
            }
            
            while (!busses.Any(b => b.IsDeparting))
            {
                busses.ForEach(b => b.CurrentTime += 1);
            }

            return busses.Where(b => b.IsDeparting).Single().Calculation.ToString();
        }

        public static string Part2()
        {
            var input = Util.SplitStringOnNewLine(Data.Day13);
            var busString = Util.SplitStringOnComma(input[1]);

            var busses = new List<Bus>();
            var deltaT = 0;
            long currentTime = 0;

            foreach (var busSpec in busString)
            {
                if (int.TryParse(busSpec, out int busNum))
                {
                    busses.Add(new Bus(busNum, currentTime, deltaT));
                }
                deltaT += 1;
            }

            long increment = 1;
            var cache = new Dictionary<string, FoundPoint>();
            
            while (!busses.All(b => b.DepartsOnDeltaSchedule))
            {
                currentTime += increment;
                busses.ForEach(b => b.CurrentTime = currentTime);

                var currAvailable = busses.Where(b => b.DepartsOnDeltaSchedule).ToList();

                if (currAvailable.Count > 1)
                {
                    var key = String.Join(",", currAvailable.OrderBy(b => b.BusId).Select(x => x.BusId.ToString()).ToArray());

                    if (cache.ContainsKey(key))
                    {
                        if (cache[key].RepeatTime == 0)
                        {
                            cache[key].RepeatTime = currentTime;
                        }

                        if (cache[key].Delta > increment)
                        {
                            increment = cache[key].Delta;
                        }
                    }
                    else
                    {
                        cache.Add(key, new FoundPoint { Id = key, InitTime = currentTime });
                    }
                }
            }
            
            return busses.First().CurrentTime.ToString();
        }

        public class FoundPoint
        {
            public FoundPoint()
            {
                InitTime = 0;
                RepeatTime = 0;
            }

            public string Id { get; set; }

            public long InitTime { get; set; }

            public long RepeatTime { get; set; }

            public long Delta
            {
                get { return RepeatTime - InitTime; }
            }
        }

        public class Bus
        {
            private readonly long EarliestDeparture;

            public Bus(int busId, long earliestDeparture)
            {
                BusId = busId;
                CurrentTime = earliestDeparture;
                EarliestDeparture = earliestDeparture;
            }

            public Bus(int busId, long earliestDeparture, long deltaT)
            {
                BusId = busId;
                CurrentTime = earliestDeparture;
                EarliestDeparture = earliestDeparture;
                DeltaT = deltaT;
            }

            public int BusId { get; set; }

            public long CurrentTime { get; set; }

            public long DeltaT { get; set; }

            public bool IsDeparting
            {
                get
                {
                    return CurrentTime % BusId == 0;
                }
            }

            public long Calculation
            {
                get
                {
                    return BusId * (CurrentTime - EarliestDeparture);
                }
            }

            public bool DepartsOnDeltaSchedule
            {
                get 
                {
                    return (CurrentTime + DeltaT) % BusId == 0;
                }
            }
        }
    }
}
