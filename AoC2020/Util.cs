﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC2020
{
    public static class Util
    {
        #region Input parsing
        public static List<string> SplitStringOnNewLine(string input, StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries)
        {
            return input.Split(new string[] { "\n", "\r\n" }, options).ToList();
        }

        public static List<T> SplitStringOnNewLine<T>(string input)
        {
            return SplitString<T>(input, new string[] { "\n", "\r\n" });
        }

        public static List<string> SplitStringOnComma(string input, StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries)
        {
            return input.Split(new string[] { "," }, options).ToList();
        }

        public static List<T> SplitStringOnComma<T>(string input)
        {
            return SplitString<T>(input, new string[] { "," });
        }

        public static List<string> SplitStringOnSpace(string input, StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries)
        {
            return input.Split(new string[] { " " }, options).ToList();
        }

        public static List<T> SplitStringOnSpace<T>(string input, StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries)
        {
            return SplitString<T>(input, new string[] { " " }, options);
        }

        public static List<T> SplitString<T>(string input, string[] delimiters, StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries)
        {
            return Array.ConvertAll(input.Split(delimiters, options), s => (T)Convert.ChangeType(s, typeof(T))).ToList();
        }
        #endregion

        #region Math
        public static long GCF(long a, long b)
        {
            while (b != 0)
            {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

        public static long LCM(long a, long b)
        {
            return (a / GCF(a, b)) * b;
        }

        public static long LCMulti(long[] numbers)
        {
            return numbers.Aggregate(LCM);
        }

        #endregion
    }
}
